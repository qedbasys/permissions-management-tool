﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;

namespace DataAccessLayer
{
    public static class PostgreSqlClass
    {
        //static string connectionString = string.Format("Server={0};Port={1};" + "User Id={2};Password={3};Database={4};",
        //            "localhost", "5432", "postgres",
        //            "Lawr123#", "permission_management_tool_db");
        static string database = ConfigurationManager.AppSettings["Database"];
        static string password = ConfigurationManager.AppSettings["Password"];
        static string userId = ConfigurationManager.AppSettings["Username"];
        static string port = ConfigurationManager.AppSettings["Port"];
        static string server = ConfigurationManager.AppSettings["Server"];

        static string connectionString = string.Format("Server={0};Port={1};" + "User Id={2};Password={3};Database={4};",server, port, userId, password, database);
        static DataTable dataTable = new DataTable();

        public static List<object> GetAllData(string query)
        {
            List<object> data = null;
            try
            {
                dataTable = new DataTable();
                // Making connection with Npgsql provider
                using (NpgsqlConnection connection = new NpgsqlConnection(connectionString))
                {
                    connection.Open();
                    // SqlCommand cmd = new SqlCommand(query, conn);
                    using (NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter(query, connection))
                    {
                        dataAdapter.Fill(dataTable);
                    }
                }
                if (dataTable != null && dataTable.Rows.Count > 0)
                {
                    data = dataTable.AsEnumerable().ToList<object>();
                }
            }
            catch (Exception exception)
            {
                data = null;
                throw exception;
            }
            return data;
        }

        public static DataTable GetAllDataTable(string query)
        {
            try
            {
                dataTable = new DataTable();
                using (NpgsqlConnection connection = new NpgsqlConnection(connectionString))
                {
                    connection.Open();
                    using (NpgsqlDataAdapter da = new NpgsqlDataAdapter(query, connection))
                    {
                        da.Fill(dataTable);
                    }
                }
                return dataTable;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public static DataSet GetDataSet(string ProcedureName, NpgsqlParameter[] commandParameters)
        {
            DataSet ds = new DataSet();
            try
            {
                using (NpgsqlConnection conn = new NpgsqlConnection(connectionString))
                {
                    conn.Open();
                    NpgsqlCommand command = new NpgsqlCommand();
                    command.Connection = conn;
                    command.CommandText = ProcedureName;
                    command.CommandTimeout = 0;
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Clear();
                    if (commandParameters != null)
                    {
                        command.Parameters.AddRange(commandParameters);
                    }
                    using (NpgsqlDataAdapter da = new NpgsqlDataAdapter(command))
                    {
                        da.Fill(ds);
                    }
                }
            }
            catch (Exception exception)
            {
                ds = null;
                throw exception;
            }
            return ds;
        }

        public static DataTable GetDataTable(string procedureName, params NpgsqlParameter[] commandParameters)
        {
            DataTable dt = new DataTable();
            try
            {
                using (NpgsqlConnection conn = new NpgsqlConnection(connectionString))
                {
                    conn.Open();
                    NpgsqlCommand command = new NpgsqlCommand();
                    command.Connection = conn;
                    command.CommandText = procedureName;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandTimeout = 0;
                    command.Parameters.Clear();
                    if (commandParameters != null)
                    {
                        command.Parameters.AddRange(commandParameters);
                    }
                    using (NpgsqlDataAdapter da = new NpgsqlDataAdapter(command))
                    {
                        da.Fill(dt);
                    }
                }
            }
            catch (Exception exception)
            {
                dt = null;
                throw exception;
            }
            return dt;
        }

        public static int InsertData(string tableName, string paramList, string paramValues)
        {
            int result = -1;
            try
            {
                string query = "INSERT INTO " + tableName + "(" + paramList + ") VALUES(" + paramValues + ");SELECT currval(pg_get_serial_sequence('" + tableName + "','id'));";

                using (NpgsqlConnection conn = new NpgsqlConnection(connectionString))
                {
                    conn.Open();
                    using (NpgsqlCommand command = new NpgsqlCommand(query, conn))
                    {
                        result = Convert.ToInt32(command.ExecuteScalar());
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return result;
        }

        public static int DeleteData(string tableName, string parameter, int value)
        {
            int result = -1;
            try
            {
                string query = "DELETE FROM " + tableName + " WHERE " + parameter + " = " + value;
                using (NpgsqlConnection connection = new NpgsqlConnection(connectionString))
                {
                    connection.Open();
                    using (NpgsqlCommand command = new NpgsqlCommand(query, connection))
                    {
                        result = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception exception)
            {
                result = 0;
                throw exception;
            }
            return result;
        }

        public static int DeleteData(string query)
        {
            int result = -1;
            try
            {
                using (NpgsqlConnection connection = new NpgsqlConnection(connectionString))
                {
                    connection.Open();
                    using (NpgsqlCommand command = new NpgsqlCommand(query, connection))
                    {
                        result = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception exception)
            {
                result = 0;
                throw exception;
            }
            return result;
        }


        public static int UpdateData(string tableName, string paramDetails, string whereCondition)
        {
            string query = string.Empty;
            int result = -1;
            try
            {
                if (!String.IsNullOrEmpty(whereCondition))
                {
                    query = "UPDATE " + tableName + " SET " + paramDetails + " WHERE " + whereCondition;
                }
                else
                {
                    query = "UPDATE " + tableName + " SET " + paramDetails;
                }
                using (NpgsqlConnection conn = new NpgsqlConnection(connectionString))
                {
                    conn.Open();
                    using (NpgsqlCommand command = new NpgsqlCommand(query, conn))
                    {
                        result = command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception exception)
            {
                result = 0;
                throw exception;
            }
            return result;
        }

        public static int UpdateData(string databaseQuery)
        {
            int result = -1;
            try
            {
                using (NpgsqlConnection conn = new NpgsqlConnection(connectionString))
                {
                    conn.Open();
                    using (NpgsqlCommand command = new NpgsqlCommand(databaseQuery, conn))
                    {
                        result = command.ExecuteNonQuery();
                    }
                }
                return result;
            }
            catch (Exception exception)
            {
                result = 0;
                throw exception;
            }
        }

        public static bool CheckIfExists(string query)
        {
            try
            {
                int result = -1;
                using (NpgsqlConnection conn = new NpgsqlConnection(connectionString))
                {
                    conn.Open();
                    using (NpgsqlCommand command = new NpgsqlCommand(query, conn))
                    {
                        // result = command.ExecuteNonQuery();
                        result = Convert.ToInt32(command.ExecuteScalar());
                    }
                }
                return result == 0 ? false : true;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public static string GetTypeById(string query)
        {
            try
            {
                string result = string.Empty;
                using (NpgsqlConnection conn = new NpgsqlConnection(connectionString))
                {
                    conn.Open();
                    using (NpgsqlCommand command = new NpgsqlCommand(query, conn))
                    {
                        result = command.ExecuteScalar().ToString();
                    }
                }
                return result;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

    }
}
