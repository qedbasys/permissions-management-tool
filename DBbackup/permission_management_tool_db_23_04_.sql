PGDMP         2                x            permission_management_tool_db    10.12    10.12 O    V           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            W           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            X           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            Y           1262    16393    permission_management_tool_db    DATABASE     �   CREATE DATABASE permission_management_tool_db WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252';
 -   DROP DATABASE permission_management_tool_db;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            Z           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12924    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            [           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1255    16649 7   getclientpermission(integer, integer, integer, integer)    FUNCTION       CREATE FUNCTION public.getclientpermission(countryid integer, userid integer, clientid integer, projectid integer) RETURNS TABLE(accesstype character varying, accesstypeid integer, organisationstructure character varying, organisationstructureid integer, ruletype character varying, ruletypeid integer, scopetype character varying, scopetypeid integer)
    LANGUAGE plpgsql
    AS $$
BEGIN
	RETURN QUERY
	SELECT at.name as AccessType,at.id as AccessTypeId,os.name as OrganisationStructure,os.id as OrganisationStructureId,
	rt.name as RuleType,rt.id as RuleTypeId,sc.name as ScopeType,sc.id as ScopeTypeId
	FROM permissions as p
	JOIN projects as pr
	ON p.project_id = pr.id
	JOIN access_type as at
	ON at.id = p.access_type_id
	JOIN organisational_structure as os
	ON os.id = p.organisational_structure_id
	JOIN rule_type as rt
	ON rt.id = p.rule_type_id
	JOIN scope_type sc
	ON sc.id = p.scope_id
	JOIN clients c
	ON c.id = pr.client_id
	WHERE c.country_id = countryId
	AND c.user_id = userId
	AND c.id = clientId
	AND pr.id = projectId;
	
END;
$$;
 r   DROP FUNCTION public.getclientpermission(countryid integer, userid integer, clientid integer, projectid integer);
       public       postgres    false    1    3            �            1259    16452    access_type    TABLE     h   CREATE TABLE public.access_type (
    id integer NOT NULL,
    name character varying(1000) NOT NULL
);
    DROP TABLE public.access_type;
       public         postgres    false    3            �            1259    16450    access_type_id_seq    SEQUENCE     �   CREATE SEQUENCE public.access_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.access_type_id_seq;
       public       postgres    false    3    201            \           0    0    access_type_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.access_type_id_seq OWNED BY public.access_type.id;
            public       postgres    false    200            �            1259    16613    clients    TABLE     �   CREATE TABLE public.clients (
    id integer NOT NULL,
    name character varying(1000) NOT NULL,
    user_id integer,
    country_id integer
);
    DROP TABLE public.clients;
       public         postgres    false    3            �            1259    16611    clients_id_seq    SEQUENCE     �   CREATE SEQUENCE public.clients_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.clients_id_seq;
       public       postgres    false    3    213            ]           0    0    clients_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.clients_id_seq OWNED BY public.clients.id;
            public       postgres    false    212            �            1259    16602 	   countries    TABLE     f   CREATE TABLE public.countries (
    id integer NOT NULL,
    name character varying(1000) NOT NULL
);
    DROP TABLE public.countries;
       public         postgres    false    3            �            1259    16600    countries_id_seq    SEQUENCE     �   CREATE SEQUENCE public.countries_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.countries_id_seq;
       public       postgres    false    3    211            ^           0    0    countries_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.countries_id_seq OWNED BY public.countries.id;
            public       postgres    false    210            �            1259    16463    organisational_structure    TABLE     u   CREATE TABLE public.organisational_structure (
    id integer NOT NULL,
    name character varying(1000) NOT NULL
);
 ,   DROP TABLE public.organisational_structure;
       public         postgres    false    3            �            1259    16461    organisational_structure_id_seq    SEQUENCE     �   CREATE SEQUENCE public.organisational_structure_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 6   DROP SEQUENCE public.organisational_structure_id_seq;
       public       postgres    false    203    3            _           0    0    organisational_structure_id_seq    SEQUENCE OWNED BY     c   ALTER SEQUENCE public.organisational_structure_id_seq OWNED BY public.organisational_structure.id;
            public       postgres    false    202            �            1259    16569    permissions    TABLE       CREATE TABLE public.permissions (
    id integer NOT NULL,
    project_id integer,
    access_type_id integer,
    organisational_structure_id integer,
    rule_type_id integer,
    scope_id integer,
    created_by integer,
    date_created date DEFAULT CURRENT_DATE NOT NULL
);
    DROP TABLE public.permissions;
       public         postgres    false    3            �            1259    16567    permissions_id_seq    SEQUENCE     �   CREATE SEQUENCE public.permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.permissions_id_seq;
       public       postgres    false    3    209            `           0    0    permissions_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.permissions_id_seq OWNED BY public.permissions.id;
            public       postgres    false    208            �            1259    16420    projects    TABLE     |   CREATE TABLE public.projects (
    id integer NOT NULL,
    name character varying(1000) NOT NULL,
    client_id integer
);
    DROP TABLE public.projects;
       public         postgres    false    3            �            1259    16418    projects_id_seq    SEQUENCE     �   CREATE SEQUENCE public.projects_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.projects_id_seq;
       public       postgres    false    3    199            a           0    0    projects_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.projects_id_seq OWNED BY public.projects.id;
            public       postgres    false    198            �            1259    16474 	   rule_type    TABLE     f   CREATE TABLE public.rule_type (
    id integer NOT NULL,
    name character varying(1000) NOT NULL
);
    DROP TABLE public.rule_type;
       public         postgres    false    3            �            1259    16472    rule_type_id_seq    SEQUENCE     �   CREATE SEQUENCE public.rule_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.rule_type_id_seq;
       public       postgres    false    3    205            b           0    0    rule_type_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.rule_type_id_seq OWNED BY public.rule_type.id;
            public       postgres    false    204            �            1259    16558 
   scope_type    TABLE     g   CREATE TABLE public.scope_type (
    id integer NOT NULL,
    name character varying(1000) NOT NULL
);
    DROP TABLE public.scope_type;
       public         postgres    false    3            �            1259    16556    scope_type_id_seq    SEQUENCE     �   CREATE SEQUENCE public.scope_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.scope_type_id_seq;
       public       postgres    false    207    3            c           0    0    scope_type_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.scope_type_id_seq OWNED BY public.scope_type.id;
            public       postgres    false    206            �            1259    16396    users    TABLE     �   CREATE TABLE public.users (
    id integer NOT NULL,
    user_id character varying(128) NOT NULL,
    first_name character varying(50),
    last_name character varying(50),
    username character varying(50),
    email character varying(50)
);
    DROP TABLE public.users;
       public         postgres    false    3            �            1259    16394    users_id_seq    SEQUENCE     �   CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.users_id_seq;
       public       postgres    false    197    3            d           0    0    users_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;
            public       postgres    false    196            �
           2604    16455    access_type id    DEFAULT     p   ALTER TABLE ONLY public.access_type ALTER COLUMN id SET DEFAULT nextval('public.access_type_id_seq'::regclass);
 =   ALTER TABLE public.access_type ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    200    201    201            �
           2604    16616 
   clients id    DEFAULT     h   ALTER TABLE ONLY public.clients ALTER COLUMN id SET DEFAULT nextval('public.clients_id_seq'::regclass);
 9   ALTER TABLE public.clients ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    212    213    213            �
           2604    16605    countries id    DEFAULT     l   ALTER TABLE ONLY public.countries ALTER COLUMN id SET DEFAULT nextval('public.countries_id_seq'::regclass);
 ;   ALTER TABLE public.countries ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    210    211    211            �
           2604    16466    organisational_structure id    DEFAULT     �   ALTER TABLE ONLY public.organisational_structure ALTER COLUMN id SET DEFAULT nextval('public.organisational_structure_id_seq'::regclass);
 J   ALTER TABLE public.organisational_structure ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    202    203    203            �
           2604    16572    permissions id    DEFAULT     p   ALTER TABLE ONLY public.permissions ALTER COLUMN id SET DEFAULT nextval('public.permissions_id_seq'::regclass);
 =   ALTER TABLE public.permissions ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    208    209    209            �
           2604    16423    projects id    DEFAULT     j   ALTER TABLE ONLY public.projects ALTER COLUMN id SET DEFAULT nextval('public.projects_id_seq'::regclass);
 :   ALTER TABLE public.projects ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    198    199    199            �
           2604    16477    rule_type id    DEFAULT     l   ALTER TABLE ONLY public.rule_type ALTER COLUMN id SET DEFAULT nextval('public.rule_type_id_seq'::regclass);
 ;   ALTER TABLE public.rule_type ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    204    205    205            �
           2604    16561    scope_type id    DEFAULT     n   ALTER TABLE ONLY public.scope_type ALTER COLUMN id SET DEFAULT nextval('public.scope_type_id_seq'::regclass);
 <   ALTER TABLE public.scope_type ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    207    206    207            �
           2604    16399    users id    DEFAULT     d   ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    197    196    197            G          0    16452    access_type 
   TABLE DATA               /   COPY public.access_type (id, name) FROM stdin;
    public       postgres    false    201   KZ       S          0    16613    clients 
   TABLE DATA               @   COPY public.clients (id, name, user_id, country_id) FROM stdin;
    public       postgres    false    213   wZ       Q          0    16602 	   countries 
   TABLE DATA               -   COPY public.countries (id, name) FROM stdin;
    public       postgres    false    211   �Z       I          0    16463    organisational_structure 
   TABLE DATA               <   COPY public.organisational_structure (id, name) FROM stdin;
    public       postgres    false    203   [       O          0    16569    permissions 
   TABLE DATA               �   COPY public.permissions (id, project_id, access_type_id, organisational_structure_id, rule_type_id, scope_id, created_by, date_created) FROM stdin;
    public       postgres    false    209   V[       E          0    16420    projects 
   TABLE DATA               7   COPY public.projects (id, name, client_id) FROM stdin;
    public       postgres    false    199   �[       K          0    16474 	   rule_type 
   TABLE DATA               -   COPY public.rule_type (id, name) FROM stdin;
    public       postgres    false    205   �[       M          0    16558 
   scope_type 
   TABLE DATA               .   COPY public.scope_type (id, name) FROM stdin;
    public       postgres    false    207   �[       C          0    16396    users 
   TABLE DATA               T   COPY public.users (id, user_id, first_name, last_name, username, email) FROM stdin;
    public       postgres    false    197   8\       e           0    0    access_type_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.access_type_id_seq', 2, true);
            public       postgres    false    200            f           0    0    clients_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.clients_id_seq', 1, true);
            public       postgres    false    212            g           0    0    countries_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.countries_id_seq', 5, true);
            public       postgres    false    210            h           0    0    organisational_structure_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.organisational_structure_id_seq', 7, true);
            public       postgres    false    202            i           0    0    permissions_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.permissions_id_seq', 2, true);
            public       postgres    false    208            j           0    0    projects_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.projects_id_seq', 2, true);
            public       postgres    false    198            k           0    0    rule_type_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.rule_type_id_seq', 2, true);
            public       postgres    false    204            l           0    0    scope_type_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.scope_type_id_seq', 2, true);
            public       postgres    false    206            m           0    0    users_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.users_id_seq', 6, true);
            public       postgres    false    196            �
           2606    16460    access_type access_type_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.access_type
    ADD CONSTRAINT access_type_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.access_type DROP CONSTRAINT access_type_pkey;
       public         postgres    false    201            �
           2606    16621    clients clients_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.clients
    ADD CONSTRAINT clients_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.clients DROP CONSTRAINT clients_pkey;
       public         postgres    false    213            �
           2606    16610    countries countries_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.countries
    ADD CONSTRAINT countries_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.countries DROP CONSTRAINT countries_pkey;
       public         postgres    false    211            �
           2606    16471 6   organisational_structure organisational_structure_pkey 
   CONSTRAINT     t   ALTER TABLE ONLY public.organisational_structure
    ADD CONSTRAINT organisational_structure_pkey PRIMARY KEY (id);
 `   ALTER TABLE ONLY public.organisational_structure DROP CONSTRAINT organisational_structure_pkey;
       public         postgres    false    203            �
           2606    16574    permissions permissions_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.permissions
    ADD CONSTRAINT permissions_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.permissions DROP CONSTRAINT permissions_pkey;
       public         postgres    false    209            �
           2606    16428    projects projects_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.projects
    ADD CONSTRAINT projects_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.projects DROP CONSTRAINT projects_pkey;
       public         postgres    false    199            �
           2606    16482    rule_type rule_type_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.rule_type
    ADD CONSTRAINT rule_type_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.rule_type DROP CONSTRAINT rule_type_pkey;
       public         postgres    false    205            �
           2606    16566    scope_type scope_type_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.scope_type
    ADD CONSTRAINT scope_type_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.scope_type DROP CONSTRAINT scope_type_pkey;
       public         postgres    false    207            �
           2606    16401    users users_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public         postgres    false    197            �
           2606    16627    clients clients_country_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.clients
    ADD CONSTRAINT clients_country_id_fkey FOREIGN KEY (country_id) REFERENCES public.countries(id);
 I   ALTER TABLE ONLY public.clients DROP CONSTRAINT clients_country_id_fkey;
       public       postgres    false    2751    211    213            �
           2606    16622    clients clients_user_id_fkey    FK CONSTRAINT     {   ALTER TABLE ONLY public.clients
    ADD CONSTRAINT clients_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);
 F   ALTER TABLE ONLY public.clients DROP CONSTRAINT clients_user_id_fkey;
       public       postgres    false    213    197    2737            �
           2606    16580 +   permissions permissions_access_type_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.permissions
    ADD CONSTRAINT permissions_access_type_id_fkey FOREIGN KEY (access_type_id) REFERENCES public.access_type(id);
 U   ALTER TABLE ONLY public.permissions DROP CONSTRAINT permissions_access_type_id_fkey;
       public       postgres    false    209    2741    201            �
           2606    16585 8   permissions permissions_organisational_structure_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.permissions
    ADD CONSTRAINT permissions_organisational_structure_id_fkey FOREIGN KEY (organisational_structure_id) REFERENCES public.organisational_structure(id);
 b   ALTER TABLE ONLY public.permissions DROP CONSTRAINT permissions_organisational_structure_id_fkey;
       public       postgres    false    209    2743    203            �
           2606    16575 '   permissions permissions_project_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.permissions
    ADD CONSTRAINT permissions_project_id_fkey FOREIGN KEY (project_id) REFERENCES public.projects(id);
 Q   ALTER TABLE ONLY public.permissions DROP CONSTRAINT permissions_project_id_fkey;
       public       postgres    false    209    2739    199            �
           2606    16590 )   permissions permissions_rule_type_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.permissions
    ADD CONSTRAINT permissions_rule_type_id_fkey FOREIGN KEY (rule_type_id) REFERENCES public.rule_type(id);
 S   ALTER TABLE ONLY public.permissions DROP CONSTRAINT permissions_rule_type_id_fkey;
       public       postgres    false    2745    205    209            �
           2606    16595 %   permissions permissions_scope_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.permissions
    ADD CONSTRAINT permissions_scope_id_fkey FOREIGN KEY (scope_id) REFERENCES public.scope_type(id);
 O   ALTER TABLE ONLY public.permissions DROP CONSTRAINT permissions_scope_id_fkey;
       public       postgres    false    2747    207    209            G      x�3�t���/�2�tIͫ����� 7A�      S   &   x�3�t�pT�M�+N,JQ��+.-J�4�4����� ���      Q   D   x�3�tJ�H�M,�2��NͫL�2��M,-�,�,-�2���LO-�L�2��/-�PpL+�LN����� '��      I   E   x�3�t20�2��\ƜN�\&��Aξ\��.�\f�.�E��%�E�\朁�.
���E�%���\1z\\\ �;�      O   &   x�3�4�4b4202�50�52�2B6@������ �O�      E   6   x�3�K�)M,���SpIMN�MJ-R020��4�2�tsRH,VH,������� 1#m      K      x�3�JML�2�/�,I����� 5��      M   *   x�3���,Vp��II-�2�0��K�t���b�=... N      C   Q  x�M��N�0Eמ��%��&;�����E+�-�����afPW�>W�~(2�d��9�D��eͨb)�����a�}����OM�3��W���]��qq�vX�%c0fY��fLZ)�Z��S��>��0 ���E��J������F�K�NG���,K���������C�;u�E�户�9��$T�Z�R@/��$�3A;U�m���2�W�s׭��'��S,܋���N0��*�W��I�[�*���N��
^�{�9�>���	�"��FZ��N�I�w� Y#��d��I���ⴄ��}���}��<����r�b���7���~�O��     