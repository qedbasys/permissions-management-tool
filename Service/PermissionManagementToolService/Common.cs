﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.ServiceProcess;

namespace PermissionManagementToolService
{
    public class Common
    {
        public static void CreateMessage()
        {
            try
            {
                using (var client = new SmtpClient())
                {
                    MailMessage message = new MailMessage();
                    message.To.Add(new MailAddress("PMT@qedact.com"));
                    message.From = new MailAddress("qedsmtp@qedactuarial.co.za");
                    //message.CC.Add(new MailAddress("etienne.louw@qedact.com"));5MtP123#
                    //message.CC.Add(new MailAddress("itsupport@qedact.com"));

                    message.Subject = "Permission Management Tool Service Error - " + DateTime.Now.Date.ToShortDateString().Replace('/', '_');
                    message.Body = @"The Permission Management Tool Service has stopped running,making the system users in domain difficult to track.";

                    // Credentials are necessary if the server requires the client 
                    // to authenticate before it will send email on the client's behalf.
                    NetworkCredential networkCredential = new NetworkCredential("qedsmtp@qedactuarial.co.za", "5MtP123#");
                    client.Port = 587;
                    client.Host = "smtp.office365.com";
                    client.UseDefaultCredentials = false;
                    client.EnableSsl = true;
                    client.Credentials = networkCredential;

                    client.Send(message);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception caught in CreateTestMessage2(): {0}",
                    ex.ToString());
            }
        }

        public static ServiceController GetService(string serviceName)
        {
            ServiceController[] services = ServiceController.GetServices();
            return services.FirstOrDefault(x => x.ServiceName == serviceName);
        }

        public static void WriteToFile(string path, string fileName, string messageToWrite)
        {
            try
            {
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                string filepath = AppDomain.CurrentDomain.BaseDirectory + "Logs\\" + fileName;
                if (!File.Exists(filepath))
                {
                    // Create a file to write to.   
                    using (StreamWriter sw = File.CreateText(filepath))
                    {
                        sw.WriteLine(messageToWrite);
                    }
                }
                else
                {
                    using (StreamWriter sw = File.AppendText(filepath))
                    {
                        sw.WriteLine(messageToWrite);
                    }
                }
            }
            catch (Exception exception)
            {
                CreateMessage();
                throw exception;
            }
        }

    }
}
