﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Timers;

namespace PermissionManagementToolService
{
    public partial class PermissionManagementToolService : ServiceBase
    {
        Timer timer = new Timer();
        string path = AppDomain.CurrentDomain.BaseDirectory + "\\Logs";
        string filepath = string.Empty;
        bool isOnStop = false;

        public PermissionManagementToolService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                WriteToFile("Service is started at " + DateTime.Now);
                timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
                timer.Interval = 30000;// 3600000; //number in milisecinds //1 hour
                timer.Enabled = true;

            }
            catch (Exception exception)
            {
                Stop();
                throw exception;
            }
        }

        protected override void OnStop()
        {
            try
            {
                if (!isOnStop)
                {
                    WriteToFile("Service is stopped at " + DateTime.Now);
                    //TODO:::implement email functionality to alert Admin on service stop
                    Common.CreateMessage();
                }
            }
            catch (Exception exception)
            {
                isOnStop = true;
                Stop();
                Common.CreateMessage();
                throw exception;
            }
        }


        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            try
            {
                WriteToFile("Service is recall at " + DateTime.Now);
            }
            catch (Exception exception)
            {
                Stop();
                throw exception;
            }
        }

        public void WriteToFile(string Message)
        {
            try
            {
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                filepath = AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\PermissionManagementToolServiceLog_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
                if (!File.Exists(filepath))
                {
                    // Create a file to write to.   
                    using (StreamWriter sw = File.CreateText(filepath))
                    {
                        sw.WriteLine(Message);
                    }
                }
                else
                {
                    using (StreamWriter sw = File.AppendText(filepath))
                    {
                        sw.WriteLine(Message);
                    }
                }
            }
            catch (Exception exception)
            {
                OnStop(exception);
                Common.CreateMessage();
                throw exception;
            }
        }

        public void OnStop(Exception exception)
        {
            try
            {
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                filepath = AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\PermissionManagementToolServiceLog_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
                if (!File.Exists(filepath))
                {
                    // Create a file to write to.   
                    using (StreamWriter sw = File.CreateText(filepath))
                    {
                        sw.WriteLine("Service stoped error:" + DateTime.Now + exception.Message);
                    }
                }
                else
                {
                    using (StreamWriter sw = File.AppendText(filepath))
                    {
                        sw.WriteLine("Service stoped error:" + DateTime.Now + exception.Message);
                    }
                }
            }
            catch (Exception exceptionError)
            {
                throw exceptionError;
            }
        }

        public void UserInfoProcessing(List<GroupPrincipal> result, UserPrincipal userPrincipal)
        {
            try
            {
                ServiceInteractions serviceInteractions = new ServiceInteractions();
                serviceInteractions.UserInfoProcessing();
            }
            catch (Exception exception)
            {
                ServiceController controller = Common.GetService("PermissionManagementToolService");
                if (controller == null || controller.Status == ServiceControllerStatus.Stopped)
                {
                    Common.CreateMessage();
                    return;
                }

                controller.Stop();
                Common.CreateMessage();
                throw exception;
            }
        }

    }
}
