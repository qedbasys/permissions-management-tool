﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.AccessControl;
using System.Text;
using System.Threading;

class Server
{
    TcpListener server = null;
    public Server(string ip, int port)
    {
        IPAddress localAddr = IPAddress.Parse(ip);
        server = new TcpListener(IPAddress.Any, port);
        server.Start();
        StartListener();
    }
    public void StartListener()
    {
        Console.WriteLine("Starting the Listener");
        try
        {
            // Buffer for reading data
            Byte[] bytes = new Byte[256];
            while (true)
            {
                Console.WriteLine("Waiting for a connection...");
                TcpClient client = server.AcceptTcpClient();
                Console.WriteLine("Connected!");

                Thread t = new Thread(new ParameterizedThreadStart(HandleDeivce));
                t.Start(client);
            }
        }
        catch (SocketException e)
        {
            Console.WriteLine("SocketException: {0}", e);
            server.Stop();
        }
    }
    public void HandleDeivce(Object obj)
    {
        TcpClient client = (TcpClient)obj;
        var stream = client.GetStream();
        string imei = String.Empty;
        string data = null;
        Byte[] bytes = new Byte[256];
        int i;
        try
        {
            while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
            {
                string hex = BitConverter.ToString(bytes);
                data = Encoding.ASCII.GetString(bytes, 0, i);
                string[] arguments = data.Split(';');
                string path = arguments[0];
                path = @"E:\QED\G_ArrayTemp\QED Clients\" + path;
                //path = path.Replace(@"\\", @"\");
                string department = arguments[1];
                string accessControl = arguments[2];
                string fileSystemRight = arguments[3];
                string accessRights = arguments[4];
                string folderType = arguments[5];
                Console.WriteLine("{1}: Received: {0}", data, Thread.CurrentThread.ManagedThreadId);
                string str = "Hey Device!";
                Byte[] reply = System.Text.Encoding.ASCII.GetBytes(str);
                stream.Write(reply, 0, reply.Length);
                Console.WriteLine("{1}: Sent: {0}", str, Thread.CurrentThread.ManagedThreadId);
                //SetClientPermissions(@"C:\Users\EtienneL\Documents\947 Certificates", "B10", "Allow", "Write");
                bool success;
                success = SetClientPermissions(path, department, accessControl, fileSystemRight, accessRights, folderType);
            }
        }
        catch (Exception e)
        {
            Console.WriteLine("Exception: {0}", e.ToString());
            client.Close();
        }
    }

    public static bool SetClientPermissions(string path, string department, string accessControl, string fileSystemRight, string accessRights, string folderType)
    {
        //string serverPrefixPath = @"E:\QED\G_ArrayTemp\QED Clients\";

        //Remove U:\ from the path that comes in and then concat the path with the server prefix

        //string root = Path.GetPathRoot(path);

        //if (root != null)
        //{
        //    path = path.Substring(root.Length);
        //}

        try
        {
            //string DirectoryName = @"C:\Inetpub\ftproot";
            //string DirectoryName = serverPrefixPath + path;
            string DirectoryName = path;

            Console.WriteLine("Adding access control entry for " + DirectoryName);
            accessControl = accessControl.ToLower();
            fileSystemRight = fileSystemRight.ToLower();
            accessRights = accessRights.ToLower();
            folderType = folderType.ToLower();

            if (accessControl == "allow")
            {
            }
            else if (accessControl == "deny")
            {
            }

            if (fileSystemRight == "read")
            {
            }
            else if (fileSystemRight == "write")
            {
            }

            if (accessRights == "add")
            {
                //AddDirectorySecurity(DirectoryName, @"qed.local\" + department, FileSystemRights.Modify, AccessControlType.Allow, folderType);
                AddDirectorySecuritySub(DirectoryName, department, FileSystemRights.Modify, AccessControlType.Allow, folderType);

            }
            else if (accessRights == "remove")
            {
                //RemoveDirectorySecurity(DirectoryName, @"qed.local\" + department, FileSystemRights.Modify, AccessControlType.Allow, folderType);
                RemoveDirectorySecuritySubDir(DirectoryName, department, FileSystemRights.Modify, AccessControlType.Allow, folderType);
            }

            // Add the access control entry to the directory.
            //AddDirectorySecurity(DirectoryName, @"QED\" + department, fileSystemRightType, accessControlType);
            //Console.WriteLine("Removing access control entry from " + DirectoryName);
            // Remove the access control entry from the directory.
            //RemoveDirectorySecurity(DirectoryName, @"QED\" + department, FileSystemRights.Write, AccessControlType.Allow);
            Console.WriteLine("Done.");
            Console.ReadLine();
            return true;
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            Console.ReadLine();
            return false;
        }


    }

    private static void RemoveDirectorySecuritySubDir(string directoryName, string department, FileSystemRights modify, AccessControlType allow, string folderType)
    {
        try
        {
            string[] subdirectoryEntries = Directory.GetDirectories(directoryName);
            foreach (string item in subdirectoryEntries)
            {
                AddFolderPermissions(directoryName, department, modify, allow);
                if ((department == "B02") || (department == "B03") || (department == "B04") || (department == "B05") || (department == "B06") || (department == "B10"))
                {
                    AddFolderPermissions(directoryName, department + " Extended", modify, allow);
                }
                AddDirectorySecuritySub(item, department, modify, allow, folderType);
                //add secretaries
                AddSecretaries(item, modify, allow);

            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    private static void AddDirectorySecuritySub(string FileName, string Account, FileSystemRights Rights, AccessControlType ControlType, string folderType)
    {
        try
        {
            string[] subdirectoryEntries = Directory.GetDirectories(FileName);
            foreach (string item in subdirectoryEntries)
            {
                AddFolderPermissions(FileName, Account, Rights, ControlType);
                if ((Account == "B02") || (Account == "B03") || (Account == "B04") || (Account == "B05") || (Account == "B06") || (Account == "B10"))
                {
                    AddFolderPermissions(FileName, Account + " Extended", Rights, ControlType);
                }
                AddDirectorySecuritySub(item, Account, Rights, ControlType, folderType);
                //add secretaries
                if (folderType == "client")
                {
                    AddSecretaries(item, Rights, ControlType);
                }
                else if (folderType == "project")
                {
                    AddProjectSecretaries(item, Rights, ControlType);
                }
            }
        }
        catch (Exception exception)
        {
            throw exception;
        }
    }

    private static void AddProjectSecretaries(string item, FileSystemRights rights, AccessControlType controlType)
    {
        try
        {
            string[] substring = item.Split('\\');
            string caseString = substring[3] + "\\" + substring[4];
            if (substring.Count() == 5)
            {
                switch (caseString)
                {
                    case "4 Results\\1 Reports and Letters":
                        AddFolderPermissions(item /*+ @"\Pre2020"*/, "Secretaries", rights, controlType);
                        break;
                    case "4 Results\\2 Presentations":
                        AddFolderPermissions(item /*+ @"\1 Meta Data\2 TendersProposals"*/, "Secretaries", rights, controlType);
                        break;
                    case "4 Results\\3 Emails":
                        AddFolderPermissions(item /*+ @"\1 Meta Data\3 Agreements"*/, "Secretaries", rights, controlType);
                        break;
                    case "4 Results\\4 Returns":
                        AddFolderPermissions(item /*+ @"\1 Meta Data\4 Billing"*/, "Secretaries", rights, controlType);
                        break;
                    case "4 Results\\5 Other":
                        AddFolderPermissions(item /*+ @"\1 Meta Data\5 Client Feedback"*/, "Secretaries", rights, controlType);
                        break;
                    default:
                        //do nothing
                        break;
                }
            }

        }
        catch (Exception exception)
        {
            throw exception;
        }
    }

    private static void AddSecretaries(string item, FileSystemRights rights, AccessControlType controlType)
    {
        try
        {
            string[] substring = item.Split('\\');
            if (substring.Count() == 4)
            {
                if (substring[3] == "Pre2020")
                {
                    AddFolderPermissions(item /*+ @"\Pre2020"*/, "Secretaries", rights, controlType);
                }
            }
            if (substring.Count() == 5)
            {
                string caseString = substring[3] + "\\" + substring[4];

                switch (caseString)
                {
                    case "Pre2020":
                        AddFolderPermissions(item /*+ @"\Pre2020"*/, "Secretaries", rights, controlType);
                        break;
                    case "1 Meta Data\\2 TendersProposals":
                        AddFolderPermissions(item /*+ @"\1 Meta Data\2 TendersProposals"*/, "Secretaries", rights, controlType);
                        break;
                    case "1 Meta Data\\3 Agreements":
                        AddFolderPermissions(item /*+ @"\1 Meta Data\3 Agreements"*/, "Secretaries", rights, controlType);
                        break;
                    case "1 Meta Data\\4 Billing":
                        AddFolderPermissions(item /*+ @"\1 Meta Data\4 Billing"*/, "Secretaries", rights, controlType);
                        break;
                    case "1 Meta Data\\5 Client Feedback":
                        AddFolderPermissions(item /*+ @"\1 Meta Data\5 Client Feedback"*/, "Secretaries", rights, controlType);
                        break;
                    default:
                        //do nothing
                        break;
                }
            }
        }
        catch (Exception exception)
        {
            throw exception;
        }
    }

    // Adds an ACL entry on the specified directory for the specified account.
    private static void AddDirectorySecurity(string FileName, string Account, FileSystemRights Rights, AccessControlType ControlType, string folderType)
    {
        if (folderType == "client")
        {
            //Actual department permissions.
            AddFolderPermissions(FileName + @"\Pre2020", Account, Rights, ControlType);
            AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Committee Packs", Account, Rights, ControlType);
            AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\External Reports", Account, Rights, ControlType);
            AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Policy Documentation", Account, Rights, ControlType);
            AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Regulatory", Account, Rights, ControlType);
            AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Reinsurance Agreements", Account, Rights, ControlType);
            AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Scheme Rules", Account, Rights, ControlType);
            AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Tables\Other", Account, Rights, ControlType);
            AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Tables\Premium Rates", Account, Rights, ControlType);
            AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Tables\PUP Values", Account, Rights, ControlType);
            AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Tables\Reinsurance Rates", Account, Rights, ControlType);
            AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Tables\Surrender Values", Account, Rights, ControlType);
            AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\QED Documentation\Client Notes", Account, Rights, ControlType);
            AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\QED Documentation\Process Documentation", Account, Rights, ControlType);
            AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail", Account, Rights, ControlType);
            AddFolderPermissions(FileName + @"\1 Meta Data\2 TendersProposals", Account, Rights, ControlType);
            AddFolderPermissions(FileName + @"\1 Meta Data\3 Agreements", Account, Rights, ControlType);
            AddFolderPermissions(FileName + @"\1 Meta Data\4 Billing", Account, Rights, ControlType);
            AddFolderPermissions(FileName + @"\1 Meta Data\5 Client Feedback", Account, Rights, ControlType);

            //Secretaries
            AddFolderPermissions(FileName + @"\Pre2020", "Secretaries", Rights, ControlType);
            AddFolderPermissions(FileName + @"\1 Meta Data\2 TendersProposals", "Secretaries", Rights, ControlType);
            AddFolderPermissions(FileName + @"\1 Meta Data\3 Agreements", "Secretaries", Rights, ControlType);
            AddFolderPermissions(FileName + @"\1 Meta Data\4 Billing", "Secretaries", Rights, ControlType);
            AddFolderPermissions(FileName + @"\1 Meta Data\5 Client Feedback", "Secretaries", Rights, ControlType);

            //Extended Group for department
            if ((Account == "B02") || (Account == "B03") || (Account == "B04") || (Account == "B05") || (Account == "B06") || (Account == "B10"))
            {
                AddFolderPermissions(FileName + @"\Pre2020", Account + " Extended", Rights, ControlType);
                AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Committee Packs", Account + " Extended", Rights, ControlType);
                AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\External Reports", Account + " Extended", Rights, ControlType);
                AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Policy Documentation", Account + " Extended", Rights, ControlType);
                AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Regulatory", Account + " Extended", Rights, ControlType);
                AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Reinsurance Agreements", Account + " Extended", Rights, ControlType);
                AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Scheme Rules", Account + " Extended", Rights, ControlType);
                AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Tables\Other", Account + " Extended", Rights, ControlType);
                AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Tables\Premium Rates", Account + " Extended", Rights, ControlType);
                AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Tables\PUP Values", Account + " Extended", Rights, ControlType);
                AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Tables\Reinsurance Rates", Account + " Extended", Rights, ControlType);
                AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Tables\Surrender Values", Account + " Extended", Rights, ControlType);
                AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\QED Documentation\Client Notes", Account + " Extended", Rights, ControlType);
                AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\QED Documentation\Process Documentation", Account + " Extended", Rights, ControlType);
                AddFolderPermissions(FileName + @"\1 Meta Data\2 TendersProposals", Account + " Extended", Rights, ControlType);
                AddFolderPermissions(FileName + @"\1 Meta Data\3 Agreements", Account + " Extended", Rights, ControlType);
                AddFolderPermissions(FileName + @"\1 Meta Data\4 Billing", Account + " Extended", Rights, ControlType);
                AddFolderPermissions(FileName + @"\1 Meta Data\5 Client Feedback", Account + " Extended", Rights, ControlType);
            }
        }
        else if (folderType == "project")
        {
            //Actual department permissions.
            AddFolderPermissions(FileName + @"\1 Audit Trail", Account + " Extended", Rights, ControlType);
            AddFolderPermissions(FileName + @"\2 Received", Account + " Extended", Rights, ControlType);
            AddFolderPermissions(FileName + @"\3 Workings", Account + " Extended", Rights, ControlType);
            AddFolderPermissions(FileName + @"\4 Results\1 Reports and Letters", Account + " Extended", Rights, ControlType);
            AddFolderPermissions(FileName + @"\4 Results\2 Presentations", Account + " Extended", Rights, ControlType);
            AddFolderPermissions(FileName + @"\4 Results\3 Emails", Account + " Extended", Rights, ControlType);
            AddFolderPermissions(FileName + @"\4 Results\4 Returns", Account + " Extended", Rights, ControlType);
            AddFolderPermissions(FileName + @"\4 Results\5 Other", Account + " Extended", Rights, ControlType);
            AddFolderPermissions(FileName + @"\5 Other", Account + " Extended", Rights, ControlType);


            //Secretaries
            AddFolderPermissions(FileName + @"\4 Results\1 Reports and Letters", Account + " Extended", Rights, ControlType);
            AddFolderPermissions(FileName + @"\4 Results\2 Presentations", Account + " Extended", Rights, ControlType);
            AddFolderPermissions(FileName + @"\4 Results\3 Emails", Account + " Extended", Rights, ControlType);
            AddFolderPermissions(FileName + @"\4 Results\4 Returns", Account + " Extended", Rights, ControlType);
            AddFolderPermissions(FileName + @"\4 Results\5 Other", Account + " Extended", Rights, ControlType);

            //Extended Group for department
            if ((Account == "B02") || (Account == "B03") || (Account == "B04") || (Account == "B05") || (Account == "B06") || (Account == "B10"))
            {
                AddFolderPermissions(FileName + @"\1 Audit Trail", Account + " Extended", Rights, ControlType);
                AddFolderPermissions(FileName + @"\2 Received", Account + " Extended", Rights, ControlType);
                AddFolderPermissions(FileName + @"\3 Workings", Account + " Extended", Rights, ControlType);
                AddFolderPermissions(FileName + @"\4 Results\1 Reports and Letters", Account + " Extended", Rights, ControlType);
                AddFolderPermissions(FileName + @"\4 Results\2 Presentations", Account + " Extended", Rights, ControlType);
                AddFolderPermissions(FileName + @"\4 Results\3 Emails", Account + " Extended", Rights, ControlType);
                AddFolderPermissions(FileName + @"\4 Results\4 Returns", Account + " Extended", Rights, ControlType);
                AddFolderPermissions(FileName + @"\4 Results\5 Other", Account + " Extended", Rights, ControlType);
                AddFolderPermissions(FileName + @"\5 Other", Account + " Extended", Rights, ControlType);
            }
        }
    }

    private static void AddFolderPermissions(string FileName, string Account, FileSystemRights Rights, AccessControlType ControlType)
    {
        try
        {
            //FileName = FileName.Replace(@"\\", @"\");
            Account = Account.Replace(@"\\", @"\");
            if (Directory.Exists(FileName))
            {
                DirectoryInfo dInfo;
                DirectorySecurity dSecurity;
                // Create a new DirectoryInfo object.
                dInfo = new DirectoryInfo(FileName);
                // Get a DirectorySecurity object that represents the 
                // current security settings.
                dSecurity = dInfo.GetAccessControl();
                // Add the FileSystemAccessRule to the security settings. 
                dSecurity.AddAccessRule(new FileSystemAccessRule(Account, Rights, ControlType));
                dSecurity.AddAccessRule(new FileSystemAccessRule(Account, Rights, InheritanceFlags.ContainerInherit, PropagationFlags.InheritOnly, ControlType));
                dSecurity.AddAccessRule(new FileSystemAccessRule(Account, Rights, InheritanceFlags.ObjectInherit, PropagationFlags.InheritOnly, ControlType));

                // Set the new access settings.
                dInfo.SetAccessControl(dSecurity);
            }
        }


        catch (Exception e)
        {
            Console.WriteLine("Exception: {0}", e.ToString());
        }
    }

    private static void RemoveFolderPermissions(string FileName, string Account, FileSystemRights Rights, AccessControlType ControlType)
    {
        try
        {
            FileName = FileName.Replace(@"\\", @"\");
            if (Directory.Exists(FileName))
            {
                DirectoryInfo dInfo;
                DirectorySecurity dSecurity;
                // Create a new DirectoryInfo object.
                dInfo = new DirectoryInfo(FileName);
                // Get a DirectorySecurity object that represents the 
                // current security settings.
                dSecurity = dInfo.GetAccessControl();
                // Add the FileSystemAccessRule to the security settings. 
                dSecurity.RemoveAccessRule(new FileSystemAccessRule(Account, Rights, ControlType));
                dSecurity.RemoveAccessRule(new FileSystemAccessRule(Account, Rights, InheritanceFlags.ContainerInherit, PropagationFlags.InheritOnly, ControlType));
                dSecurity.RemoveAccessRule(new FileSystemAccessRule(Account, Rights, InheritanceFlags.ObjectInherit, PropagationFlags.InheritOnly, ControlType));

                // Set the new access settings.
                dInfo.SetAccessControl(dSecurity);
            }
        }

        catch (Exception e)
        {
            Console.WriteLine("Exception: {0}", e.ToString());
        }
    }

    // Removes an ACL entry on the specified directory for the specified account.
    private static void RemoveDirectorySecurity(string FileName, string Account, FileSystemRights Rights, AccessControlType ControlType, string folderType)
    {
        if (folderType == "client")
        {
            //Actual department permissions.

            RemoveFolderPermissions(FileName + @"\Pre2020", Account, Rights, ControlType);
            RemoveFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Committee Packs", Account, Rights, ControlType);
            RemoveFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\External Reports", Account, Rights, ControlType);
            RemoveFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Policy Documentation", Account, Rights, ControlType);
            RemoveFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Regulatory", Account, Rights, ControlType);
            RemoveFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Reinsurance Agreements", Account, Rights, ControlType);
            RemoveFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Scheme Rules", Account, Rights, ControlType);
            RemoveFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Tables\Other", Account, Rights, ControlType);
            RemoveFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Tables\Premium Rates", Account, Rights, ControlType);
            RemoveFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Tables\PUP Values", Account, Rights, ControlType);
            RemoveFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Tables\Reinsurance Rates", Account, Rights, ControlType);
            RemoveFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Tables\Surrender Values", Account, Rights, ControlType);
            RemoveFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\QED Documentation\Client Notes", Account, Rights, ControlType);
            RemoveFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\QED Documentation\Process Documentation", Account, Rights, ControlType);
            RemoveFolderPermissions(FileName + @"\1 Meta Data\2 TendersProposals", Account, Rights, ControlType);
            RemoveFolderPermissions(FileName + @"\1 Meta Data\3 Agreements", Account, Rights, ControlType);
            RemoveFolderPermissions(FileName + @"\1 Meta Data\4 Billing", Account, Rights, ControlType);
            RemoveFolderPermissions(FileName + @"\1 Meta Data\5 Client Feedback", Account, Rights, ControlType);

            //Extended Group for department
            if ((Account == "B02") || (Account == "B03") || (Account == "B04") || (Account == "B05") || (Account == "B06") || (Account == "B10"))
            {
                RemoveFolderPermissions(FileName + @"\Pre2020", Account + " Extended", Rights, ControlType);
                RemoveFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Committee Packs", Account + " Extended", Rights, ControlType);
                RemoveFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\External Reports", Account + " Extended", Rights, ControlType);
                RemoveFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Policy Documentation", Account + " Extended", Rights, ControlType);
                RemoveFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Regulatory", Account + " Extended", Rights, ControlType);
                RemoveFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Reinsurance Agreements", Account + " Extended", Rights, ControlType);
                RemoveFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Scheme Rules", Account + " Extended", Rights, ControlType);
                RemoveFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Tables\Other", Account + " Extended", Rights, ControlType);
                RemoveFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Tables\Premium Rates", Account + " Extended", Rights, ControlType);
                RemoveFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Tables\PUP Values", Account + " Extended", Rights, ControlType);
                RemoveFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Tables\Reinsurance Rates", Account + " Extended", Rights, ControlType);
                RemoveFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Tables\Surrender Values", Account + " Extended", Rights, ControlType);
                RemoveFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\QED Documentation\Client Notes", Account + " Extended", Rights, ControlType);
                RemoveFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\QED Documentation\Process Documentation", Account + " Extended", Rights, ControlType);
                RemoveFolderPermissions(FileName + @"\1 Meta Data\2 TendersProposals", Account + " Extended", Rights, ControlType);
                RemoveFolderPermissions(FileName + @"\1 Meta Data\3 Agreements", Account + " Extended", Rights, ControlType);
                RemoveFolderPermissions(FileName + @"\1 Meta Data\4 Billing", Account + " Extended", Rights, ControlType);
                RemoveFolderPermissions(FileName + @"\1 Meta Data\5 Client Feedback", Account + " Extended", Rights, ControlType);
            }
        }
        else if (folderType == "project")
        {
            //Actual department permissions.
            RemoveFolderPermissions(FileName + @"\1 Audit Trail", Account + " Extended", Rights, ControlType);
            RemoveFolderPermissions(FileName + @"\2 Received", Account + " Extended", Rights, ControlType);
            RemoveFolderPermissions(FileName + @"\3 Workings", Account + " Extended", Rights, ControlType);
            RemoveFolderPermissions(FileName + @"\4 Results\1 Reports and Letters", Account + " Extended", Rights, ControlType);
            RemoveFolderPermissions(FileName + @"\4 Results\2 Presentations", Account + " Extended", Rights, ControlType);
            RemoveFolderPermissions(FileName + @"\4 Results\3 Emails", Account + " Extended", Rights, ControlType);
            RemoveFolderPermissions(FileName + @"\4 Results\4 Returns", Account + " Extended", Rights, ControlType);
            RemoveFolderPermissions(FileName + @"\4 Results\5 Other", Account + " Extended", Rights, ControlType);
            RemoveFolderPermissions(FileName + @"\5 Other", Account + " Extended", Rights, ControlType);


            //Secretaries
            RemoveFolderPermissions(FileName + @"\4 Results\1 Reports and Letters", Account + " Extended", Rights, ControlType);
            RemoveFolderPermissions(FileName + @"\4 Results\2 Presentations", Account + " Extended", Rights, ControlType);
            RemoveFolderPermissions(FileName + @"\4 Results\3 Emails", Account + " Extended", Rights, ControlType);
            RemoveFolderPermissions(FileName + @"\4 Results\4 Returns", Account + " Extended", Rights, ControlType);
            RemoveFolderPermissions(FileName + @"\4 Results\5 Other", Account + " Extended", Rights, ControlType);

            //Extended Group for department
            if ((Account == "B02") || (Account == "B03") || (Account == "B04") || (Account == "B05") || (Account == "B06") || (Account == "B10"))
            {
                RemoveFolderPermissions(FileName + @"\1 Audit Trail", Account + " Extended", Rights, ControlType);
                RemoveFolderPermissions(FileName + @"\2 Received", Account + " Extended", Rights, ControlType);
                RemoveFolderPermissions(FileName + @"\3 Workings", Account + " Extended", Rights, ControlType);
                RemoveFolderPermissions(FileName + @"\4 Results\1 Reports and Letters", Account + " Extended", Rights, ControlType);
                RemoveFolderPermissions(FileName + @"\4 Results\2 Presentations", Account + " Extended", Rights, ControlType);
                RemoveFolderPermissions(FileName + @"\4 Results\3 Emails", Account + " Extended", Rights, ControlType);
                RemoveFolderPermissions(FileName + @"\4 Results\4 Returns", Account + " Extended", Rights, ControlType);
                RemoveFolderPermissions(FileName + @"\4 Results\5 Other", Account + " Extended", Rights, ControlType);
                RemoveFolderPermissions(FileName + @"\5 Other", Account + " Extended", Rights, ControlType);
            }
        }
    }
}
