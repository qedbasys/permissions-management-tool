﻿using PermissionsManagementToolFrontEnd.Models;
using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Reflection;
using System.Security.AccessControl;
using System.Text.RegularExpressions;

namespace PermissionsManagementToolFrontEnd.Helpers
{
    static class Common
    {
        internal static string invalidFileNameChars = new string(Path.GetInvalidPathChars());

        internal static bool IsValidFilename(string fileName)
        {
            invalidFileNameChars += @":/?*<>|" + "\"";
            Regex containsABadCharacter = new Regex("[" + Regex.Escape(invalidFileNameChars) + "]");
            if (containsABadCharacter.IsMatch(fileName))
            {
                return false;
            }

            return true;
        }

        internal static void CreateProjectSubDirectories(DirectoryInfo path)
        {
            try
            {
                for (int i = 1; i < 6; i++)
                {
                    string description = GetEnumDescription((ProjectSubDirEnums)i);
                    path.CreateSubdirectory(description);//i + " " + Enum.GetName(typeof(ProjectSubDirEnums), i));
                    if (i == 4)
                    {
                        string newPath = path.FullName + "\\" + description;//"\\" + i + " " + Enum.GetName(typeof(ProjectSubDirEnums), i);
                        DirectoryInfo newDirectoryInfo = new DirectoryInfo(newPath);
                        //do sub directories  for results
                        for (int j = 1; j < 6; j++)
                        {
                            string newDescription = GetEnumDescription((ResultsEnum)j);
                            newDirectoryInfo.CreateSubdirectory(newDescription);//Enum.GetName(typeof(ResultsEnum), j));
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        internal static void CreateClientSubDirectories(DirectoryInfo path)
        {
            try
            {
                for (int i = 1; i < 6; i++)
                {
                    string description = GetEnumDescription((ClientSubDirEnum)i);
                    path.CreateSubdirectory(description);
                    if (i == 1)
                    {
                        string newPath = path.FullName + "\\" + description;
                        DirectoryInfo newDirectoryInfo = new DirectoryInfo(newPath);
                        //do sub directories  for results
                        for (int j = 1; j < 3; j++)
                        {
                            string newDescription = GetEnumDescription((ClientMetaDataSubDir)j);
                            newDirectoryInfo.CreateSubdirectory(newDescription);
                            if (j == 1)
                            {
                                string subPath = path.FullName + "\\" + description + "\\" + newDescription;
                                DirectoryInfo newSubDir = new DirectoryInfo(subPath);
                                for (int k = 1; k < 9; k++)
                                {
                                    string forthDescription = GetEnumDescription((ClientMetaDataAuditTrailClientDocEnum)k);
                                    newSubDir.CreateSubdirectory(forthDescription);
                                    if (k == 8)
                                    {
                                        string tablesPath = path.FullName + "\\" + description + "\\" + newDescription + "\\" + forthDescription;
                                        DirectoryInfo directoryInfo = new DirectoryInfo(tablesPath);
                                        for (int l = 1; l < 6; l++)
                                        {
                                            string fifthDes = GetEnumDescription((TablesEnum)l);
                                            directoryInfo.CreateSubdirectory(fifthDes);
                                        }
                                    }
                                    else if (k == 1)
                                    {
                                        string tablesPath = path.FullName + "\\" + description + "\\" + newDescription + "\\" + forthDescription;
                                        DirectoryInfo directoryInfo = new DirectoryInfo(tablesPath);
                                        for (int l = 1; l < 4; l++)
                                        {
                                            string fifthDes = GetEnumDescription((CommitteePacksEnum)l);
                                            directoryInfo.CreateSubdirectory(fifthDes);
                                        }
                                    }
                                    else if (k == 4)
                                    {
                                        string tablesPath = path.FullName + "\\" + description + "\\" + newDescription + "\\" + forthDescription;
                                        DirectoryInfo directoryInfo = new DirectoryInfo(tablesPath);
                                        for (int l = 1; l < 4; l++)
                                        {
                                            directoryInfo.CreateSubdirectory("Sample Policy Documentation");
                                        }
                                    }
                                    else if (k == 5)
                                    {
                                        string tablesPath = path.FullName + "\\" + description + "\\" + newDescription + "\\" + forthDescription;
                                        DirectoryInfo directoryInfo = new DirectoryInfo(tablesPath);
                                        for (int l = 1; l < 4; l++)
                                        {
                                            directoryInfo.CreateSubdirectory("Quarterly Returns");
                                        }
                                    }
                                    else if (k == 6)
                                    {
                                        string tablesPath = path.FullName + "\\" + description + "\\" + newDescription + "\\" + forthDescription;
                                        DirectoryInfo directoryInfo = new DirectoryInfo(tablesPath);
                                        for (int l = 1; l < 4; l++)
                                        {
                                            directoryInfo.CreateSubdirectory("Package 174 Reinsurance Rates");
                                        }

                                    }
                                }
                            }
                            else if (j == 2)
                            {
                                string subPath = path.FullName + "\\" + description + "\\" + newDescription;
                                DirectoryInfo newSubDir = new DirectoryInfo(subPath);
                                for (int k = 1; k < 3; k++)
                                {
                                    string qedDocumentation = GetEnumDescription((QEDDocumentationEnum)k);
                                    newSubDir.CreateSubdirectory(qedDocumentation);
                                }
                            }
                        }
                    }
                    else if (i == 3)
                    {
                        string newPath = path.FullName + "\\" + description;
                        DirectoryInfo newDirectoryInfo = new DirectoryInfo(newPath);
                        //do sub directories  for results
                        for (int j = 1; j < 2; j++)
                        {
                            newDirectoryInfo.CreateSubdirectory("2020");
                        }
                    }
                    else if (i == 4)
                    {
                        string newPath = path.FullName + "\\" + description;
                        DirectoryInfo newDirectoryInfo = new DirectoryInfo(newPath);
                        //do sub directories  for results
                        for (int j = 1; j < 2; j++)
                        {
                            newDirectoryInfo.CreateSubdirectory("2020");
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }


        public static string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes = fi.GetCustomAttributes(typeof(DescriptionAttribute), false) as DescriptionAttribute[];

            if (attributes != null && attributes.Any())
            {
                return attributes.First().Description;
            }

            return value.ToString();
        }

        internal static void CopyProjectMeta(DirectoryInfo source, DirectoryInfo target)
        {
            try
            {
                // Copy each subdirectory using recursion.
                foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
                {
                    DirectoryInfo nextTargetSubDir = target.CreateSubdirectory(diSourceSubDir.Name);
                    CopyProjectMeta(diSourceSubDir, nextTargetSubDir);
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        internal static void SetPermissionsInActiveDirectory(string permissions)
        {
            try
            {
                string[] permissionssubString = permissions.Split(';');
                string path = permissionssubString[0];
                string department = permissionssubString[1];
                string accessControl = permissionssubString[2];
                string fileSystemRight = permissionssubString[3];
                string accessRights = permissionssubString[4];
                string folderType = permissionssubString[5];

                bool success = SetClientPermissions(path, department, accessControl, fileSystemRight, accessRights, folderType);
                //string ipAddress = "10.45.120.66";
                //Connect(ipAddress, permissions);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        private static bool SetClientPermissions(string path, string department, string accessControl, string fileSystemRight, string accessRights, string folderType)
        {
            try
            {
                //string DirectoryName = @"C:\Inetpub\ftproot";
                //string DirectoryName = serverPrefixPath + path;
                string DirectoryName = "U:\\" + path;

                Console.WriteLine("Adding access control entry for " + DirectoryName);
                accessControl = accessControl.ToLower();
                fileSystemRight = fileSystemRight.ToLower();
                accessRights = accessRights.ToLower();
                folderType = folderType.ToLower();

                if (accessControl == "allow")
                {
                }
                else if (accessControl == "deny")
                {
                }

                if (fileSystemRight == "read")
                {
                }
                else if (fileSystemRight == "write")
                {
                }

                if (accessRights == "add")
                {
                    AddDirectorySecuritySub(DirectoryName, department, FileSystemRights.Modify, AccessControlType.Allow, folderType);
                    // AddDirectorySecurity(DirectoryName, @"qed.local\" + department, FileSystemRights.Modify, AccessControlType.Allow, folderType);
                }
                else if (accessRights == "remove")
                {
                    // RemoveDirectorySecurity(DirectoryName, @"qed.local\" + department, FileSystemRights.Modify, AccessControlType.Allow, folderType);
                }

                // Add the access control entry to the directory.
                //AddDirectorySecurity(DirectoryName, @"QED\" + department, fileSystemRightType, accessControlType);
                //Console.WriteLine("Removing access control entry from " + DirectoryName);
                // Remove the access control entry from the directory.
                //RemoveDirectorySecurity(DirectoryName, @"QED\" + department, FileSystemRights.Write, AccessControlType.Allow);
                Console.WriteLine("Done.");
                Console.ReadLine();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Console.ReadLine();
                throw e;
            }
        }

        private static void AddDirectorySecuritySub(string FileName, string Account, FileSystemRights Rights, AccessControlType ControlType, string folderType)
        {
            try
            {
                string[] subdirectoryEntries = Directory.GetDirectories(FileName);
                foreach (string item in subdirectoryEntries)
                {
                    AddFolderPermissions(FileName, Account, Rights, ControlType);
                    if ((Account == "B02") || (Account == "B03") || (Account == "B04") || (Account == "B05") || (Account == "B06") || (Account == "B10"))
                    {
                        AddFolderPermissions(FileName, Account + " Extended", Rights, ControlType);
                    }
                    AddDirectorySecuritySub(item, Account, Rights, ControlType, folderType);
                    //add secretaries
                    if (folderType == "client")
                    {
                        AddSecretaries(item, Rights, ControlType);
                    }
                    else if (folderType == "project")
                    {
                        AddProjectSecretaries(item, Rights, ControlType);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        private static void AddProjectSecretaries(string item, FileSystemRights rights, AccessControlType controlType)
        {
            try
            {
                string[] substring = item.Split('\\');
                if (substring.Count() == 7)
                {
                    string caseString = substring[5] + "\\" + substring[6];

                    switch (caseString)
                    {
                        case "4 Results\\1 Reports and Letters":
                            AddFolderPermissions(item /*+ @"\Pre2020"*/, "Secretaries", rights, controlType);
                            break;
                        case "4 Results\\2 Presentations":
                            AddFolderPermissions(item /*+ @"\1 Meta Data\2 TendersProposals"*/, "Secretaries", rights, controlType);
                            break;
                        case "4 Results\\3 Emails":
                            AddFolderPermissions(item /*+ @"\1 Meta Data\3 Agreements"*/, "Secretaries", rights, controlType);
                            break;
                        case "4 Results\\4 Returns":
                            AddFolderPermissions(item /*+ @"\1 Meta Data\4 Billing"*/, "Secretaries", rights, controlType);
                            break;
                        case "4 Results\\5 Other":
                            AddFolderPermissions(item /*+ @"\1 Meta Data\5 Client Feedback"*/, "Secretaries", rights, controlType);
                            break;
                        default:
                            //do nothing
                            break;
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }


        private static void AddSecretaries(string item, FileSystemRights rights, AccessControlType controlType)
        {
            try
            {
                string[] substring = item.Split('\\');
                if (substring.Count() == 4)
                {
                    if (substring[3] == "Pre2020")
                    {
                        AddFolderPermissions(item /*+ @"\Pre2020"*/, "Secretaries", rights, controlType); 
                    }
                }
                if (substring.Count() == 5)
                {
                    string caseString = substring[3] + "\\" + substring[4];

                    switch (caseString)
                    {
                        case "Pre2020":
                            AddFolderPermissions(item /*+ @"\Pre2020"*/, "Secretaries", rights, controlType);
                            break;
                        case "1 Meta Data\\2 TendersProposals":
                            AddFolderPermissions(item /*+ @"\1 Meta Data\2 TendersProposals"*/, "Secretaries", rights, controlType);
                            break;
                        case "1 Meta Data\\3 Agreements":
                            AddFolderPermissions(item /*+ @"\1 Meta Data\3 Agreements"*/, "Secretaries", rights, controlType);
                            break;
                        case "1 Meta Data\\4 Billing":
                            AddFolderPermissions(item /*+ @"\1 Meta Data\4 Billing"*/, "Secretaries", rights, controlType);
                            break;
                        case "1 Meta Data\\5 Client Feedback":
                            AddFolderPermissions(item /*+ @"\1 Meta Data\5 Client Feedback"*/, "Secretaries", rights, controlType);
                            break;
                        default:
                            //do nothing
                            break;
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        private static void AddDirectorySecurity(string FileName, string Account, FileSystemRights Rights, AccessControlType ControlType, string folderType)
        {
            if (folderType == "client")
            {
                //Actual department permissions.
                AddFolderPermissions(FileName + @"\Pre2020", Account, Rights, ControlType);
                AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Committee Packs", Account, Rights, ControlType);
                AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\External Reports", Account, Rights, ControlType);
                AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Policy Documentation", Account, Rights, ControlType);
                AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Regulatory", Account, Rights, ControlType);
                AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Reinsurance Agreements", Account, Rights, ControlType);
                AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Scheme Rules", Account, Rights, ControlType);
                AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Tables\Other", Account, Rights, ControlType);
                AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Tables\Premium Rates", Account, Rights, ControlType);
                AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Tables\PUP Values", Account, Rights, ControlType);
                AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Tables\Reinsurance Rates", Account, Rights, ControlType);
                AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Tables\Surrender Values", Account, Rights, ControlType);
                AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\QED Documentation\Client Notes", Account, Rights, ControlType);
                AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\QED Documentation\Process Documentation", Account, Rights, ControlType);
                AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail", Account, Rights, ControlType);

                AddFolderPermissions(FileName + @"\1 Meta Data\2 TendersProposals", Account, Rights, ControlType);
                AddFolderPermissions(FileName + @"\1 Meta Data\3 Agreements", Account, Rights, ControlType);
                AddFolderPermissions(FileName + @"\1 Meta Data\4 Billing", Account, Rights, ControlType);
                AddFolderPermissions(FileName + @"\1 Meta Data\5 Client Feedback", Account, Rights, ControlType);

                //Secretaries
                AddFolderPermissions(FileName + @"\Pre2020", "Secretaries", Rights, ControlType);
                AddFolderPermissions(FileName + @"\1 Meta Data\2 TendersProposals", "Secretaries", Rights, ControlType);
                AddFolderPermissions(FileName + @"\1 Meta Data\3 Agreements", "Secretaries", Rights, ControlType);
                AddFolderPermissions(FileName + @"\1 Meta Data\4 Billing", "Secretaries", Rights, ControlType);
                AddFolderPermissions(FileName + @"\1 Meta Data\5 Client Feedback", "Secretaries", Rights, ControlType);

                //Extended Group for department
                if ((Account.Contains("B02")) || (Account.Contains("B03")) || (Account.Contains("B04")) || (Account.Contains("B05")) || (Account.Contains("B06")) || (Account.Contains("B10")))
                {
                    AddFolderPermissions(FileName + @"\Pre2020", Account + " Extended", Rights, ControlType);
                    AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Committee Packs", Account + " Extended", Rights, ControlType);
                    AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\External Reports", Account + " Extended", Rights, ControlType);
                    AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Policy Documentation", Account + " Extended", Rights, ControlType);
                    AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Regulatory", Account + " Extended", Rights, ControlType);
                    AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Reinsurance Agreements", Account + " Extended", Rights, ControlType);
                    AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Scheme Rules", Account + " Extended", Rights, ControlType);
                    AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Tables\Other", Account + " Extended", Rights, ControlType);
                    AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Tables\Premium Rates", Account + " Extended", Rights, ControlType);
                    AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Tables\PUP Values", Account + " Extended", Rights, ControlType);
                    AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Tables\Reinsurance Rates", Account + " Extended", Rights, ControlType);
                    AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\Client Documentation\Tables\Surrender Values", Account + " Extended", Rights, ControlType);
                    AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\QED Documentation\Client Notes", Account + " Extended", Rights, ControlType);
                    AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail\QED Documentation\Process Documentation", Account + " Extended", Rights, ControlType);
                    AddFolderPermissions(FileName + @"\1 Meta Data\1 Audit Trail", Account, Rights, ControlType);

                    AddFolderPermissions(FileName + @"\1 Meta Data\2 TendersProposals", Account + " Extended", Rights, ControlType);
                    AddFolderPermissions(FileName + @"\1 Meta Data\3 Agreements", Account + " Extended", Rights, ControlType);
                    AddFolderPermissions(FileName + @"\1 Meta Data\4 Billing", Account + " Extended", Rights, ControlType);
                    AddFolderPermissions(FileName + @"\1 Meta Data\5 Client Feedback", Account + " Extended", Rights, ControlType);
                }
            }
            else if (folderType == "project")
            {
                //Actual department permissions.
                AddFolderPermissions(FileName + @"\1 Audit Trail", Account + " Extended", Rights, ControlType);
                AddFolderPermissions(FileName + @"\2 Received", Account + " Extended", Rights, ControlType);
                AddFolderPermissions(FileName + @"\3 Workings", Account + " Extended", Rights, ControlType);
                AddFolderPermissions(FileName + @"\4 Results\1 Reports and Letters", Account + " Extended", Rights, ControlType);
                AddFolderPermissions(FileName + @"\4 Results\2 Presentations", Account + " Extended", Rights, ControlType);
                AddFolderPermissions(FileName + @"\4 Results\3 Emails", Account + " Extended", Rights, ControlType);
                AddFolderPermissions(FileName + @"\4 Results\4 Returns", Account + " Extended", Rights, ControlType);
                AddFolderPermissions(FileName + @"\4 Results\5 Other", Account + " Extended", Rights, ControlType);
                AddFolderPermissions(FileName + @"\5 Other", Account + " Extended", Rights, ControlType);


                //Secretaries
                AddFolderPermissions(FileName + @"\4 Results\1 Reports and Letters", Account + " Extended", Rights, ControlType);
                AddFolderPermissions(FileName + @"\4 Results\2 Presentations", Account + " Extended", Rights, ControlType);
                AddFolderPermissions(FileName + @"\4 Results\3 Emails", Account + " Extended", Rights, ControlType);
                AddFolderPermissions(FileName + @"\4 Results\4 Returns", Account + " Extended", Rights, ControlType);
                AddFolderPermissions(FileName + @"\4 Results\5 Other", Account + " Extended", Rights, ControlType);

                //Extended Group for department
                if ((Account == "B02") || (Account == "B03") || (Account == "B04") || (Account == "B05") || (Account == "B06") || (Account == "B10"))
                {
                    AddFolderPermissions(FileName + @"\1 Audit Trail", Account + " Extended", Rights, ControlType);
                    AddFolderPermissions(FileName + @"\2 Received", Account + " Extended", Rights, ControlType);
                    AddFolderPermissions(FileName + @"\3 Workings", Account + " Extended", Rights, ControlType);
                    AddFolderPermissions(FileName + @"\4 Results\1 Reports and Letters", Account + " Extended", Rights, ControlType);
                    AddFolderPermissions(FileName + @"\4 Results\2 Presentations", Account + " Extended", Rights, ControlType);
                    AddFolderPermissions(FileName + @"\4 Results\3 Emails", Account + " Extended", Rights, ControlType);
                    AddFolderPermissions(FileName + @"\4 Results\4 Returns", Account + " Extended", Rights, ControlType);
                    AddFolderPermissions(FileName + @"\4 Results\5 Other", Account + " Extended", Rights, ControlType);
                    AddFolderPermissions(FileName + @"\5 Other", Account + " Extended", Rights, ControlType);
                }
            }
        }

        private static void AddFolderPermissions(string FileName, string Account, FileSystemRights Rights, AccessControlType ControlType)
        {
            try
            {
                //FileName = FileName.Replace(@"\\", @"\");
                Account = Account.Replace(@"\\", @"\");
                if (Directory.Exists(FileName))
                {
                    DirectoryInfo dInfo;
                    DirectorySecurity dSecurity;
                    // Create a new DirectoryInfo object.
                    dInfo = new DirectoryInfo(FileName);
                    // Get a DirectorySecurity object that represents the 
                    // current security settings.
                    dSecurity = dInfo.GetAccessControl();
                    // Add the FileSystemAccessRule to the security settings. 
                    dSecurity.AddAccessRule(new FileSystemAccessRule(Account, Rights, ControlType));
                    dSecurity.AddAccessRule(new FileSystemAccessRule(Account, Rights, InheritanceFlags.ContainerInherit, PropagationFlags.InheritOnly, ControlType));
                    dSecurity.AddAccessRule(new FileSystemAccessRule(Account, Rights, InheritanceFlags.ObjectInherit, PropagationFlags.InheritOnly, ControlType));

                    // Set the new access settings.
                    dInfo.SetAccessControl(dSecurity);
                }
            }


            catch (Exception e)
            {
                throw e;
                //Console.WriteLine("Exception: {0}", e.ToString());
            }
        }


        static void Connect(string server, string message)
        {
            try
            {
                int port = 13000;
                TcpClient client = new TcpClient(server, port);
                NetworkStream stream = client.GetStream();

                // Translate the Message into ASCII.
                byte[] data = System.Text.Encoding.ASCII.GetBytes(message);
                // Send the message to the connected TcpServer. 
                stream.Write(data, 0, data.Length);
                data = new byte[256];
                string response = string.Empty;
                // Read the Tcp Server Response Bytes.
                int bytes = stream.Read(data, 0, data.Length);
                response = System.Text.Encoding.ASCII.GetString(data, 0, bytes);

                stream.Close();
                client.Close();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}
