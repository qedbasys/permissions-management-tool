﻿using DataAccessLayer;
using PermissionsManagementToolFrontEnd.Models;
using System;
using System.Collections.Generic;
using System.Data;

namespace PermissionsManagementToolFrontEnd.Controllers
{
    class CountriesController
    {
        internal static CountriesModel GetCountryByName(string countryName)
        {

            try
            {
                string sql = "SELECT * FROM countries WHERE name = '" + countryName + "'";
                DataTable accessTypes = PostgreSqlClass.GetAllDataTable(sql);
                CountriesModel model = new CountriesModel();
                for (int i = 0; i < accessTypes.Rows.Count; i++)
                {
                    model = new CountriesModel
                    {
                        Id = Convert.ToInt32(accessTypes.Rows[i]["id"]),
                        Name = accessTypes.Rows[i]["name"].ToString(),
                    };
                }
                return model;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        internal static List<CountriesModel> GetCountries()
        {
            try
            {
                string sql = "SELECT * FROM countries";
                DataTable countries = PostgreSqlClass.GetAllDataTable(sql);
                List<CountriesModel> countriesModelList = new List<CountriesModel>();
                for (int i = 0; i < countries.Rows.Count; i++)
                {
                    CountriesModel countriesModel = new CountriesModel
                    {
                        Id = Convert.ToInt32(countries.Rows[i]["id"]),
                        Name = countries.Rows[i]["name"].ToString(),
                    };

                    countriesModelList.Add(countriesModel);
                }
                return countriesModelList;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        internal static CountriesModel InsertIntoCountries(string countryName)
        {
            try
            {
                string columnString = "name";
                string valuesString = "'" + countryName + "'";
                int countryId = PostgreSqlClass.InsertData("countries", columnString, valuesString);
                CountriesModel countryModel = new CountriesModel
                {
                    Id = countryId,
                    Name = countryName,
                };
                return countryModel;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}
