﻿using DataAccessLayer;
using PermissionsManagementToolFrontEnd.Models;
using System;
using System.Collections.Generic;
using System.Data;

namespace PermissionsManagementToolFrontEnd.Controllers
{
    class UsersController
    {
        internal static UsersModel GetUserById(int userId)
        {
            try
            {
                string sql = "SELECT * FROM users WHERE id = " + userId;
                DataTable userSet = PostgreSqlClass.GetAllDataTable(sql);
                UsersModel model = new UsersModel();
                for (int i = 0; i < userSet.Rows.Count; i++)
                {
                    model = new UsersModel
                    {
                        Id = Convert.ToInt32(userSet.Rows[i]["id"]),
                        FirstName = userSet.Rows[i]["first_name"].ToString(),
                        LastName = userSet.Rows[i]["last_name"].ToString(),
                        UserId = userSet.Rows[i]["user_id"].ToString(),
                        Email = userSet.Rows[i]["email"].ToString(),
                        UserName = userSet.Rows[i]["username"].ToString(),
                    };
                }
                return model;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        internal static UsersModel GetUserFromDb(string firstName, string lastName)
        {
            try
            {
                string sql = "SELECT * FROM users WHERE first_name = '" + firstName + "' AND last_name='" + lastName + "'";
                DataTable userSet = PostgreSqlClass.GetAllDataTable(sql);
                UsersModel model = new UsersModel();
                for (int i = 0; i < userSet.Rows.Count; i++)
                {
                    model = new UsersModel
                    {
                        Id = Convert.ToInt32(userSet.Rows[i]["id"]),
                        FirstName = userSet.Rows[i]["first_name"].ToString(),
                        LastName = userSet.Rows[i]["last_name"].ToString(),
                        UserId = userSet.Rows[i]["user_id"].ToString(),
                        Email = userSet.Rows[i]["email"].ToString(),
                        UserName = userSet.Rows[i]["username"].ToString(),
                    };
                }
                return model;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        internal static List<UsersModel> GetAllUsers()
        {
            try
            {
                string sql = "SELECT * FROM users";
                // List<object>managers = PostgreSqlClass.GetAllDataTable(sql);
                DataTable managers = PostgreSqlClass.GetAllDataTable(sql);
                List<UsersModel> usersModelList = new List<UsersModel>();
                for (int i = 0; i < managers.Rows.Count; i++)
                {
                    UsersModel usersModel = new UsersModel
                    {
                        Id = Convert.ToInt32(managers.Rows[i]["id"]),
                        UserId = managers.Rows[i]["user_id"].ToString(),
                        FirstName = managers.Rows[i]["first_name"].ToString(),
                        LastName = managers.Rows[i]["last_name"].ToString(),
                        Email = managers.Rows[i]["email"].ToString(),
                        UserName = managers.Rows[i]["username"].ToString(),
                        ClientManagerName = managers.Rows[i]["first_name"].ToString() + " " + managers.Rows[i]["last_name"].ToString()
                    };

                    usersModelList.Add(usersModel);
                }
                return usersModelList;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        internal static UsersModel GetUserByUserName(string username)
        {
            try
            {
                string sql = "SELECT * FROM users WHERE username = '" + username + "'";
                DataTable userSet = PostgreSqlClass.GetAllDataTable(sql);
                UsersModel model = new UsersModel();
                for (int i = 0; i < userSet.Rows.Count; i++)
                {
                    model = new UsersModel
                    {
                        Id = Convert.ToInt32(userSet.Rows[i]["id"]),
                        FirstName = userSet.Rows[i]["first_name"].ToString(),
                        LastName = userSet.Rows[i]["last_name"].ToString(),
                        UserId = userSet.Rows[i]["user_id"].ToString(),
                        Email = userSet.Rows[i]["email"].ToString(),
                        UserName = userSet.Rows[i]["username"].ToString(),
                    };
                }
                return model;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        internal static bool IsUserInRole(int userId, int directorId)
        {
            try
            {
                string query = "SELECT * FROM users as u JOIN user_organisational_structure o ON u.id = o.user_id JOIN organisational_structure s ON o.organisational_structure_id = s.id WHERE s.id = 6 AND u.id=" + userId;
                return PostgreSqlClass.CheckIfExists(query);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}
