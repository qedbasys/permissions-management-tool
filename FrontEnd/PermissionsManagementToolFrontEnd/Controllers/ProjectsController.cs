﻿using DataAccessLayer;
using PermissionsManagementToolFrontEnd.Models;
using System;
using System.Collections.Generic;
using System.Data;

namespace PermissionsManagementToolFrontEnd.Controllers
{
    class ProjectsController
    {
        public static ProjectsModel GetProjectByName(string projectName)
        {
            try
            {
                ProjectsModel model = new ProjectsModel();
                string sql = "SELECT * FROM projects WHERE name = '" + projectName + "'";
                DataTable projectsSet = PostgreSqlClass.GetAllDataTable(sql);

                for (int i = 0; i < projectsSet.Rows.Count; i++)
                {
                    model = new ProjectsModel
                    {
                        Id = Convert.ToInt32(projectsSet.Rows[i]["id"]),
                        Name = projectsSet.Rows[i]["name"].ToString(),
                        ClientId = Convert.ToInt32(projectsSet.Rows[i]["client_id"]),
                    };
                }
                return model;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        internal static ProjectsModel CreateProject(string projectName, int clientModelId, string year)
        {
            try
            {
                //create new project based on the information of user and current folder then recall the method for getting project using this info
                string columnString = "name,client_id,year";
                string valuesString = "'" + projectName + "'," + clientModelId + ",'" + year + "'";

                int projectId = PostgreSqlClass.InsertData("projects", columnString, valuesString);
                return new ProjectsModel
                {
                    Id = projectId,
                    Name = projectName,
                    ClientId = clientModelId,
                };
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        internal static List<ClientProjectModel> GetClientProjectsByProjectId(int projectId)
        {
            try
            {
                string sql = "SELECT * FROM client_project WHERE project_id =" + projectId;
                DataTable clientProjectsDataTable = PostgreSqlClass.GetAllDataTable(sql);
                List<ClientProjectModel> clientProjectModelList = new List<ClientProjectModel>();
                for (int i = 0; i < clientProjectsDataTable.Rows.Count; i++)
                {
                    ClientProjectModel clientProjectModel = new ClientProjectModel
                    {
                        Id = Convert.ToInt32(clientProjectsDataTable.Rows[i]["id"]),
                        ProjectId = Convert.ToInt32(clientProjectsDataTable.Rows[i]["project_id"]),
                    };

                    clientProjectModelList.Add(clientProjectModel);
                }
                return clientProjectModelList;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        internal static bool CheckIfProjectExists(string text, int clientId, string year)
        {
            try
            {
                string query = string.Format("SELECT COUNT(*) FROM projects WHERE name='{0}' AND client_id={1} AND year='{2}'", text, clientId, year);
                return PostgreSqlClass.CheckIfExists(query);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        internal static List<PermissionsModel> GetClientProjectModelPermission(int clientProjectModelId)
        {
            try
            {
                string sql = "SELECT * FROM getClientPermission(" + clientProjectModelId + ")";
                List<PermissionsModel> permissionsModelList = new List<PermissionsModel>();
                DataTable permissionsDataTable = PostgreSqlClass.GetAllDataTable(sql);
                for (int i = 0; i < permissionsDataTable.Rows.Count; i++)
                {
                    PermissionsModel permissionsModel = new PermissionsModel
                    {
                        AccessTypeId = Convert.ToInt32(permissionsDataTable.Rows[i]["AccessTypeId"]),
                        AccessType = permissionsDataTable.Rows[i]["AccessType"].ToString(),
                        OrganisationalStructure = permissionsDataTable.Rows[i]["OrganisationStructure"].ToString(),
                        OrganisationalStructureId = Convert.ToInt32(permissionsDataTable.Rows[i]["OrganisationStructureId"]),
                        //Project = permissionsDataTable.Rows[i]["ProjectName"].ToString(),
                        ClientProjectId = Convert.ToInt32(permissionsDataTable.Rows[i]["ProjectIdNum"]),
                        RuleType = permissionsDataTable.Rows[i]["RuleType"].ToString(),
                        RuleTypeId = Convert.ToInt32(permissionsDataTable.Rows[i]["RuleTypeId"]),
                        ScopeId = Convert.ToInt32(permissionsDataTable.Rows[i]["ScopeTypeId"]),
                        ScopeType = permissionsDataTable.Rows[i]["ScopeType"].ToString(),
                        Id = Convert.ToInt32(permissionsDataTable.Rows[i]["PermissionId"]),

                    };
                    permissionsModelList.Add(permissionsModel);
                }
                return permissionsModelList;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        internal static ProjectsModel GetProjectByNameClientAndYear(string projectName, int id, string projectFolderName)
        {
            try
            {
                ProjectsModel model = new ProjectsModel();
                string sql = string.Format("SELECT * FROM projects WHERE name = '{0}' AND client_id={1} AND year='{2}'", projectName, id, projectFolderName);
                DataTable projectsSet = PostgreSqlClass.GetAllDataTable(sql);

                for (int i = 0; i < projectsSet.Rows.Count; i++)
                {
                    model = new ProjectsModel
                    {
                        Id = Convert.ToInt32(projectsSet.Rows[i]["id"]),
                        Name = projectsSet.Rows[i]["name"].ToString(),
                        ClientId = Convert.ToInt32(projectsSet.Rows[i]["client_id"]),
                    };
                }
                return model;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}
