﻿using DataAccessLayer;
using PermissionsManagementToolFrontEnd.Models;
using System;
using System.Data;

namespace PermissionsManagementToolFrontEnd.Controllers
{
    class ClientController
    {

        internal static ClientsModel GetClientById(int clientId)
        {
            try
            {
                ClientsModel model = new ClientsModel();
                string sql = "SELECT * FROM clients WHERE id = '" + clientId + "'";
                DataTable clientsSet = PostgreSqlClass.GetAllDataTable(sql);
                for (int i = 0; i < clientsSet.Rows.Count; i++)
                {
                    model = new ClientsModel
                    {
                        Id = Convert.ToInt32(clientsSet.Rows[i]["id"]),
                        Name = clientsSet.Rows[i]["name"].ToString(),
                        CountryId = Convert.ToInt32(clientsSet.Rows[i]["country_id"]),
                        UserId = Convert.ToInt32(clientsSet.Rows[i]["user_id"]),
                    };
                }
                return model;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        internal static ClientsModel GetClientByName(string client)
        {
            try
            {
                ClientsModel model = new ClientsModel();
                string sql = "SELECT * FROM clients WHERE name = '" + client + "'";
                DataTable clientsSet = PostgreSqlClass.GetAllDataTable(sql);
                for (int i = 0; i < clientsSet.Rows.Count; i++)
                {
                    model = new ClientsModel
                    {
                        Id = Convert.ToInt32(clientsSet.Rows[i]["id"]),
                        Name = clientsSet.Rows[i]["name"].ToString(),
                        CountryId = Convert.ToInt32(clientsSet.Rows[i]["country_id"]),
                        UserId = Convert.ToInt32(clientsSet.Rows[i]["user_id"]),
                    };
                }
                return model;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        internal static ClientsModel InsertIntoClients(string clientName, int userId, int countryId)
        {
            try
            {
                string columnString = "name,user_id,country_id";
                string valuesString = "'" + clientName + "'," + userId + "," + countryId;
                int clientId = PostgreSqlClass.InsertData("clients", columnString, valuesString);
                ClientsModel clientModel = new ClientsModel
                {
                    Id = clientId,
                    Name = clientName,
                    CountryId = countryId,
                    UserId = userId,
                };
                return clientModel;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        internal static bool CheckIfClientExists(string text, int userId, int countryId)
        {
            try
            {
                string query = string.Format("SELECT COUNT(*) FROM clients WHERE name='{0}' AND user_id={1} AND country_id={2}", text,userId,countryId);
                return PostgreSqlClass.CheckIfExists(query);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        internal static int GetClientIdByClientName(string clientName)
        {
            ClientsModel model = new ClientsModel();
            try
            {
                model = GetClientByName(clientName);
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return model.Id;
        }

    }
}
