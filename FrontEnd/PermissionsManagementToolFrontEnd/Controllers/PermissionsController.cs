﻿using DataAccessLayer;
using PermissionsManagementToolFrontEnd.Models;
using System;
using System.Collections.Generic;
using System.Data;

namespace PermissionsManagementToolFrontEnd.Controllers
{
    class PermissionsController
    {
        internal static UserPermissionModel InsertIntoPermissions(string valuesString)
        {
            try
            {
                string columnString = "project_client_id,access_type_id,organisational_structure_id,rule_type_id,scope_id,created_by";
                int savePermission = PostgreSqlClass.InsertData("permissions", columnString, valuesString);
                UserPermissionModel userPermissionModel = new UserPermissionModel();
                if (savePermission > 0)
                {
                    userPermissionModel = new UserPermissionModel
                    {
                        PermissionId = savePermission,
                    };
                }
                return userPermissionModel;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        internal static int DeletePermission(int permissionId)
        {
            try
            {
                return PostgreSqlClass.DeleteData("DELETE FROM permissions WHERE id = " + permissionId);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        internal static int UpdatePermission(int accessTypeId, int organisationalStructureId, int ruleTypeId, int scopeType, int userId, string date, int permissionId)
        {
            try
            {
                string columnString = string.Format("UPDATE permissions SET" +
                    " access_type_id={0},organisational_structure_id={1},rule_type_id={2},scope_id={3},update_by={4},date_update='{5}' WHERE id={6};",
                    accessTypeId, organisationalStructureId, ruleTypeId, scopeType, userId, date, permissionId);
                return PostgreSqlClass.UpdateData(columnString);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        internal static List<PermissionsModel> GetPermissionByClientProjectId(int id)
        {
            try
            {
                string sql = string.Format("SELECT * FROM permissions WHERE project_client_id ={0}", id);
                DataTable clientProjectsDataTable = PostgreSqlClass.GetAllDataTable(sql);
                List<PermissionsModel> permissionsModelList = new List<PermissionsModel>();
                for (int i = 0; i < clientProjectsDataTable.Rows.Count; i++)
                {
                    PermissionsModel permissionsModel = new PermissionsModel
                    {
                        Id = Convert.ToInt32(clientProjectsDataTable.Rows[i]["id"]),
                        AccessTypeId = Convert.ToInt32(clientProjectsDataTable.Rows[i]["access_type_id"]),
                        OrganisationalStructureId = Convert.ToInt32(clientProjectsDataTable.Rows[i]["organisational_structure_id"]),
                        RuleTypeId = Convert.ToInt32(clientProjectsDataTable.Rows[i]["rule_type_id"]),
                        ScopeId = Convert.ToInt32(clientProjectsDataTable.Rows[i]["scope_id"]),
                        ClientProjectId = Convert.ToInt32(clientProjectsDataTable.Rows[i]["project_client_id"]),
                    };

                    permissionsModelList.Add(permissionsModel);
                }
                return permissionsModelList;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        internal static List<PermissionsModel> GetPermissionByClientProjectId(int id, int accessTypeId, int organisationalStructureId, int ruleTypeId, int scopeType)
        {
            try
            {
                string sql = string.Format("SELECT * FROM permissions WHERE project_client_id ={0} AND access_type_id={1} AND organisational_structure_id={2} AND " +
                    "rule_type_id={3} AND scope_id={4}", id, accessTypeId, organisationalStructureId, ruleTypeId, scopeType);
                DataTable clientProjectsDataTable = PostgreSqlClass.GetAllDataTable(sql);
                List<PermissionsModel> permissionsModelList = new List<PermissionsModel>();
                for (int i = 0; i < clientProjectsDataTable.Rows.Count; i++)
                {
                    PermissionsModel permissionsModel = new PermissionsModel
                    {
                        Id = Convert.ToInt32(clientProjectsDataTable.Rows[i]["id"]),
                    };

                    permissionsModelList.Add(permissionsModel);
                }
                return permissionsModelList;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        internal static bool CheckIfPermissionExists(int id, int accessTypeId, int organisationalStructureId, int ruleTypeId, int scopeType)
        {
            try
            {
                //string query = string.Format("SELECT COUNT(*) FROM permissions WHERE name='{0}' AND user_id={1} AND country_id={2}", text, userId, countryId);
                string query = string.Empty;
                return PostgreSqlClass.CheckIfExists(query);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

    }
}
