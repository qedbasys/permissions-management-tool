﻿using DataAccessLayer;
using PermissionsManagementToolFrontEnd.Models;
using System;
using System.Collections.Generic;
using System.Data;

namespace PermissionsManagementToolFrontEnd.Controllers
{
    class TypesController
    {
        internal static List<AccessTypesModel> GetAccessTypes()
        {
            try
            {
                string sql = "SELECT * FROM access_type";
                DataTable accessTypes = PostgreSqlClass.GetAllDataTable(sql);
                List<AccessTypesModel> accessTypesModelList = new List<AccessTypesModel>();
                for (int i = 0; i < accessTypes.Rows.Count; i++)
                {
                    AccessTypesModel accessTypesModel = new AccessTypesModel
                    {
                        Id = Convert.ToInt32(accessTypes.Rows[i]["id"]),
                        Name = accessTypes.Rows[i]["name"].ToString(),
                    };

                    accessTypesModelList.Add(accessTypesModel);
                }
                return accessTypesModelList;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        internal static List<OrganisationalStructuresModel> GetOrganisationalStructure()
        {
            try
            {
                string sql = "SELECT * FROM organisational_structure";
                DataTable organisationalStructures = PostgreSqlClass.GetAllDataTable(sql);
                List<OrganisationalStructuresModel> organisationalStructuresModelList = new List<OrganisationalStructuresModel>();
                for (int i = 0; i < organisationalStructures.Rows.Count; i++)
                {
                    OrganisationalStructuresModel organisationalStructuresModel = new OrganisationalStructuresModel
                    {
                        Id = Convert.ToInt32(organisationalStructures.Rows[i]["id"]),
                        Name = organisationalStructures.Rows[i]["name"].ToString(),
                    };

                    if (!organisationalStructuresModel.Name.Contains("Extended"))
                    {
                        organisationalStructuresModelList.Add(organisationalStructuresModel);
                    }
                }
                return organisationalStructuresModelList;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        internal static List<RuleTypeModel> GetRuleTypes()
        {
            try
            {
                string sql = "SELECT * FROM rule_type";
                DataTable ruleTypes = PostgreSqlClass.GetAllDataTable(sql);
                List<RuleTypeModel> ruleTypeModelList = new List<RuleTypeModel>();
                for (int i = 0; i < ruleTypes.Rows.Count; i++)
                {
                    RuleTypeModel ruleTypeModel = new RuleTypeModel
                    {
                        Id = Convert.ToInt32(ruleTypes.Rows[i]["id"]),
                        Name = ruleTypes.Rows[i]["name"].ToString(),
                    };

                    ruleTypeModelList.Add(ruleTypeModel);
                }
                return ruleTypeModelList;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }


        internal static List<ScopeTypeModel> GetScopeTypes()
        {
            try
            {
                string sql = "SELECT * FROM scope_type";
                DataTable scopeTypes = PostgreSqlClass.GetAllDataTable(sql);
                List<ScopeTypeModel> scopeTypeModelList = new List<ScopeTypeModel>();
                for (int i = 0; i < scopeTypes.Rows.Count; i++)
                {
                    ScopeTypeModel scopeTypeModel = new ScopeTypeModel
                    {
                        Id = Convert.ToInt32(scopeTypes.Rows[i]["id"]),
                        Name = scopeTypes.Rows[i]["name"].ToString(),
                    };

                    scopeTypeModelList.Add(scopeTypeModel);
                }
                return scopeTypeModelList;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        internal static string GetTypeById(string table, int id)
        {
            try
            {
                string query = string.Format("SELECT name FROM {0} WHERE id={1}", table, id);
                return PostgreSqlClass.GetTypeById(query);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}
