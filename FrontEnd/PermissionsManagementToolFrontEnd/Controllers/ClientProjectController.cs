﻿using DataAccessLayer;
using PermissionsManagementToolFrontEnd.Models;
using System;
using System.Collections.Generic;
using System.Data;

namespace PermissionsManagementToolFrontEnd.Controllers
{
    class ClientProjectController
    {
        internal static List<ClientProjectModel> GetClientProjects()
        {
            throw new NotImplementedException();
        }

        internal static List<ClientProjectModel> GetClientProjectById(int id)
        {
            throw new NotImplementedException();
        }

        internal static List<ClientProjectModel> GetClientProjectsByClientId(int clientId)
        {
            try
            {
                string sql = "SELECT * FROM client_project WHERE client_id =" + clientId;
                DataTable clientProjectsDataTable = PostgreSqlClass.GetAllDataTable(sql);
                List<ClientProjectModel> clientProjectModelList = new List<ClientProjectModel>();
                for (int i = 0; i < clientProjectsDataTable.Rows.Count; i++)
                {
                    ClientProjectModel clientProjectModel = new ClientProjectModel
                    {
                        Id = Convert.ToInt32(clientProjectsDataTable.Rows[i]["id"]),
                        ClientId = Convert.ToInt32(clientProjectsDataTable.Rows[i]["client_id"]),
                    };

                    clientProjectModelList.Add(clientProjectModel);
                }
                return clientProjectModelList;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        internal static List<PermissionsModel> GetClientProjectModelPermission(int clientProjectId)
        {
            try
            {
                string sql = "SELECT * FROM getClientPermission(" + clientProjectId + ")";
                DataTable permissionsDataTable = PostgreSqlClass.GetAllDataTable(sql);
                List<PermissionsModel> permissionsModelList = new List<PermissionsModel>();
                for (int i = 0; i < permissionsDataTable.Rows.Count; i++)
                {
                    PermissionsModel permissionsModel = new PermissionsModel
                    {
                        AccessTypeId = Convert.ToInt32(permissionsDataTable.Rows[i]["AccessTypeId"]),
                        AccessType = permissionsDataTable.Rows[i]["AccessType"].ToString(),
                        OrganisationalStructure = permissionsDataTable.Rows[i]["OrganisationStructure"].ToString(),
                        OrganisationalStructureId = Convert.ToInt32(permissionsDataTable.Rows[i]["OrganisationStructureId"]),
                        //Project = permissionsDataTable.Rows[i]["ProjectName"].ToString(),
                        ClientProjectId = Convert.ToInt32(permissionsDataTable.Rows[i]["ProjectIdNum"]),
                        RuleType = permissionsDataTable.Rows[i]["RuleType"].ToString(),
                        RuleTypeId = Convert.ToInt32(permissionsDataTable.Rows[i]["RuleTypeId"]),
                        ScopeId = Convert.ToInt32(permissionsDataTable.Rows[i]["ScopeTypeId"]),
                        ScopeType = permissionsDataTable.Rows[i]["ScopeType"].ToString(),
                        Id = Convert.ToInt32(permissionsDataTable.Rows[i]["PermissionId"]),

                    };
                    permissionsModelList.Add(permissionsModel);
                }
                return permissionsModelList;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        internal static ClientProjectModel InsertIntoClientProject(string columnString, string valuesString)
        {
            try
            {
                int saveClientProject = PostgreSqlClass.InsertData("client_project", columnString, valuesString);
                ClientProjectModel clientProjectModel = new ClientProjectModel();

                if (saveClientProject > 0)
                {
                    clientProjectModel = new ClientProjectModel
                    {
                        Id = saveClientProject
                    };
                }
                return clientProjectModel;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        internal static List<ClientProjectModel> GetClientProjectsByProjectId(int id)
        {
            try
            {
                string sql = "SELECT * FROM client_project WHERE project_id =" + id;
                DataTable clientProjectsDataTable = PostgreSqlClass.GetAllDataTable(sql);
                List<ClientProjectModel> clientProjectModelList = new List<ClientProjectModel>();
                for (int i = 0; i < clientProjectsDataTable.Rows.Count; i++)
                {
                    ClientProjectModel clientProjectModel = new ClientProjectModel
                    {
                        Id = Convert.ToInt32(clientProjectsDataTable.Rows[i]["id"]),
                        ProjectId = Convert.ToInt32(clientProjectsDataTable.Rows[i]["project_id"]),
                    };

                    clientProjectModelList.Add(clientProjectModel);
                }
                return clientProjectModelList;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}
