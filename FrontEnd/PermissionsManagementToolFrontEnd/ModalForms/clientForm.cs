﻿using PermissionsManagementToolFrontEnd.Controllers;
using PermissionsManagementToolFrontEnd.Helpers;
using PermissionsManagementToolFrontEnd.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.DirectoryServices.AccountManagement;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Threading;

namespace PermissionsManagementToolFrontEnd.ModalForms
{
    public partial class clientForm : Form
    {
        List<UsersModel> usersModelList = new List<UsersModel>();
        List<CountriesModel> countriesModelList = new List<CountriesModel>();
        loader loader = new loader();

        public clientForm()
        {
            InitializeComponent();
            // get clients

            GetManagers();
            Getcountries();
        }

        private void Getcountries()
        {
            try
            {
                countriesModelList = CountriesController.GetCountries();
                PopulateCombobox(cmbCountry.Name);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Countries Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void GetManagers()
        {
            try
            {
                usersModelList = UsersController.GetAllUsers();
                PopulateCombobox(cmbManager.Name);
                cmbManager.Text = "Please Select Client Manager";
                cmbCountry.Text = "Please Select Country";
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Client Managers Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void PopulateCombobox(string combobox)
        {
            try
            {
                switch (combobox)
                {
                    case "cmbManager":
                        cmbManager.Items.Clear();
                        cmbManager.DataSource = usersModelList.OrderBy(x => x.FirstName).ThenBy(y => y.LastName).ToArray();
                        cmbManager.DisplayMember = "ClientManagerName";
                        break;
                    default:
                        //cmbCountry
                        cmbCountry.Items.Clear();
                        cmbCountry.DataSource = countriesModelList.OrderBy(z => z.Name).ToArray();
                        CountriesModel selected = countriesModelList.FirstOrDefault(x => x.Name == "South Africa");
                        cmbCountry.SelectedItem = selected;
                        cmbCountry.DisplayMember = "Name";
                        break;
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public static DialogResult result = new DialogResult();
        public static DialogResult Result { get => result; set => result = value; }
        public static string ClientLocation = string.Empty;
        public static string CountryName = string.Empty;
        public string Year = string.Empty;
        Thread thread;

        private new void Load()
        {
            if (loader.InvokeRequired)
            {
                loader.Invoke(new MethodInvoker(loader.Close));
                loader = new loader();
                Application.Run(loader);
            }
            else
            {
                if (loader.IsDisposed)
                {
                    loader = new loader();
                }
                Application.Run(loader);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                Close();
                Form1 form1 = new Form1();
                form1.Show();
            }
            catch (Exception exception)
            {
                if (loader.InvokeRequired)
                {
                    loader.Invoke(new MethodInvoker(loader.CloseLoader));
                }
                Activate();

                MessageBox.Show(exception.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public static string savePath = string.Empty;
        public static string bundledPerm = string.Empty;

        private void btnCreate_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtClientName.Text) && !string.IsNullOrEmpty(txtFolderName.Text) && cmbCountry.SelectedIndex != -1 && cmbManager.SelectedIndex != -1)
                {
                    if (Common.IsValidFilename(txtClientName.Text))
                    {
                        thread = new System.Threading.Thread(Load);

                        thread.Start();

                        if (CreateClient() > 0)
                        {
                            //close loader
                            if (loader.InvokeRequired)
                            {
                                loader.Invoke(new MethodInvoker(loader.CloseLoader));
                            }
                            Activate();

                            int year = DateTime.Now.Year;
                            result = MessageBox.Show("Client Successfully created.Do you want to create a year: " + year + "?", "Alert", MessageBoxButtons.YesNo);
                            string[] drives = Environment.GetLogicalDrives();
                            string drive = drives.Where(d => d == "U:\\").FirstOrDefault();
                            string country_name = ((CountriesModel)cmbCountry.SelectedValue).Name;

                            if (result == DialogResult.Yes)
                            {
                                Result = DialogResult.Yes;
                                // string country_name = ((CountriesModel)cmbCountry.SelectedValue).Name;
                                Directory.CreateDirectory(drive + "\\" + country_name + "\\" + txtClientName.Text);
                                // create year
                                Directory.CreateDirectory(drive + "\\" + country_name + "\\" + txtClientName.Text + "\\" + year);
                                ClientLocation = drive + "\\" + country_name + "\\" + txtClientName.Text + "\\" + year;
                            }
                            else
                            {
                                //NO
                                Result = DialogResult.No;
                                Directory.CreateDirectory(drive + "\\" + country_name + "\\" + txtClientName.Text);
                                ClientLocation = drive + "\\" + country_name + "\\" + txtClientName.Text;
                                CountryName = country_name;
                            }
                            //create sub folders for clients 1 Meta data
                            //string path = AppDomain.CurrentDomain.BaseDirectory + "clientFolderMeta";
                            string projectFolder = drive + "\\" + country_name + "\\" + txtClientName.Text + "\\1 Meta Data";
                            //var directoryInfoSource = new DirectoryInfo(path);
                            Directory.CreateDirectory(drive + "\\" + country_name + "\\" + txtClientName.Text + "\\1 Meta Data");
                            var directoryInfoTarget = new DirectoryInfo(projectFolder);
                            Common.CreateClientSubDirectories(directoryInfoTarget);

                            //Common.CopyProjectMeta(directoryInfoSource, directoryInfoTarget);
                            Common.SetPermissionsInActiveDirectory(savePath + bundledPerm);//+ ";B05;Allow;Write;Add;Client"

                            Form1.newLoad = true;
                            Hide();
                            Form1 form1 = new Form1();
                            form1.Show();
                        }
                    }
                    else
                    {
                        if (loader.InvokeRequired)
                        {
                            loader.Invoke(new MethodInvoker(loader.CloseLoader));
                        }
                        Activate();

                        MessageBox.Show(string.Format("Please remove special characters from Client name."), "Special Characters Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    if (loader.InvokeRequired)
                    {
                        loader.Invoke(new MethodInvoker(loader.CloseLoader));
                    }
                    Activate();

                    MessageBox.Show(string.Format("Please enter all fields."), "Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception exception)
            {
                if (loader.InvokeRequired)
                {
                    loader.Invoke(new MethodInvoker(loader.CloseLoader));
                }
                MessageBox.Show(exception.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Hide();
                Form1 form1 = new Form1();
                form1.Show();
            }
        }

        ClientsModel clientModel = new ClientsModel();
        string domainName = System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName;
        PrincipalContext principalContext;
        UsersModel userModel = new UsersModel();

        private int CreateClient()
        {
            try
            {
                principalContext = new PrincipalContext(ContextType.Domain, domainName);
                //DirectoryInfo directoryInfo = new DirectoryInfo("U:\\Test Country");
                //FileSecurity fileSecurity = File.GetAccessControl("U:\\Test Country");
                //IdentityReference identityReference = fileSecurity.GetOwner(typeof(SecurityIdentifier));
                //NTAccount ntAccount = identityReference.Translate(typeof(NTAccount)) as NTAccount;

                // find your user
                string currentUserName = Environment.UserName;
                UserPrincipal userPrincipal = UserPrincipal.FindByIdentity(principalContext, currentUserName);
                //UserPrincipal user = UserPrincipal.FindByIdentity(principalContext, ntAccount.Value);

                // Get the collection of authorization rules that apply to the current directory
                if (userPrincipal != null)// (user != null)
                {
                    // get owner by id
                    userModel = UsersController.GetUserFromDb(userPrincipal.GivenName, userPrincipal.Surname);
                }

                int userId = userModel.Id;
                int countryId = ((CountriesModel)cmbCountry.SelectedValue).Id;
                string countryName = ((CountriesModel)cmbCountry.SelectedValue).Name;
                if (!ClientController.CheckIfClientExists(txtClientName.Text.Trim(), userId, countryId))
                {
                    clientModel = ClientController.InsertIntoClients(txtClientName.Text.Trim(), userId, countryId);
                    // create permissions for this client based on this user groups/departments
                    PrincipalSearchResult<Principal> groups = userPrincipal.GetAuthorizationGroups();
                    List<OrganisationalStructuresModel> organisationalStructuresList = TypesController.GetOrganisationalStructure();
                    //filter all organisational Structures from group
                    List<int> filteredOrganisationalStructure = new List<int>();
                    foreach (var item in organisationalStructuresList)
                    {
                        var groupName = groups.FirstOrDefault(x => x.Name == item.Name);
                        if (groupName != null)
                        {
                            filteredOrganisationalStructure.Add(item.Id);
                        }
                    }
                    string columnString = "client_id";
                    string valuesString = clientModel.Id.ToString();
                    Form1.newLoad = false;
                    Form1 form1 = new Form1();

                    List<ClientProjectModel> clientProjectList = ClientProjectController.GetClientProjectsByClientId(clientModel.Id);
                    if (clientProjectList.Count == 0)
                    {
                        ClientProjectModel clientProjectModel = ClientProjectController.InsertIntoClientProject(columnString, valuesString);
                        foreach (var item in filteredOrganisationalStructure)
                        {
                            Form1.clientFormLocation = "U:\\\\" + countryName + "\\" + txtClientName.Text;
                            form1.SetPermissionForSubFolders(clientProjectModel.Id, userId, 1, item, 2, 1);
                        }
                    }
                    else if (clientProjectList.Count > 0)
                    {
                        foreach (var item in clientProjectList)
                        {
                            List<PermissionsModel> permissionsList = PermissionsController.GetPermissionByClientProjectId(item.Id);
                            if (permissionsList.Count == 0)
                            {
                                foreach (var permissions in permissionsList)
                                {
                                    foreach (var os in filteredOrganisationalStructure)
                                    {
                                        Form1.clientFormLocation = "U:\\\\" + countryName + "\\" + txtClientName.Text;
                                        form1.SetPermissionForSubFolders(item.Id, userId, 1, os, 2, 1);
                                    }
                                }
                            }
                            else if (permissionsList.Count > 0)
                            {
                                foreach (var permissions in permissionsList)
                                {
                                    Form1.clientFormLocation = "U:\\\\" + countryName + "\\" + txtClientName.Text;
                                    form1.SetPermissionForSubFolders(item.Id, userId, permissions.AccessTypeId, permissions.OrganisationalStructureId, permissions.RuleTypeId, permissions.ScopeId);
                                }
                                foreach (var os in filteredOrganisationalStructure)
                                {
                                    Form1.clientFormLocation = "U:\\\\" + countryName + "\\" + txtClientName.Text;
                                    form1.SetPermissionForSubFolders(item.Id, userId, 1, os, 2, 1);
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (loader.InvokeRequired)
                    {
                        loader.Invoke(new MethodInvoker(loader.CloseLoader));
                    }
                    Activate();

                    MessageBox.Show(string.Format("{0},already exist for {1}", txtClientName.Text, ((CountriesModel)cmbCountry.SelectedValue).Name), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception exception)
            {
                if (loader.InvokeRequired)
                {
                    loader.Invoke(new MethodInvoker(loader.CloseLoader));
                }
                Activate();

                MessageBox.Show(exception.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return clientModel.Id;
        }
    }
}
