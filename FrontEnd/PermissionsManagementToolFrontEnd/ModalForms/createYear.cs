﻿using System;
using System.IO;
using System.Windows.Forms;

namespace PermissionsManagementToolFrontEnd.ModalForms
{
    public partial class createYear : Form
    {
        public static DialogResult result = new DialogResult();
        public static DialogResult Result { get => result; set => result = value; }
        public static string country = string.Empty;
        public static string clientLocation = string.Empty;

        public createYear()
        {
            InitializeComponent();
            PopulateYears();
        }

        public void PopulateYears()
        {
            try
            {
                int yearVal = DateTime.Now.Year;
                for (int i = 0; i < 4; i++)
                {
                    cmbYear.Items.Add(yearVal + i);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Could not get years", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                int year = ((int)cmbYear.SelectedItem);
                bool isExists = true;
                if (!string.IsNullOrEmpty(clientLocation))
                {
                    if (!Directory.Exists(clientLocation + "\\" + year))
                    {
                        Directory.CreateDirectory(clientLocation + "\\" + year);
                    }
                    else
                    {
                        isExists = false;
                        string[] countrysubstring = clientLocation.Split('\\');
                        MessageBox.Show(string.Format("{0},already exist for {1}.", year.ToString(), countrysubstring[2]), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    clientLocation = clientForm.ClientLocation;
                    if (!Directory.Exists(clientLocation + "\\" + year))
                    {
                        Directory.CreateDirectory(clientLocation + "\\" + year);
                    }
                    else
                    {
                        isExists = false;
                        string[] countrysubstring = clientLocation.Split('\\');
                        MessageBox.Show(string.Format("{0},already exist for {1}.", year.ToString(), countrysubstring[2]), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

                if (isExists)
                {
                    string[] clientNameSubstring = clientLocation.Split('\\');
                    result = MessageBox.Show("Year Successfully created year for " + clientNameSubstring[3] + ".Do you want to create a Project.", "Alert", MessageBoxButtons.YesNo);
                    if (result == DialogResult.Yes)
                    {
                        string currentlocation = clientLocation + "\\" + ((int)cmbYear.SelectedItem);
                        Result = DialogResult.Yes;
                        Hide();
                        Form1.clientFormLocation = clientLocation + "\\" + year;
                        projectNameFrm.currentLocation = currentlocation;
                        projectNameFrm projectName = new projectNameFrm();
                        projectName.Show();
                    }
                    else
                    {
                        //NO
                        //do nothing
                        Result = DialogResult.No;
                        Form1.clientFormLocation = clientLocation + "\\" + year;
                        Hide();
                        Form1 form1 = new Form1();
                        form1.Show();
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Result = DialogResult.Cancel;
                Close();
                Form1 form1 = new Form1();
                form1.Show();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
