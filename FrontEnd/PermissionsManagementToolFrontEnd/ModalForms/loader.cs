﻿using System;
using System.Windows.Forms;

namespace PermissionsManagementToolFrontEnd.ModalForms
{
    public partial class loader : Form
    {
        public loader()
        {
            InitializeComponent();
        }

        private void loader_Load(object sender, EventArgs e)
        {
            timer1.Start();
        }

        int move = 2;

        private void timer1_Tick(object sender, EventArgs e)
        {
            panelSlide.Left += 6;
            if (panelSlide.Left > 500)
            {
                panelSlide.Left = 0;
            }
            if (panelSlide.Left < 0)
            {
                move = 2;
            }
        }

        public void CloseLoader()
        {
            Close();
        }
    }
}
