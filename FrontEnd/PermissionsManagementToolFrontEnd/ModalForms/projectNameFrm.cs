﻿using PermissionsManagementToolFrontEnd.Controllers;
using PermissionsManagementToolFrontEnd.Helpers;
using PermissionsManagementToolFrontEnd.Models;
using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Windows.Forms;
using System.Threading;

namespace PermissionsManagementToolFrontEnd.ModalForms
{
    public partial class projectNameFrm : Form
    {
        public static string currentLocation = string.Empty;
        public static DialogResult result = new DialogResult();
        public static DialogResult Result { get => result; set => result = value; }
        internal static string projectName = string.Empty;
        ProjectsModel projectsModel = new ProjectsModel();
        public static string savePath = string.Empty;
        public static List<string> bundledPerm = new List<string>();
        loader loader = new loader();
        Thread thread;
        string domainName = System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName;
        PrincipalContext principalContext;
        UsersModel userModel = new UsersModel();

        public projectNameFrm()
        {
            InitializeComponent();
        }

        private new void Load()
        {
            if (loader.InvokeRequired)
            {
                loader.Invoke(new MethodInvoker(loader.Close));
                loader = new loader();
                Application.Run(loader);
            }
            else
            {
                if (loader.IsDisposed)
                {
                    loader = new loader();
                }
                Application.Run(loader);
            }
        }

        private void btnCreateProject_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtProjectName.Text) && txtProjectName.Text != "Enter Project Name")
                {
                    if (Common.IsValidFilename(txtProjectName.Text))
                    {
                        thread = new System.Threading.Thread(Load);

                        thread.Start();

                        if (CreateProject() > 0)
                        {
                            //close loader
                            if (loader.InvokeRequired)
                            {
                                loader.Invoke(new MethodInvoker(loader.CloseLoader));
                            }
                            Activate();

                            Directory.CreateDirectory(currentLocation + "\\" + projectsModel.Name);
                            //string path = AppDomain.CurrentDomain.BaseDirectory + "ProjTemp";
                            string projectFolder = currentLocation + "\\" + projectsModel.Name;
                            //var directoryInfoSource = new DirectoryInfo(path);
                            var directoryInfoTarget = new DirectoryInfo(projectFolder);
                            //Common.CopyProjectMeta(directoryInfoSource, directoryInfoTarget);
                            Common.CreateProjectSubDirectories(directoryInfoTarget);
                            projectName = projectsModel.Name;
                            foreach (var item in bundledPerm)
                            {
                                Common.SetPermissionsInActiveDirectory(savePath + item);
                            }

                            Result = Result = DialogResult.Yes;
                            Form1.newLoad = true;
                            Hide();
                            Form1 form1 = new Form1();
                            form1.Show();
                        }
                    }
                    else
                    {
                        if (loader.InvokeRequired)
                        {
                            loader.Invoke(new MethodInvoker(loader.CloseLoader));
                        }
                        Activate();

                        MessageBox.Show(string.Format("Please remove special characters from Project name."), "Special Characters Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    if (loader.InvokeRequired)
                    {
                        loader.Invoke(new MethodInvoker(loader.CloseLoader));
                    }
                    Activate();

                    MessageBox.Show("Please enter appropriate text", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception exception)
            {
                if (loader.InvokeRequired)
                {
                    loader.Invoke(new MethodInvoker(loader.CloseLoader));
                }
                Activate();

                MessageBox.Show(exception.Message, "Create Project Click Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private int CreateProject()
        {
            try
            {
                string[] getProjectSubstring = currentLocation.Split('\\');
                int client_id = ClientController.GetClientIdByClientName(getProjectSubstring[3]);
                string year = getProjectSubstring[4];

                if (!Directory.Exists(currentLocation + "\\" + txtProjectName.Text))
                {
                    string folderLocation = getProjectSubstring[0] + "\\" + getProjectSubstring[1] + "\\" + getProjectSubstring[2];
                    DirectoryInfo directoryInfo = new DirectoryInfo(folderLocation);
                    FileSecurity fileSecurity = File.GetAccessControl(folderLocation);
                    IdentityReference identityReference = fileSecurity.GetOwner(typeof(SecurityIdentifier));
                    NTAccount ntAccount = identityReference.Translate(typeof(NTAccount)) as NTAccount;

                    principalContext = new PrincipalContext(ContextType.Domain, domainName);

                    string currentUserName = Environment.UserName;
                    UserPrincipal userPrincipal = UserPrincipal.FindByIdentity(principalContext, currentUserName);
                    UserPrincipal user = UserPrincipal.FindByIdentity(principalContext, ntAccount.Value);
                    //Add code that gets current client
                    //after getting client get permissions of a client
                    List<ClientProjectModel> clientProjectModelList = ClientProjectController.GetClientProjectsByClientId(client_id);
                    if (clientProjectModelList.Count > 0)
                    {
                        List<PermissionsModel> permissionsModelList = PermissionsController.GetPermissionByClientProjectId(clientProjectModelList.FirstOrDefault().Id);
                        if (userPrincipal != null)
                        {
                            // get owner by id
                            userModel = UsersController.GetUserFromDb(userPrincipal.GivenName, userPrincipal.Surname);
                        }
                        if (!ProjectsController.CheckIfProjectExists(txtProjectName.Text.Trim(), client_id, year))
                        {
                            projectsModel = ProjectsController.CreateProject(txtProjectName.Text, client_id, year);
                            PrincipalSearchResult<Principal> groups = userPrincipal.GetAuthorizationGroups();
                            // PrincipalSearchResult<Principal> ownerGroups = user.GetAuthorizationGroups();
                            List<int> ownerGroups = new List<int>();
                            foreach (var item in permissionsModelList)
                            {
                                ownerGroups.Add(item.Id);
                            }
                            List<OrganisationalStructuresModel> organisationalStructuresList = TypesController.GetOrganisationalStructure();
                            //filter all organisational Structures from group
                            List<int> filteredOrganisationalStructure = new List<int>();
                            Form1.newLoad = false;
                            Form1 form1 = new Form1();
                            foreach (var item in organisationalStructuresList)
                            {
                                var groupName = groups.FirstOrDefault(x => x.Name == item.Name);
                                var ownerGroupName = ownerGroups.FirstOrDefault(x => x == item.Id);
                                if (groupName != null)
                                {
                                    filteredOrganisationalStructure.Add(item.Id);
                                }
                                if (ownerGroupName != 0 && !filteredOrganisationalStructure.Contains(item.Id))
                                {
                                    filteredOrganisationalStructure.Add(item.Id);
                                }
                            }
                            string columnString = "project_id";
                            string valuesString = projectsModel.Id.ToString();

                            List<ClientProjectModel> clientProjectList = ClientProjectController.GetClientProjectsByProjectId(projectsModel.Id);
                            List<ClientProjectModel> clientProjectListForClients = ClientProjectController.GetClientProjectsByClientId(client_id);
                            if (clientProjectList.Count == 0)
                            {
                                ClientProjectModel clientProjectModel = ClientProjectController.InsertIntoClientProject(columnString, valuesString);
                                Form1.clientFormLocation = currentLocation + "\\" + txtProjectName.Text;
                                foreach (var item in filteredOrganisationalStructure)
                                {
                                    form1.SetPermissionForSubFolders(clientProjectModel.Id, userModel.Id, 1, item, 2, 1);
                                }
                                foreach (var item in clientProjectListForClients)
                                {
                                    List<PermissionsModel> permissionsList = PermissionsController.GetPermissionByClientProjectId(item.Id);
                                    if (permissionsList.Count == 0)
                                    {
                                        foreach (var os in filteredOrganisationalStructure)
                                        {
                                            Form1.clientFormLocation = currentLocation;
                                            form1.SetPermissionForSubFolders(item.Id, userModel.Id, 1, os, 2, 1);
                                        }
                                    }
                                    else if (permissionsList.Count > 0)
                                    {
                                        List<PermissionsModel> projectPermissionsList = PermissionsController.GetPermissionByClientProjectId(clientProjectModel.Id);
                                        foreach (var permissions in permissionsList)
                                        {

                                            Form1.clientFormLocation = currentLocation;
                                            var isInList = projectPermissionsList.Where(x => x.OrganisationalStructureId == permissions.OrganisationalStructureId).FirstOrDefault();
                                            if (isInList == null)
                                            {
                                                form1.SetPermissionForSubFolders(clientProjectModel.Id, userModel.Id, permissions.AccessTypeId, permissions.OrganisationalStructureId, permissions.RuleTypeId, permissions.ScopeId);
                                            }
                                        }
                                    }
                                }
                            }
                            else if (clientProjectList.Count > 0)
                            {
                                foreach (var item in clientProjectList)
                                {
                                    List<PermissionsModel> permissionsList = PermissionsController.GetPermissionByClientProjectId(item.Id);
                                    if (permissionsList.Count == 0)
                                    {
                                        foreach (var os in filteredOrganisationalStructure)
                                        {
                                            Form1.clientFormLocation = currentLocation;
                                            form1.SetPermissionForSubFolders(item.Id, userModel.Id, 1, os, 2, 1);
                                        }
                                    }
                                    else if (permissionsList.Count > 0)
                                    {
                                        foreach (var permissions in permissionsList)
                                        {
                                            Form1.clientFormLocation = currentLocation;
                                            form1.SetPermissionForSubFolders(item.Id, userModel.Id, permissions.AccessTypeId, permissions.OrganisationalStructureId, permissions.RuleTypeId, permissions.ScopeId);
                                        }
                                        foreach (var os in filteredOrganisationalStructure)
                                        {
                                            Form1.clientFormLocation = currentLocation;
                                            form1.SetPermissionForSubFolders(item.Id, userModel.Id, 1, os, 2, 1);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (loader.InvokeRequired)
                            {
                                loader.Invoke(new MethodInvoker(loader.CloseLoader));
                            }
                            Activate();

                            string[] countrysubstring = currentLocation.Split('\\');
                            MessageBox.Show(string.Format("{0},already exist in {1} folder.", txtProjectName.Text.ToString(), countrysubstring[2]), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        if (loader.InvokeRequired)
                        {
                            loader.Invoke(new MethodInvoker(loader.CloseLoader));
                        }
                        Activate();

                        string[] countrysubstring = currentLocation.Split('\\');
                        MessageBox.Show(string.Format("{0},does not have any permissions,please contact Admin.", countrysubstring[3]), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Close();
                        Form1 form1 = new Form1();
                        form1.Show();

                    }
                }
                else
                {
                    if (loader.InvokeRequired)
                    {
                        loader.Invoke(new MethodInvoker(loader.CloseLoader));
                    }
                    Activate();

                    string[] countrysubstring = currentLocation.Split('\\');
                    MessageBox.Show(string.Format("{0},already exist in {1} folder.", txtProjectName.Text.ToString(), countrysubstring[3]), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception exception)
            {
                if (loader.InvokeRequired)
                {
                    loader.Invoke(new MethodInvoker(loader.CloseLoader));
                }
                Activate();

                MessageBox.Show(exception.Message, "Create Project Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return projectsModel.Id;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Result = Result = DialogResult.Cancel;
                currentLocation = string.Empty;
                Close();
                Form1 form1 = new Form1();
                form1.Show();
            }
            catch (Exception exception)
            {
                if (loader.InvokeRequired)
                {
                    loader.Invoke(new MethodInvoker(loader.CloseLoader));
                }
                Activate();

                MessageBox.Show(exception.Message, "Cancel Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
