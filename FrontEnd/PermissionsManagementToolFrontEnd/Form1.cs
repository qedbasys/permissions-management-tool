﻿using PermissionsManagementToolFrontEnd.Controllers;
using PermissionsManagementToolFrontEnd.Helpers;
using PermissionsManagementToolFrontEnd.ModalForms;
using PermissionsManagementToolFrontEnd.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.DirectoryServices.AccountManagement;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace PermissionsManagementToolFrontEnd
{
    public partial class Form1 : Form
    {
        // global variables
        createYear createYear = new createYear();
        List<AccessTypesModel> accessTypesModelList = new List<AccessTypesModel>();
        List<OrganisationalStructuresModel> organisationalStructuresModelList = new List<OrganisationalStructuresModel>();
        List<RuleTypeModel> ruleTypeModelList = new List<RuleTypeModel>();
        List<ScopeTypeModel> scopeTypeModelList = new List<ScopeTypeModel>();
        public static string clientFormLocation = string.Empty;
        string clientCountry = string.Empty;
        string projectLocation = string.Empty;
        List<GroupPrincipal> result = new List<GroupPrincipal>();
        List<GroupPrincipal> folderResult = new List<GroupPrincipal>();
        List<string> usernames = new List<string>();
        loader loader = new loader();
        internal static bool newLoad = true;
        Thread thread;
        string domainName = System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName;
        PrincipalContext principalContext;
        string countryName = string.Empty;
        CountriesModel country = new CountriesModel();
        UsersModel userModel = new UsersModel();
        string clientName = string.Empty;
        ClientsModel clientModel = new ClientsModel();
        ProjectsModel projectsModel = new ProjectsModel();
        string year = string.Empty;
        List<PermissionsModel> permissionsModelList = new List<PermissionsModel>();
        List<UserPermissionModel> userPermissionModelList = new List<UserPermissionModel>();
        bool isOwner = false;

        //creates new instance of the loader
        private new void Load()
        {
            if (loader.IsDisposed)
            {
                loader = new loader();

                if (loader.InvokeRequired)
                {
                    loader.Invoke(new MethodInvoker(loader.Close));
                    loader = new loader();
                    Application.Run(loader);
                }
                else
                {
                    // if (loader.IsDisposed)
                    // {
                    //    loader = new loader();
                    // }
                    Application.Run(loader);
                }
            }
        }
        public Form1()
        {
            //initialize the loader
            thread = new Thread(Load);

            if (newLoad)
            {
                thread.Start();

                //createCountries();

                InitializeComponent();

                DialogResult dialogResult = clientForm.Result;
                DialogResult yearResult = createYear.Result;
                DialogResult projectResult = projectNameFrm.Result;
                string[] getNodeNameArray = new string[4];
                if (dialogResult == DialogResult.Yes && yearResult == DialogResult.None)
                {
                    clientForm clientForm = new clientForm();
                    clientFormLocation = clientForm.ClientLocation;
                    getNodeNameArray = clientFormLocation.Split('\\');
                    btnCreateYear.Visible = false;
                    btnCreateClient.Visible = false;
                    btnCreateProject.Visible = true;
                    clientForm.Close();
                    clientForm.Result = DialogResult.None;
                    createYear.Result = DialogResult.None;
                }
                else if (dialogResult == DialogResult.No && yearResult == DialogResult.None)
                {
                    clientFormLocation = clientForm.ClientLocation;
                    clientCountry = clientForm.CountryName;

                    btnCreateYear.Visible = true;
                    btnCreateClient.Visible = false;
                    btnCreateProject.Visible = false;
                    clientForm.Result = DialogResult.None;
                    createYear.Result = DialogResult.None;

                }
                else if (dialogResult == DialogResult.No && yearResult == DialogResult.No)
                {
                    clientFormLocation = clientForm.ClientLocation;
                    clientForm.Result = DialogResult.None;
                    createYear.Result = DialogResult.None;

                }

                if (yearResult == DialogResult.Yes && dialogResult == DialogResult.Yes)
                {
                    btnCreateProject.Visible = true;
                    btnCreateClient.Visible = false;
                    btnCreateYear.Visible = false;
                    createYear.Close();
                    clientForm.Result = DialogResult.None;
                    createYear.Result = DialogResult.None;

                }
                else if (yearResult == DialogResult.No && dialogResult == DialogResult.Yes)
                {
                    btnCreateYear.Visible = false;
                    btnCreateProject.Visible = true;
                    btnCreateClient.Visible = false;
                    createYear.Close();
                    clientForm.Result = DialogResult.None;
                    createYear.Result = DialogResult.None;

                }
                else if (yearResult == DialogResult.No && dialogResult == DialogResult.No)
                {
                    btnCreateYear.Visible = false;
                    btnCreateProject.Visible = true;
                    btnCreateClient.Visible = false;
                    createYear.Close();
                    clientForm.Result = DialogResult.None;
                    createYear.Result = DialogResult.None;
                }
                else if (yearResult == DialogResult.Cancel)
                {
                    clientFormLocation = string.Empty;
                    clientForm.Result = DialogResult.None;
                    createYear.Result = DialogResult.None;
                    projectNameFrm.Result = DialogResult.None;
                }

                if (projectResult == DialogResult.Yes)
                {
                    projectLocation = createYear.clientLocation;
                    btnCreateYear.Visible = false;
                    btnCreateProject.Visible = false;
                    btnCreateClient.Visible = true;
                    clientForm.Result = DialogResult.None;
                    createYear.Result = DialogResult.None;
                    projectNameFrm.Result = DialogResult.None;
                }
                else if (projectResult == DialogResult.Cancel)
                {
                    clientFormLocation = string.Empty;
                    clientForm.Result = DialogResult.None;
                    createYear.Result = DialogResult.None;
                    projectNameFrm.Result = DialogResult.None;
                }

                //open loader
                //activate the loader
                btnShowFiles_Click(null, null);

                GetComboxFromDb();

                //GetCurrentUser();
                //close loader
                //if (loader.InvokeRequired)
                //{
                //    loader.Invoke(new MethodInvoker(loader.CloseLoader));
                //}
            }
            //if (loader.InvokeRequired)
            //{
            //    loader.Invoke(new MethodInvoker(loader.CloseLoader));
            //}
        }

        private void GetComboxFromDb()
        {
            try
            {
                GetAccessTypes();

                GetOrganisationalStructures();

                GetRuleTypes();

                GetScopTypes();
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void GetScopTypes()
        {
            try
            {
                scopeTypeModelList = TypesController.GetScopeTypes();
                PopulateCombobox(cmbScopeType.Name);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Rule Types Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void GetRuleTypes()
        {
            try
            {
                ruleTypeModelList = TypesController.GetRuleTypes();
                PopulateCombobox(cmbRuleType.Name);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "GRule Types Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void GetOrganisationalStructures()
        {
            try
            {
                organisationalStructuresModelList = TypesController.GetOrganisationalStructure();
                PopulateCombobox(cmbOrganisationalStructure.Name);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Organisational Structure Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void GetAccessTypes()
        {
            try
            {
                accessTypesModelList = TypesController.GetAccessTypes();
                PopulateCombobox(cmbAccessType.Name);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Access Types Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void PopulateCombobox(string comboxName)
        {
            try
            {
                switch (comboxName)
                {
                    case "cmbAccessType":
                        cmbAccessType.DataSource = accessTypesModelList;
                        cmbAccessType.DisplayMember = "Name";
                        break;
                    case "cmbOrganisationalStructure":
                        cmbOrganisationalStructure.DataSource = organisationalStructuresModelList;
                        cmbOrganisationalStructure.DisplayMember = "Name";
                        break;
                    case "cmbRuleType":
                        cmbRuleType.DataSource = ruleTypeModelList;
                        cmbRuleType.DisplayMember = "Name";
                        break;
                    default:
                        cmbScopeType.DataSource = scopeTypeModelList;
                        cmbScopeType.DisplayMember = "Name";
                        break;
                }
                cmbScopeType.Text = "Please select Scope Type";
                cmbRuleType.Text = "Please select Rule Type";
                cmbOrganisationalStructure.Text = "Please Select Organisational Structure";
                cmbAccessType.Text = "Please Select Access Type";
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Populate Combobox Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void GetCurrentUser(string onwer)
        {
            try
            {
                //string currentUserName = Environment.UserName;
                // string domainName = System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName;//"DCESTAGIO"
                PrincipalContext principalContext = new PrincipalContext(ContextType.Domain, domainName);
                UserPrincipal userPrincipal = UserPrincipal.FindByIdentity(principalContext, onwer);

                if (userPrincipal != null)
                {
                    PrincipalSearchResult<Principal> groups = userPrincipal.GetAuthorizationGroups();
                    List<Principal> groupsFilter = new List<Principal>();//= userPrincipal.GetAuthorizationGroups();
                    foreach (var item in organisationalStructuresModelList)
                    {
                        //isOrgStr = groups.Where(x => x.Name == item.Name).FirstOrDefault().Name == null ? true: false;
                        var memberOfGroups = groups.Where(g => g.SamAccountName.Contains(item.Name)).ToList();
                        if (memberOfGroups.Count > 0)
                        {
                            groupsFilter.AddRange(memberOfGroups);
                        }
                    }
                    if (groupsFilter.Count > 0)
                    {

                        //get groups that appear only in the organisational structure only
                        // iterate over all groups

                        foreach (Principal p in groupsFilter)//foreach (Principal p in groups)
                        {
                            // make sure to add only group principals
                            if (p is GroupPrincipal)
                            {
                                result.Add((GroupPrincipal)p);
                                //using (principalContext)
                                //{
                                GroupPrincipal grp = GroupPrincipal.FindByIdentity(principalContext, p.Name);
                                if (grp != null)
                                {
                                    var members = grp.GetMembers(true);
                                    foreach (Principal principal in members)
                                    {
                                        string username = string.Empty;

                                        if (principal is UserPrincipal u)
                                        {
                                            username = u.SamAccountName.ToLower();
                                        }

                                        if (!string.IsNullOrEmpty(username) && !usernames.Contains(username))
                                        {
                                            usernames.Add(username);
                                        }
                                    }
                                }
                                //}
                            }
                        }

                    }

                }                //todo::: close application
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Get Current User", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnShowFiles_Click(object sender, EventArgs e)
        {
            try
            {

                treeViewFolder.Nodes.Clear();
                string[] drives = Environment.GetLogicalDrives();
                //TODO:::add functionality that will allow for the X drive enhancement 
                //and move the u drive below to the app.config file
                string drive = drives.Where(d => d == "U:\\").FirstOrDefault();
                if (!string.IsNullOrEmpty(drive))
                {
                    DriveInfo driveInfo = new DriveInfo(drive);
                    TreeNode treeNode = new TreeNode(drive, 13, 13);
                    treeViewFolder.Nodes.Add(treeNode);
                    FillDirectory(treeNode, 0);
                }
                else
                {
                    if (MessageBox.Show("You are not connected to the domain network,please contact Admin", "Domain Network", MessageBoxButtons.OK, MessageBoxIcon.Information) == DialogResult.OK)
                    {
                        Application.Exit();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            }
        }

        private void FillDirectory(TreeNode treeNode, int level)
        {
            if (Directory.Exists(treeNode.FullPath))
            {
                string[] directories = Directory.GetDirectories(treeNode.FullPath);

                DirectoryInfo rootDirectories = new DirectoryInfo(treeNode.FullPath);

                foreach (var file in rootDirectories.GetFiles())
                {
                    TreeNode n = new TreeNode(file.Name, 13, 13);
                    treeNode.Nodes.Add(n);
                }
                foreach (string directory in directories)
                {
                    DirectoryInfo di = new DirectoryInfo(directory);

                    TreeNode node = new TreeNode(di.Name, 0, 1);

                    try
                    {
                        node.Tag = directory;
                        if (level > 0)
                        {
                            if (di.GetDirectories().Count() > 0)
                            {
                                TreeNode childNode = new TreeNode(di.Name, 4, 4);
                                FillDirectory(node, 0);
                            }

                            foreach (var file in di.GetFiles())
                            {
                                TreeNode n = new TreeNode(file.Name, 13, 13);
                                node.Nodes.Add(n);
                            }
                        }
                    }
                    catch (UnauthorizedAccessException)
                    {
                        node.ImageIndex = 12;
                        node.SelectedImageIndex = 12;
                    }
                    catch (Exception)
                    {
                        continue;
                    }
                    finally
                    {
                        treeNode.Nodes.Add(node);
                    }
                }
            }
        }

        private void treeViewFolder_AfterSelect(object sender, TreeViewEventArgs e)
        {
            //cde below is activated when a user clicks a file or folder in the treenode
            //the first run is it checks the current location loaction clicked and if it is fit for getting permissions
            //once getting the current location it sets the treenode to current location and gets the files and folders 
            //of the current location,that is the second run
            try
            {
                //thread.Start();
                isOwner = false;
                if (permissionDataGrid.DataSource != null)
                {
                    permissionDataGrid.DataSource = null;
                }
                thread = new Thread(Load);

                thread.Start();

                userPermissionModelList = new List<UserPermissionModel>(); ;
                TreeNode treeNode = e.Node;
                string fullPath = treeNode.FullPath;
                int fileCount = GetFilesInfo(fullPath);
                DirectoryInfo directoryInfo = new DirectoryInfo(fullPath);
                FileSecurity fileSecurity = File.GetAccessControl(fullPath);
                IdentityReference identityReference = fileSecurity.GetOwner(typeof(SecurityIdentifier));
                NTAccount ntAccount = identityReference.Translate(typeof(NTAccount)) as NTAccount;
                string[] userName = ntAccount.Value.Split('\\');
                UsersModel dbUserModel = UsersController.GetUserByUserName(userName[1].ToLower());
                labelOwner.Text = dbUserModel.Id != 0 ? "Owner : " + dbUserModel.FirstName + " " + dbUserModel.LastName : "Owner : " + ntAccount.Value;
                labelLastModfied.Text = "Last Modified : " + directoryInfo.LastWriteTime.ToString("dd-MMM-yyyy");
                labelFileCount.Text = "Folder Count : " + fileCount.ToString();
                lblFolderName.Text = "Current Folder Name : " + directoryInfo.Name;
                //current folder group
                principalContext = new PrincipalContext(ContextType.Domain, domainName);

                // find your user in both db and active directory
                string currentUserName = Environment.UserName;
                UserPrincipal userPrincipal = UserPrincipal.FindByIdentity(principalContext, currentUserName);

                UserPrincipal user = new UserPrincipal(principalContext);
                if (dbUserModel.Id > 0)
                {
                    user = UserPrincipal.FindByIdentity(principalContext, ntAccount.Value);
                    bool isUserInRole = UsersController.IsUserInRole(dbUserModel.Id, 6);
                    //TODO:::get groups and check if user is in group
                    if ((user.SamAccountName == userPrincipal.SamAccountName) || (isUserInRole))
                    {
                        isOwner = true;
                    }
                    else
                    {
                        GetCurrentUser(userPrincipal.SamAccountName.ToLower());
                        var findOwner = usernames.Where(x => x == dbUserModel.UserName.ToLower()).FirstOrDefault();
                        if (findOwner != null)
                        {
                            isOwner = true;
                        }
                    }
                }

                if (userPrincipal != null)
                {
                    // get owner by id
                    userModel = UsersController.GetUserFromDb(userPrincipal.GivenName, userPrincipal.Surname);
                }

                if (treeNode.Level == 0 && !string.IsNullOrEmpty(clientFormLocation))
                {
                    string[] directorySubstring = clientFormLocation.Split('\\');
                    string name = directorySubstring[2].TrimStart().TrimEnd();

                    foreach (TreeNode node in treeViewFolder.Nodes)
                    {
                        if (node.Nodes.Count > 0)
                        {
                            foreach (TreeNode child in node.Nodes)
                            {
                                if (child.Text == name)
                                {
                                    if (child.Nodes.Count > 0)
                                    {
                                        foreach (TreeNode item in child.Nodes)
                                        {
                                            if (item.Text == directorySubstring[3])
                                            {
                                                if (item.Nodes.Count > 0)
                                                {
                                                    foreach (TreeNode yearNode in item.Nodes)
                                                    {
                                                        if (yearNode.Text == directorySubstring[4])
                                                        {
                                                            if (yearNode.Nodes.Count > 0)
                                                            {
                                                                foreach (TreeNode projectNode in yearNode.Nodes)
                                                                {
                                                                    if (projectNode.Text == projectNameFrm.projectName)
                                                                    {
                                                                        projectNode.Expand();
                                                                        projectNode.TreeView.SelectedNode = projectNode;
                                                                        FillDirectory(projectNode, projectNode.Level);
                                                                        btnCreateClient.Visible = true;
                                                                        btnCreateProject.Visible = false;
                                                                        btnCreateYear.Visible = false;
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                yearNode.Expand();
                                                                yearNode.TreeView.SelectedNode = yearNode;
                                                                FillDirectory(yearNode, yearNode.Level);
                                                                btnCreateClient.Visible = false;
                                                                btnCreateProject.Visible = true;
                                                                btnCreateYear.Visible = false;
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    item.Expand();
                                                    item.TreeView.SelectedNode = item;
                                                    FillDirectory(item, item.Level);
                                                    btnCreateClient.Visible = false;
                                                    btnCreateProject.Visible = false;
                                                    btnCreateYear.Visible = true;

                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        child.Expand();
                                        child.TreeView.SelectedNode = child;
                                        // FillDirectory(child, child.Level);
                                        btnCreateClient.Visible = true;
                                        btnCreateProject.Visible = false;
                                        btnCreateYear.Visible = false;
                                    }
                                }
                            }
                        }
                    }
                }
                else if (treeNode.Level == 0 && string.IsNullOrEmpty(clientFormLocation))
                {
                    if (treeNode.Level > 0)
                    {
                        FillDirectory(treeNode, treeNode.Level);

                        btnCreateClient.Visible = true;
                    }
                }

                if (treeNode.Level == 1)
                {
                    string name = string.Empty;
                    string[] directorySubstring = treeNode.FullPath.Split('\\');
                    name = directorySubstring[2].TrimStart().TrimEnd();
                    clientFormLocation = treeNode.FullPath;
                    labelOwner.Text = dbUserModel.Id != 0 ? "Owner : " + dbUserModel.FirstName + " " + dbUserModel.LastName : "Owner : " + ntAccount.Value;
                    if (Directory.Exists(treeNode.FullPath))
                    {
                        foreach (TreeNode node in treeViewFolder.Nodes)
                        {
                            if (node.Nodes.Count > 0)
                            {
                                foreach (TreeNode child in node.Nodes)
                                {
                                    if (child.Text == name)
                                    {
                                        child.Expand();
                                        child.TreeView.SelectedNode = child;
                                        if (!string.IsNullOrEmpty(clientFormLocation))
                                        {
                                            if (child.Nodes.Count == 0)
                                            {
                                                FillDirectory(child, child.Level);
                                            }
                                        }
                                        btnCreateClient.Visible = true;
                                        btnCreateProject.Visible = false;
                                        btnCreateYear.Visible = false;
                                        button1.Enabled = false;
                                    }
                                }
                            }
                        }

                    }
                    else
                    {
                        //get file info
                        FileInfo fileInfo = new FileInfo(treeNode.FullPath);

                        labelLastModfied.Text = "Last Modified : " + directoryInfo.LastWriteTime.ToString("dd-MMM-yyyy");
                        labelFileCount.Text = "Folder Count : " + fileCount.ToString();


                    }
                }
                else if (treeNode.Level == 2)
                {
                    //get client
                    string[] directorySubstring = treeNode.FullPath.Split('\\');
                    clientName = directorySubstring[3];

                    clientFormLocation = treeNode.FullPath;

                    country = CountriesController.GetCountryByName(directorySubstring[2]);
                    //if country does not exits,create and return
                    if (country.Id == 0)
                    {
                        country = CountriesController.InsertIntoCountries(directorySubstring[2]);
                    }
                    clientModel = ClientController.GetClientByName(clientName);
                    //if client does not exits,create and return
                    if (clientModel.Id == 0)
                    {
                        clientModel = ClientController.InsertIntoClients(clientName, userModel.Id, country.Id);
                    }

                    if (clientModel.Id > 0)
                    {
                        ClientsModel folderOwnerModel = ClientController.GetClientById(clientModel.Id);
                        UsersModel folderOwner = UsersController.GetUserById(folderOwnerModel.UserId);
                        labelOwner.Text = "Owner : " + folderOwner.FirstName + " " + folderOwner.LastName;//ntAccount.Value;
                        //assign this user as owner based on the db ownership
                        Console.Write(user.SamAccountName);
                        Console.Write(userPrincipal.SamAccountName);
                        if (folderOwner.Id != 0 && userPrincipal.SamAccountName.ToLower() == folderOwner.UserName.ToLower())
                        {
                            isOwner = true;
                        }
                        List<ClientProjectModel> clientProjectModelsList = new List<ClientProjectModel>();
                        clientProjectModelsList = ClientProjectController.GetClientProjectsByClientId(clientModel.Id);
                        List<PermissionsModel> permissionsModelListForClientProject = new List<PermissionsModel>();
                        foreach (var clientProjectModel in clientProjectModelsList)
                        {
                            var clientProject = ClientProjectController.GetClientProjectModelPermission(clientProjectModel.Id);
                            foreach (PermissionsModel item in clientProject)
                            {
                                permissionsModelListForClientProject.Add(item);
                            }
                        }
                        userPermissionModelList = new List<UserPermissionModel>();
                        foreach (PermissionsModel item in permissionsModelListForClientProject)
                        {
                            UserPermissionModel userPermissionModel = new UserPermissionModel
                            {
                                PermissionId = item.Id,
                                AccessType = item.AccessType,
                                OrganisationalStructure = item.OrganisationalStructure,
                                RuleType = item.RuleType,
                                ScopeType = item.ScopeType,
                            };
                            userPermissionModelList.Add(userPermissionModel);
                        }
                        permissionDataGrid.DataSource = userPermissionModelList.ToArray();
                        if (permissionDataGrid.Columns.Count > 0)
                        {
                            permissionDataGrid.Columns["PermissionId"].Visible = false;
                        }
                        clientName = string.Empty;
                        countryName = string.Empty;
                    }
                    foreach (TreeNode node in treeViewFolder.Nodes)
                    {
                        if (node.Nodes.Count > 0)
                        {
                            foreach (TreeNode child in node.Nodes)
                            {
                                if (child.Text == country.Name)
                                {
                                    foreach (TreeNode item in child.Nodes)
                                    {
                                        if (item.Text == clientModel.Name)
                                        {
                                            item.Expand();
                                            item.TreeView.SelectedNode = item;
                                            if (item.Nodes.Count == 0)
                                            {
                                                FillDirectory(item, item.Level);
                                            }
                                            btnCreateProject.Visible = false;
                                            btnCreateYear.Visible = true;
                                            btnCreateClient.Visible = false;
                                            button1.Enabled = true;

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else if (treeNode.Level == 3)
                {
                    //get year
                    string[] directorySubstring = treeNode.FullPath.Split('\\');
                    year = directorySubstring[4];
                    clientFormLocation = treeNode.FullPath;

                    foreach (TreeNode node in treeViewFolder.Nodes)
                    {
                        if (node.Nodes.Count > 0)
                        {
                            foreach (TreeNode child in node.Nodes)
                            {
                                if (child.Text == directorySubstring[2])
                                {
                                    foreach (TreeNode item in child.Nodes)
                                    {
                                        if (item.Text == directorySubstring[3])
                                        {
                                            if (item.Nodes.Count > 0)
                                            {
                                                foreach (TreeNode yearNode in item.Nodes)
                                                {
                                                    if (yearNode.Text == directorySubstring[4])
                                                    {
                                                        yearNode.Expand();
                                                        yearNode.TreeView.SelectedNode = yearNode;
                                                        if (yearNode.Nodes.Count == 0)
                                                        {
                                                            FillDirectory(yearNode, yearNode.Level);
                                                        }
                                                        btnCreateClient.Visible = false;
                                                        btnCreateProject.Visible = true;
                                                        btnCreateYear.Visible = false;
                                                        button1.Enabled = false;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                item.Expand();
                                                item.TreeView.SelectedNode = item;
                                                if (item.Nodes.Count == 0)
                                                {
                                                    FillDirectory(item, item.Level);
                                                }
                                                btnCreateClient.Visible = false;
                                                btnCreateProject.Visible = true;
                                                btnCreateYear.Visible = false;
                                                button1.Enabled = false;

                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else if (treeNode.Level > 3)
                {
                    string tagString = (string)treeNode.Tag;
                    if (string.IsNullOrEmpty(tagString))
                    {
                        string[] stringPath = treeNode.FullPath.Split('\\');
                        if (File.Exists(treeNode.FullPath))
                        {
                            string LastUser = ProcessFile(treeNode.FullPath, 10);
                            UserPrincipal userWhoModifiedFilePrincipal = UserPrincipal.FindByIdentity(principalContext, LastUser);
                            lblUsername.Text = userWhoModifiedFilePrincipal.ToString();
                            DateTime LastModifiedDate = Convert.ToDateTime(ProcessFile(treeNode.FullPath, 3));
                            lblAccessDate.Text = LastModifiedDate.ToString();
                        }
                        else if (Directory.Exists(treeNode.FullPath))
                        {

                        }
                    }
                    else
                    {
                        //get node where level > 3 i.e projects and/or 
                        string tag = treeNode.Tag.ToString();
                        string[] stringPath = tag.Split('\\');
                        countryName = stringPath[2];
                        clientName = stringPath[3];
                        year = stringPath[4];

                        string projectName = stringPath[5];

                        projectsModel = ProjectsController.GetProjectByName(projectName);

                        if (projectsModel.Id == 0)
                        {
                            clientModel = ClientController.GetClientByName(clientName);
                            //create new project based on the information of user and current folder then recall the method for getting project using this info
                            projectsModel = ProjectsController.CreateProject(projectName, clientModel.Id, year);
                        }
                        country = CountriesController.GetCountryByName(countryName);
                        //if country does not exits,create and return
                        if (country.Id == 0)
                        {
                            country = CountriesController.InsertIntoCountries(clientName);
                        }
                        clientModel = ClientController.GetClientByName(clientName);
                        //if client does not exits,create and return
                        if (clientModel.Id == 0)
                        {
                            clientModel = ClientController.InsertIntoClients(clientName, userModel.Id, country.Id);
                        }

                        //code that checks project owner
                        if (ntAccount.Value == "QED\\QEDClients")
                        {
                            //Get client owner
                            var fs = File.GetAccessControl(treeNode.FullPath);
                            UsersModel client = UsersController.GetUserById(clientModel.UserId);
                            var newNtAccount = new NTAccount(domainName, client.UserName);
                            fs.SetOwner(newNtAccount);
                            labelOwner.Text = client.Id != 0 ? "Owner : " + client.FirstName + " " + client.LastName : "Owner : " + newNtAccount.Value;
                            if ((client.UserName.ToLower() == userPrincipal.SamAccountName.ToLower()))
                            {
                                isOwner = true;
                            }
                            else
                            {
                                GetCurrentUser(client.UserName.ToLower());
                                var findOwner = usernames.Where(x => x == client.UserName.ToLower()).FirstOrDefault();
                                if (findOwner != null)
                                {
                                    isOwner = true;
                                }
                            }
                        }

                        if (projectsModel.Id > 0)
                        {
                            List<ClientProjectModel> clientProjectModelsList = new List<ClientProjectModel>();
                            clientProjectModelsList = ProjectsController.GetClientProjectsByProjectId(projectsModel.Id);
                            List<PermissionsModel> permissionsModelListForClientProject = new List<PermissionsModel>();

                            foreach (var clientProjectModel in clientProjectModelsList)
                            {
                                var clientProject = ProjectsController.GetClientProjectModelPermission(clientProjectModel.Id);
                                foreach (var item in clientProject)
                                {
                                    permissionsModelListForClientProject.Add(item);
                                }
                            }

                            userPermissionModelList = new List<UserPermissionModel>();
                            for (int i = 0; i < permissionsModelListForClientProject.Count; i++)
                            {
                                UserPermissionModel userPermissionModel = new UserPermissionModel
                                {
                                    PermissionId = permissionsModelListForClientProject[i].Id,
                                    AccessType = permissionsModelListForClientProject[i].AccessType,
                                    OrganisationalStructure = permissionsModelListForClientProject[i].OrganisationalStructure,
                                    RuleType = permissionsModelListForClientProject[i].RuleType,
                                    ScopeType = permissionsModelListForClientProject[i].ScopeType,
                                };
                                userPermissionModelList.Add(userPermissionModel);
                            }
                            //display on grid
                            //add data to datagrid
                            permissionDataGrid.DataSource = userPermissionModelList.ToArray();
                            if (permissionDataGrid.Columns.Count > 0)
                            {
                                permissionDataGrid.Columns["PermissionId"].Visible = false;
                            }
                            clientName = string.Empty;
                            countryName = string.Empty;
                            foreach (TreeNode node in treeViewFolder.Nodes)
                            {
                                if (node.Nodes.Count > 0)
                                {
                                    foreach (TreeNode child in node.Nodes)
                                    {
                                        if (child.Text == country.Name)
                                        {
                                            foreach (TreeNode item in child.Nodes)
                                            {
                                                if (item.Text == clientModel.Name)
                                                {
                                                    if (item.Nodes.Count > 0)
                                                    {
                                                        foreach (TreeNode yearNode in item.Nodes)
                                                        {
                                                            if (yearNode.Nodes.Count > 0)
                                                            {
                                                                foreach (TreeNode projectNode in yearNode.Nodes)
                                                                {
                                                                    if (projectNode.Text == projectsModel.Name)
                                                                    {
                                                                        if (treeNode.Level == 4)
                                                                        {
                                                                            projectNode.Expand();
                                                                            projectNode.TreeView.SelectedNode = projectNode;
                                                                        }
                                                                        if (!string.IsNullOrEmpty(clientFormLocation))
                                                                        {

                                                                            if (projectNode.Nodes.Count == 0)
                                                                            {
                                                                                FillDirectory(projectNode, projectNode.Level);
                                                                                GetSubFolders(projectNode);
                                                                            }
                                                                            if (treeNode.Level >= 6)
                                                                            {
                                                                                projectNode.Expand();
                                                                                projectNode.TreeView.SelectedNode = treeNode;
                                                                                if (treeNode.Nodes.Count == 0)
                                                                                {
                                                                                    FillDirectory(treeNode, treeNode.Level);
                                                                                    GetSubFolders(treeNode);
                                                                                }
                                                                            }
                                                                        }
                                                                        button1.Enabled = true;

                                                                        btnCreateClient.Visible = true;
                                                                        btnCreateProject.Visible = false;
                                                                        btnCreateYear.Visible = false;
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                if (yearNode.Text == year.ToString())
                                                                {
                                                                    yearNode.Expand();
                                                                    yearNode.TreeView.SelectedNode = yearNode;
                                                                    btnCreateClient.Visible = false;
                                                                    btnCreateProject.Visible = false;
                                                                    btnCreateYear.Visible = true;
                                                                    button1.Enabled = false;

                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        item.Expand();
                                                        item.TreeView.SelectedNode = item;
                                                        btnCreateClient.Visible = true;
                                                        btnCreateProject.Visible = false;
                                                        btnCreateYear.Visible = false;
                                                        button1.Enabled = false;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        clientFormLocation = treeNode.FullPath;
                    }
                }
                if (loader.InvokeRequired)
                {
                    loader.Invoke(new MethodInvoker(loader.CloseLoader));
                }
                Activate();
            }
            catch (Exception exception)
            {
                if (loader.InvokeRequired)
                {
                    loader.Invoke(new MethodInvoker(loader.CloseLoader));
                }

                clientName = string.Empty;
                countryName = string.Empty;
                MessageBox.Show(exception.Message + ".Please contact Admin.", "Tree Node Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Activate();
            }
        }

        private string ProcessFile(string fullPath, params int[] indexes)
        {
            try
            {
                FileInfo fileInfo = new FileInfo(fullPath);
                string fileName = Path.GetFileName(fullPath);
                string folderName = Path.GetDirectoryName(fullPath);
                Shell32.Shell shell = new Shell32.Shell();
                Shell32.Folder objFolder;
                objFolder = shell.NameSpace(folderName);
                StringBuilder sb = new StringBuilder();

                foreach (Shell32.FolderItem2 item in objFolder.Items())
                {
                    if (fileName == item.Name)
                    {
                        for (int i = 0; i < indexes.Length; i++)
                        {
                            sb.Append(objFolder.GetDetailsOf(item, indexes[i]) + ",");
                        }

                        break;
                    }
                }

                string result = sb.ToString().Trim();
                //Protection for no results causing an exception on the `SubString` method
                if (result.Length == 0)
                {
                    return string.Empty;
                }
                return result.Substring(0, result.Length - 1);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        private void GetSubFolders(TreeNode projectNode)
        {
            try
            {
                for (int i = 0; i < projectNode.Nodes.Count; i++)
                {
                    FillDirectory(projectNode.Nodes[i], projectNode.Level);
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        private int GetFilesInfo(string fullPath)
        {
            try
            {
                DirectoryInfo directory = new DirectoryInfo(fullPath);
                if (!directory.Exists)
                {
                    return 0;
                }
                // get directory folders number
                int fileCount = directory.GetDirectories().Length;
                return fileCount;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        private void btnCreateYear_Click(object sender, EventArgs e)
        {
            try
            {
                createYear createYear = new createYear();
                if (!string.IsNullOrEmpty(clientFormLocation))
                {
                    string[] substringChecker = clientFormLocation.Split('\\');
                    if (substringChecker.Count() > 3)
                    {
                        createYear.country = substringChecker[2];
                        createYear.clientLocation = substringChecker[0] + '\\' + substringChecker[1] + '\\' + substringChecker[2] + '\\' + substringChecker[3];
                    }
                    else
                    {
                        createYear.country = clientCountry;
                        createYear.clientLocation = clientFormLocation;
                    }
                }
                else
                {
                    createYear.country = treeViewFolder.SelectedNode.FullPath.Split('\\')[2];
                    createYear.clientLocation = treeViewFolder.SelectedNode.FullPath;
                }

                createYear.Show();
                Hide();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Create Year Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCreateProject_Click(object sender, EventArgs e)
        {
            try
            {
                projectNameFrm createProject = new projectNameFrm();
                projectNameFrm.currentLocation = clientFormLocation;
                createProject.Show();
                Hide();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Create Project Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCreateClient_Click(object sender, EventArgs e)
        {
            try
            {
                clientFormLocation = string.Empty;
                clientForm clientForm = new clientForm();
                clientForm.Show();
                Hide();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Create Client Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        ClientProjectModel clientProjectModel = new ClientProjectModel();

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (isOwner)
                {
                    if (cmbAccessType.SelectedIndex != -1 && cmbOrganisationalStructure.SelectedIndex != -1 && cmbRuleType.SelectedIndex != -1 && cmbScopeType.SelectedIndex != -1)
                    {
                        thread = new Thread(Load);

                        thread.Start();

                        //add code saving a permssion
                        int accessTypeId = ((AccessTypesModel)cmbAccessType.SelectedValue).Id;
                        int organisationalStructureId = ((OrganisationalStructuresModel)cmbOrganisationalStructure.SelectedValue).Id;
                        int ruleTypeId = ((RuleTypeModel)cmbRuleType.SelectedValue).Id;
                        int scopeType = ((ScopeTypeModel)cmbScopeType.SelectedValue).Id;
                        clientProjectModel = new ClientProjectModel();
                        //determine if the save is for a client or the save is for a project

                        string columnString = string.Empty;
                        int userId = userModel.Id;
                        int selectedNode = treeViewFolder.SelectedNode.Level;
                        if (clientModel.Id > 0 && selectedNode == 2)
                        {
                            columnString = "client_id";
                            string valuesString = clientModel.Id.ToString();

                            //check if exists
                            List<ClientProjectModel> clientProjectList = ClientProjectController.GetClientProjectsByClientId(clientModel.Id);
                            if (clientProjectList.Count == 0)
                            {
                                clientProjectModel = ClientProjectController.InsertIntoClientProject(columnString, valuesString);
                                //set all projects
                                SetClientSubFolders(userModel.Id, accessTypeId, organisationalStructureId, ruleTypeId, scopeType);
                            }
                            else
                            {
                                foreach (var item in clientProjectList)
                                {
                                    clientProjectModel = item;
                                    SetPermissionsDbAndActDir(clientProjectModel.Id, userModel.Id, ((AccessTypesModel)cmbAccessType.SelectedValue).Id,
                                    ((OrganisationalStructuresModel)cmbOrganisationalStructure.SelectedValue).Id, ((RuleTypeModel)cmbRuleType.SelectedValue).Id,
                                    ((ScopeTypeModel)cmbScopeType.SelectedValue).Id);

                                    //set all projects
                                    if (!showInverse)
                                    {
                                        SetClientSubFolders(userModel.Id, accessTypeId, organisationalStructureId, ruleTypeId, scopeType);
                                    }                                    //check duplicates
                                }
                                clientProjectModel = new ClientProjectModel();
                            }
                            //}
                        }
                        else if (projectsModel.Id > 0 && selectedNode == 4)
                        {
                            columnString = "project_id";
                            string valuesString = projectsModel.Id.ToString();
                            List<ClientProjectModel> clientProjectList = ClientProjectController.GetClientProjectsByProjectId(projectsModel.Id);
                            if (clientProjectList.Count == 0)
                            {
                                clientProjectModel = ClientProjectController.InsertIntoClientProject(columnString, valuesString);
                            }
                            else if (clientProjectList.Count > 0)
                            {
                                List<PermissionsModel> permissionsList = new List<PermissionsModel>();
                                foreach (var item in clientProjectList)
                                {
                                    permissionsList = PermissionsController.GetPermissionByClientProjectId(item.Id, accessTypeId, organisationalStructureId, ruleTypeId, scopeType);
                                    if (permissionsList.Count == 0)
                                    {
                                        clientFormLocation = treeViewFolder.SelectedNode.FullPath;
                                        SetPermissionForSubFolders(item.Id, userId, accessTypeId, organisationalStructureId, ruleTypeId, scopeType);
                                        string[] pathSplit = clientFormLocation.Split('\\');
                                        if (string.IsNullOrEmpty(savePath))
                                        {
                                            string clientOrProject = pathSplit.Count() > 4 ? "Project" : "Client";
                                            savePath = pathSplit.Count() > 4 ? pathSplit[2] + '\\' + pathSplit[3] + '\\' + pathSplit[4] + '\\' + pathSplit[5] : pathSplit[2] + "\\" + pathSplit[3];
                                            bundledPerm = ";" + organisationalStructureId + ";" + accessTypeId + ";" + ruleTypeId + ";Add;" + clientOrProject;
                                        }
                                        if (!showInverse)
                                        {
                                            Common.SetPermissionsInActiveDirectory(savePath + bundledPerm);
                                        }

                                    }
                                }
                                if (permissionsList.Count == 0 && !showInverse)
                                {
                                    UserPermissionModel userPermissionModel = new UserPermissionModel();
                                    userPermissionModel.ScopeType = ((ScopeTypeModel)cmbScopeType.SelectedValue).Name;
                                    userPermissionModel.RuleType = ((RuleTypeModel)cmbRuleType.SelectedValue).Name;
                                    userPermissionModel.AccessType = ((AccessTypesModel)cmbAccessType.SelectedValue).Name;
                                    userPermissionModel.OrganisationalStructure = ((OrganisationalStructuresModel)cmbOrganisationalStructure.SelectedValue).Name;
                                    userPermissionModel.PermissionId = returnId;
                                    userPermissionModelList.Add(userPermissionModel);
                                    permissionDataGrid.DataSource = null;
                                    permissionDataGrid.DataSource = userPermissionModelList;
                                    if (permissionDataGrid.Columns.Count > 0)
                                    {
                                        permissionDataGrid.Columns["PermissionId"].Visible = false;
                                    }
                                }
                            }
                        }

                        if (!showInverse)
                        {
                            SetPermissionsDbAndActDir(clientProjectModel.Id, userModel.Id, ((AccessTypesModel)cmbAccessType.SelectedValue).Id,
                                ((OrganisationalStructuresModel)cmbOrganisationalStructure.SelectedValue).Id, ((RuleTypeModel)cmbRuleType.SelectedValue).Id,
                                ((ScopeTypeModel)cmbScopeType.SelectedValue).Id);
                        }
                        else
                        {
                            //if (loader.InvokeRequired)
                            //{
                            loader.Invoke(new MethodInvoker(loader.CloseLoader));
                            //}
                            MessageBox.Show("Please edit or delete inverse first or contact folder owner or Admin.", "Inverse Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            Activate();
                        }
                        clientModel = new ClientsModel();
                        projectsModel = new ProjectsModel();
                        clientFormLocation = string.Empty;
                        showInverse = false;
                        //close loader
                        if (loader.InvokeRequired)
                        {
                            loader.Invoke(new MethodInvoker(loader.CloseLoader));
                        }
                        //Activate();
                        //reset combobox
                        resetCombobox();
                    }
                    else
                    {
                        if (loader.InvokeRequired)
                        {
                            loader.Invoke(new MethodInvoker(loader.CloseLoader));
                        }
                        Activate();
                        MessageBox.Show("Please make sure you have selected all combox boxes", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                else
                {
                    if (loader.InvokeRequired)
                    {
                        loader.Invoke(new MethodInvoker(loader.CloseLoader));
                    }
                    Activate();
                    MessageBox.Show("Only folder owner can create a permission for this folder", "Authority Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception exception)
            {
                if (loader.InvokeRequired)
                {
                    loader.Invoke(new MethodInvoker(loader.CloseLoader));
                }
                //Activate();
                MessageBox.Show(exception.Message, "Save Exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void SetPermissionsDbAndActDir(int id, int userId, int accessTypeId, int organisationalStructureId, int ruleTypeId, int scopeType)
        {
            try
            {
                if (clientProjectModel.Id > 0)
                {
                    //userId = ;
                    string valuesString = clientProjectModel.Id + "," + accessTypeId + "," + organisationalStructureId + "," + ruleTypeId + "," + scopeType + "," + userId;
                    //Check permissions
                    List<PermissionsModel> permissionList = PermissionsController.GetPermissionByClientProjectId(clientProjectModel.Id, accessTypeId, organisationalStructureId, ruleTypeId, scopeType);
                    if (permissionList.Count == 0)
                    {
                        //TODO:::Check if access is not already available
                        int inverseAccessTypeId = accessTypeId == 1 ? 2 : 1;
                        List<PermissionsModel> inversePermissionList = PermissionsController.GetPermissionByClientProjectId(clientProjectModel.Id, inverseAccessTypeId, organisationalStructureId, ruleTypeId, scopeType);
                        showInverse = false;
                        if (inversePermissionList.Count == 0)
                        {
                            UserPermissionModel userPermissionModel = PermissionsController.InsertIntoPermissions(valuesString);

                            if (userPermissionModel.PermissionId > 0)
                            {
                                userPermissionModel.ScopeType = ((ScopeTypeModel)cmbScopeType.SelectedValue).Name;
                                userPermissionModel.RuleType = ((RuleTypeModel)cmbRuleType.SelectedValue).Name;
                                userPermissionModel.OrganisationalStructure = ((OrganisationalStructuresModel)cmbOrganisationalStructure.SelectedValue).Name;
                                userPermissionModel.AccessType = ((AccessTypesModel)cmbAccessType.SelectedValue).Name;

                                //save the permissions to the actove directory on the server
                                string[] pathSplit = clientFormLocation.Split('\\');
                                string clientOrProject = pathSplit.Count() > 4 ? "Project" : "Client";
                                if (pathSplit.Count() == 5)
                                {
                                    savePath = pathSplit.Count() > 4 ? pathSplit[2] + '\\' + pathSplit[3] + '\\' + pathSplit[4] : pathSplit[2] + "\\" + pathSplit[3];
                                }
                                else
                                {
                                    savePath = pathSplit.Count() > 4 ? pathSplit[2] + '\\' + pathSplit[3] + '\\' + pathSplit[4] + '\\' + pathSplit[5] : pathSplit[2] + "\\" + pathSplit[3];
                                }
                                string organStructure = ((OrganisationalStructuresModel)cmbOrganisationalStructure.SelectedValue).Name;
                                string accessType = ((AccessTypesModel)cmbAccessType.SelectedValue).Name;
                                string ruleType = ((RuleTypeModel)cmbRuleType.SelectedValue).Name;
                                bundledPerm = ";" + organStructure + ";" + accessType + ";" + ruleType + ";Add;" + clientOrProject;

                                Common.SetPermissionsInActiveDirectory(savePath + bundledPerm);

                                userPermissionModelList.Add(userPermissionModel);
                                permissionDataGrid.DataSource = null;
                                permissionDataGrid.DataSource = userPermissionModelList;
                                if (permissionDataGrid.Columns.Count > 0)
                                {
                                    permissionDataGrid.Columns["PermissionId"].Visible = false;
                                }
                                if (organisationalStructureId == 1 || organisationalStructureId == 2 || organisationalStructureId == 3 || organisationalStructureId == 11 || organisationalStructureId == 12 || organisationalStructureId == 13)
                                {
                                    userPermissionModel = SetExtended(clientProjectModel.Id, userId, accessTypeId, organisationalStructureId, ruleTypeId, scopeType);
                                    if (userPermissionModel.PermissionId > 0)
                                    {
                                        userPermissionModel.ScopeType = ((ScopeTypeModel)cmbScopeType.SelectedValue).Name;
                                        userPermissionModel.RuleType = ((RuleTypeModel)cmbRuleType.SelectedValue).Name;
                                        userPermissionModel.AccessType = ((AccessTypesModel)cmbAccessType.SelectedValue).Name;

                                        userPermissionModelList.Add(userPermissionModel);
                                        permissionDataGrid.DataSource = null;
                                        permissionDataGrid.DataSource = userPermissionModelList;
                                        if (permissionDataGrid.Columns.Count > 0)
                                        {
                                            permissionDataGrid.Columns["PermissionId"].Visible = false;
                                        }
                                    }
                                }
                                if (userPermissionModel.PermissionId > 0)
                                {
                                    if (loader.InvokeRequired)
                                    {
                                        loader.Invoke(new MethodInvoker(loader.CloseLoader));
                                    }
                                    MessageBox.Show("Permission saved succesfully", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                            }
                            else
                            {
                                if (loader.InvokeRequired)
                                {
                                    loader.Invoke(new MethodInvoker(loader.CloseLoader));
                                }
                                MessageBox.Show("Permission saved error", "Save Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            showInverse = true;
                        }
                    }

                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        private void SetClientSubFolders(int userId, int accessTypeId, int organisationalStructureId, int ruleTypeId, int scopeType)
        {
            try
            {
                foreach (TreeNode treeNode in treeViewFolder.SelectedNode.Nodes)
                {
                    treeNode.TreeView.SelectedNode = treeNode;
                    if (treeNode.Text != "1 Meta Data" && !treeNode.Text.Contains("Pre") && !treeNode.Text.Contains(".exe"))
                    {

                        foreach (TreeNode item in treeNode.Nodes)
                        {
                            if (!item.Text.Contains(".exe"))
                            {
                                string[] folderSplit = item.FullPath.Split('\\');
                                string projectFolderName = folderSplit[4];
                                string projectName = folderSplit[5];
                                ProjectsModel clientProjectModelExists = ProjectsController.GetProjectByNameClientAndYear(projectName, clientModel.Id, projectFolderName);
                                //if client does not exits,create and return
                                ProjectsModel newClientModel = new ProjectsModel();
                                if (clientProjectModelExists.Id == 0)
                                {
                                    newClientModel = ProjectsController.CreateProject(projectName, clientModel.Id, projectFolderName);
                                    ClientProjectModel clientProjectModelForProject = new ClientProjectModel();
                                    List<ClientProjectModel> clientProjectList = ClientProjectController.GetClientProjectsByProjectId(newClientModel.Id);

                                    if (clientProjectList.Count > 0)
                                    {
                                        //Check if the client project has permissions
                                        foreach (var clientProjectListItem in clientProjectList)
                                        {
                                            List<PermissionsModel> permissionsList = PermissionsController.GetPermissionByClientProjectId(clientProjectListItem.Id, accessTypeId, organisationalStructureId, ruleTypeId, scopeType);
                                            if (permissionsList.Count == 0)
                                            {
                                                clientFormLocation = item.FullPath;
                                                // clientProjectModelForProject = ClientProjectController.InsertIntoClientProject("project_id", clientProjectListItem.Id.ToString());
                                                SetPermissionForSubFolders(clientProjectListItem.Id, userId, accessTypeId, organisationalStructureId, ruleTypeId, scopeType);
                                                if (!showInverse)
                                                {
                                                    Common.SetPermissionsInActiveDirectory(savePath + bundledPerm);
                                                }
                                            }
                                            //TODO:::handle if permissions already exists
                                        }
                                    }
                                    else
                                    {
                                        clientProjectModelForProject = ClientProjectController.InsertIntoClientProject("project_id", newClientModel.Id.ToString());
                                    }
                                    if (clientProjectModelForProject.Id > 0)
                                    {
                                        clientFormLocation = item.FullPath;
                                        SetPermissionForSubFolders(clientProjectModelForProject.Id, userId, accessTypeId, organisationalStructureId, ruleTypeId, scopeType);
                                        if (!showInverse)
                                        {
                                            Common.SetPermissionsInActiveDirectory(savePath + bundledPerm);//+ ";B05;Allow;Write;Add;Client"

                                        }
                                    }
                                }
                                else if (clientProjectModelExists.Id > 0)
                                {
                                    //check if client project exists first
                                    ClientProjectModel clientProjectModelForProject = new ClientProjectModel();
                                    List<ClientProjectModel> clientProjectList = ClientProjectController.GetClientProjectsByProjectId(clientProjectModelExists.Id);

                                    if (clientProjectList.Count > 0)
                                    {
                                        //Check if the client project has permissions
                                        foreach (var clientProjectListItem in clientProjectList)
                                        {
                                            List<PermissionsModel> permissionsList = PermissionsController.GetPermissionByClientProjectId(clientProjectListItem.Id, accessTypeId, organisationalStructureId, ruleTypeId, scopeType);
                                            if (permissionsList.Count == 0)
                                            {
                                                clientFormLocation = item.FullPath;
                                                // clientProjectModelForProject = ClientProjectController.InsertIntoClientProject("project_id", clientProjectListItem.Id.ToString());
                                                SetPermissionForSubFolders(clientProjectListItem.Id, userId, accessTypeId, organisationalStructureId, ruleTypeId, scopeType);
                                                if (!showInverse)
                                                {
                                                    Common.SetPermissionsInActiveDirectory(savePath + bundledPerm);
                                                }
                                            }
                                            //TODO:::handle if permissions already exists
                                        }
                                    }
                                    else
                                    {
                                        clientProjectModelForProject = ClientProjectController.InsertIntoClientProject("project_id", clientProjectModelExists.Id.ToString());
                                    }
                                    if (clientProjectModelForProject.Id > 0)
                                    {
                                        clientFormLocation = item.FullPath;
                                        SetPermissionForSubFolders(clientProjectModelForProject.Id, userId, accessTypeId, organisationalStructureId, ruleTypeId, scopeType);
                                        Common.SetPermissionsInActiveDirectory(savePath + bundledPerm);//+ ";B05;Allow;Write;Add;Client"

                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
        string savePath = string.Empty;
        string bundledPerm = string.Empty;
        int returnId = new int();
        public void SetPermissionForSubFolders(int id, int userId, int accessTypeId, int organisationalStructureId, int ruleTypeId, int scopeType)
        {
            try
            {
                id = clientProjectModel.Id == 0 ? id : clientProjectModel.Id;
                string valuesString = id + "," + accessTypeId + "," + organisationalStructureId + "," + ruleTypeId + "," + scopeType + "," + userId;
                int inverseAccessTypeId = accessTypeId == 1 ? 2 : 1;
                List<PermissionsModel> inversePermissionList = PermissionsController.GetPermissionByClientProjectId(id, inverseAccessTypeId, organisationalStructureId, ruleTypeId, scopeType);
                showInverse = false;
                if (inversePermissionList.Count == 0)
                {
                    //chek if permission exists
                    UserPermissionModel userPermissionModel = PermissionsController.InsertIntoPermissions(valuesString);
                    //check if is any of the B groups and find specific to set permissions extended
                    if (organisationalStructureId == 1 || organisationalStructureId == 2 || organisationalStructureId == 3 || organisationalStructureId == 11 || organisationalStructureId == 12 || organisationalStructureId == 13)
                    {
                        SetExtended(id, userId, accessTypeId, organisationalStructureId, ruleTypeId, scopeType);
                    }
                    if (userPermissionModel.PermissionId > 0)
                    {
                        returnId = userPermissionModel.PermissionId;
                        //save the permissions to the actove directory on the server
                        string[] pathSplit = clientFormLocation.Split('\\');
                        string clientOrProject = pathSplit.Count() > 4 ? "Project" : "Client";
                        if (pathSplit.Count() != 5)
                        {
                            savePath = pathSplit.Count() > 4 ? pathSplit[2] + '\\' + pathSplit[3] + '\\' + pathSplit[4] + '\\' + pathSplit[5] : pathSplit[2] + "\\" + pathSplit[3];
                            string organStructure = TypesController.GetTypeById("organisational_structure", organisationalStructureId);
                            string accessType = TypesController.GetTypeById("access_type", accessTypeId);
                            string ruleType = TypesController.GetTypeById("rule_type", ruleTypeId);
                            bundledPerm = ";" + organStructure + ";" + accessType + ";" + ruleType + ";Add;" + clientOrProject;
                            if (clientOrProject == "Client")
                            {
                                clientForm.savePath = savePath;
                                clientForm.bundledPerm = bundledPerm;
                            }
                            else if (clientOrProject == "Project")
                            {
                                projectNameFrm.savePath = savePath;
                                projectNameFrm.bundledPerm.Add(bundledPerm);
                            }
                        }
                    }
                    if (userPermissionModel.PermissionId == 0)
                    {
                        MessageBox.Show("Permission saved error", "Save Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    showInverse = true;
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        bool showInverse = false;

        private UserPermissionModel SetExtended(int id, int userId, int accessTypeId, int organisationalStructureId, int ruleTypeId, int scopeType)
        {
            try
            {
                string valuesString = string.Empty;
                UserPermissionModel userPermissionModel = new UserPermissionModel();
                switch (organisationalStructureId)
                {
                    case 1:
                        valuesString = id + "," + accessTypeId + "," + 21 + "," + ruleTypeId + "," + scopeType + "," + userId;
                        userPermissionModel = PermissionsController.InsertIntoPermissions(valuesString);
                        userPermissionModel.OrganisationalStructure = "B04 Extended";
                        return userPermissionModel;
                    case 2:
                        valuesString = id + "," + accessTypeId + "," + 22 + "," + ruleTypeId + "," + scopeType + "," + userId;
                        userPermissionModel = PermissionsController.InsertIntoPermissions(valuesString);
                        userPermissionModel.OrganisationalStructure = "B05 Extended";
                        return userPermissionModel;
                    case 3:
                        valuesString = id + "," + accessTypeId + "," + 24 + "," + ruleTypeId + "," + scopeType + "," + userId;
                        userPermissionModel = PermissionsController.InsertIntoPermissions(valuesString);
                        userPermissionModel.OrganisationalStructure = "B10 Extended";
                        return userPermissionModel;
                    case 11:
                        valuesString = id + "," + accessTypeId + "," + 25 + "," + ruleTypeId + "," + scopeType + "," + userId;
                        userPermissionModel = PermissionsController.InsertIntoPermissions(valuesString);
                        userPermissionModel.OrganisationalStructure = "B02 Extended";
                        return userPermissionModel;
                    case 12:
                        valuesString = id + "," + accessTypeId + "," + 26 + "," + ruleTypeId + "," + scopeType + "," + userId;
                        userPermissionModel = PermissionsController.InsertIntoPermissions(valuesString);
                        userPermissionModel.OrganisationalStructure = "B03 Extended";
                        return userPermissionModel;
                    case 13:
                        valuesString = id + "," + accessTypeId + "," + 23 + "," + ruleTypeId + "," + scopeType + "," + userId;
                        userPermissionModel = PermissionsController.InsertIntoPermissions(valuesString);
                        userPermissionModel.OrganisationalStructure = "B06 Extended";
                        return userPermissionModel;
                    default:
                        //do nothing
                        MessageBox.Show("Could not save extended permission,please contact Admin.", "Extended Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return userPermissionModel;
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        UserPermissionModel currentObject = new UserPermissionModel();

        private void permissionDataGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                // functionality for editing
                currentObject = (UserPermissionModel)permissionDataGrid.CurrentRow.DataBoundItem;
                btnCancel.Visible = true;
                btnDelete.Visible = true;
                btnSaveChanges.Visible = true;
                button1.Visible = false;
                // set all dropdowsn to current selection of objects
                if (permissionsModelList.Count > 0)
                {
                    PermissionsModel permissionsModel = permissionsModelList.Where(x => x.Id == currentObject.PermissionId).FirstOrDefault();
                    //accessTypesModelList
                    cmbAccessType.SelectedIndex = accessTypesModelList.FindIndex(y => y.Id == permissionsModel.AccessTypeId);
                    cmbOrganisationalStructure.SelectedIndex = organisationalStructuresModelList.FindIndex(y => y.Id == permissionsModel.OrganisationalStructureId);
                    cmbRuleType.SelectedIndex = ruleTypeModelList.FindIndex(y => y.Id == permissionsModel.RuleTypeId);
                    cmbScopeType.SelectedIndex = scopeTypeModelList.FindIndex(y => y.Id == permissionsModel.ScopeId);
                }
                else
                {
                    cmbAccessType.SelectedIndex = accessTypesModelList.FindIndex(y => y.Name == currentObject.AccessType);
                    cmbOrganisationalStructure.SelectedIndex = organisationalStructuresModelList.FindIndex(y => y.Name == currentObject.OrganisationalStructure);
                    cmbRuleType.SelectedIndex = ruleTypeModelList.FindIndex(y => y.Name == currentObject.RuleType);
                    cmbScopeType.SelectedIndex = scopeTypeModelList.FindIndex(y => y.Name == currentObject.ScopeType);
                }
            }
            catch (Exception exception)
            {
                button1.Visible = true;
                btnCancel.Visible = false;
                btnDelete.Visible = false;
                btnSaveChanges.Visible = false;
                currentObject = new UserPermissionModel();
                MessageBox.Show(exception.Message, "Edit Exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            resetCombobox();
        }

        private void resetCombobox()
        {
            //Reset current object and set buttons
            button1.Visible = true;
            btnCancel.Visible = false;
            btnDelete.Visible = false;
            btnSaveChanges.Visible = false;
            currentObject = new UserPermissionModel();
            cmbAccessType.Text = "Please select Access Type";
            cmbOrganisationalStructure.Text = "Please select Organisational Structure";
            cmbRuleType.Text = "Please select Rule Type";
            cmbScopeType.Text = "Please select Scope Type";
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                // add functionality to delete permission
                if (isOwner)
                {
                    int deletePermission = PermissionsController.DeletePermission(currentObject.PermissionId);
                    if (deletePermission == 1)
                    {
                        string[] pathSplit = clientFormLocation.Split('\\');
                        string clientOrProject = pathSplit.Count() > 4 ? "Project" : "Client";
                        savePath = pathSplit.Count() > 4 && pathSplit.Count() != 5 ? pathSplit[2] + '\\' + pathSplit[3] + '\\' + pathSplit[4] + '\\' + pathSplit[5] : pathSplit[2] + "\\" + pathSplit[3];
                        string organStructure = string.Empty;
                        if (currentObject.OrganisationalStructure == "B04 Extended" || currentObject.OrganisationalStructure == "B05 Extended" || currentObject.OrganisationalStructure == "B10 Extended" || currentObject.OrganisationalStructure == "B02 Extended" || currentObject.OrganisationalStructure == "B03 Extended" || currentObject.OrganisationalStructure == "B06 Extended")
                        {
                            organStructure = GetOrganStr(currentObject.OrganisationalStructure);
                        }
                        string accessType = ((AccessTypesModel)cmbAccessType.SelectedValue).Name;
                        string ruleType = ((RuleTypeModel)cmbRuleType.SelectedValue).Name;
                        bundledPerm = ";" + organStructure + ";" + accessType + ";" + ruleType + ";Remove;" + clientOrProject;

                        Common.SetPermissionsInActiveDirectory(savePath + bundledPerm);

                        userPermissionModelList.Remove(currentObject);
                        PermissionsModel permissionsModel = permissionsModelList.Where(x => x.Id == currentObject.PermissionId).FirstOrDefault();
                        permissionsModelList.Remove(permissionsModel);
                        permissionDataGrid.DataSource = null;
                        permissionDataGrid.DataSource = userPermissionModelList.ToArray();

                        if (permissionDataGrid.Columns.Count > 0)
                        {
                            permissionDataGrid.Columns["PermissionId"].Visible = false;
                        }
                        resetCombobox();
                        MessageBox.Show("Permission deleted succesfully", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Permission delete error", "Delete Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Only folder owner can create a permission for this folder", "Authority Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception exception)
            {
                resetCombobox();
                MessageBox.Show(exception.Message, "Delete Permission", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private string GetOrganStr(string organisationalStructure)
        {
            switch (organisationalStructure)
            {
                case "B04 Extended":
                    return "B04";
                case "B05 Extended":
                    return "B05";
                case "B10 Extended":
                    return "B10";
                case "B02 Extended":
                    return "B02";
                case "B03 Extended":
                    return "B03";
                case "B06 Extended":
                    return "B06";
                default:
                    //do nothing
                    return "";
            }
        }

        private void btnSaveChanges_Click(object sender, EventArgs e)
        {
            try
            {
                if (isOwner)
                {
                    if (cmbAccessType.SelectedIndex != -1 && cmbOrganisationalStructure.SelectedIndex != -1 && cmbRuleType.SelectedIndex != -1 && cmbScopeType.SelectedIndex != -1)
                    {
                        //add code for edting permissions
                        int accessTypeId = ((AccessTypesModel)cmbAccessType.SelectedValue).Id;
                        int organisationalStructureId = ((OrganisationalStructuresModel)cmbOrganisationalStructure.SelectedValue).Id;
                        int ruleTypeId = ((RuleTypeModel)cmbRuleType.SelectedValue).Id;
                        int scopeType = ((ScopeTypeModel)cmbScopeType.SelectedValue).Id;
                        int userId = userModel.Id;
                        string date = DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day;

                        int updatePermission = PermissionsController.UpdatePermission(accessTypeId, organisationalStructureId, ruleTypeId, scopeType, userId, date, currentObject.PermissionId);

                        if (updatePermission == 1)
                        {
                            string[] pathSplit = clientFormLocation.Split('\\');
                            string clientOrProject = pathSplit.Count() > 4 ? "Project" : "Client";
                            savePath = pathSplit.Count() > 4 ? pathSplit[2] + '\\' + pathSplit[3] + '\\' + pathSplit[4] + '\\' + pathSplit[5] : pathSplit[2] + "\\" + pathSplit[3];
                            string organStructure = ((OrganisationalStructuresModel)cmbOrganisationalStructure.SelectedValue).Name;
                            string accessType = ((AccessTypesModel)cmbAccessType.SelectedValue).Name;
                            string ruleType = ((RuleTypeModel)cmbRuleType.SelectedValue).Name;
                            bundledPerm = ";" + organStructure + ";" + accessType + ";" + ruleType + ";Edit;" + clientOrProject;

                            //TODO:::REMOVE COMMENTS BELOW AS SOON AS EDIT CODE IS AVAILABLE
                            //Common.SetPermissionsInActiveDirectory(savePath + bundledPerm);

                            UserPermissionModel userPermissionModel = userPermissionModelList.Where(x => x.PermissionId == currentObject.PermissionId).FirstOrDefault();
                            userPermissionModelList.Remove(userPermissionModel);
                            userPermissionModel = new UserPermissionModel
                            {
                                AccessType = ((AccessTypesModel)cmbAccessType.SelectedValue).Name,
                                OrganisationalStructure = ((OrganisationalStructuresModel)cmbOrganisationalStructure.SelectedValue).Name,
                                PermissionId = currentObject.PermissionId,
                                RuleType = ((RuleTypeModel)cmbRuleType.SelectedValue).Name,
                                ScopeType = ((ScopeTypeModel)cmbScopeType.SelectedValue).Name
                            };
                            userPermissionModelList.Add(userPermissionModel);
                            permissionDataGrid.DataSource = null;
                            permissionDataGrid.DataSource = userPermissionModelList.OrderBy(x => x.PermissionId).ToArray();
                            if (permissionDataGrid.Columns.Count > 0)
                            {
                                permissionDataGrid.Columns["PermissionId"].Visible = false;
                            }

                            MessageBox.Show("Permission updated succesfully", "Updated", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            MessageBox.Show("Permission update error", "Updated Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        resetCombobox();

                    }
                    else
                    {
                        MessageBox.Show("Please make sure you have selected all combox boxes", "Update", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                else
                {
                    MessageBox.Show("Only folder owner can create a permission for this folder", "Authority Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Edit Permission Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool IsCurrentUserInClient()
        {
            try
            {
                //get current user username "lawrencep" compare current user to see if they appear in current emails list
                //if user then return "true" else "false"
                return false;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to exit?", "Confirm exit", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }
    }
}
