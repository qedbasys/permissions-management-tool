﻿namespace PermissionsManagementToolFrontEnd.Models
{
    class RuleTypeModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
