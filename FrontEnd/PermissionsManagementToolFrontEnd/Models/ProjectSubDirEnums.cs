﻿using System.ComponentModel;

namespace PermissionsManagementToolFrontEnd.Models
{
    enum ProjectSubDirEnums
    {
        [Description("1 Audit Trail")]
        AuditTrail = 1,
        [Description("2 Received")]
        Received = 2,
        [Description("3 Workings")]
        Workings = 3,
        [Description("4 Results")]
        Results = 4,
        [Description("5 Other")]
        Other = 5
    }

    enum ResultsEnum
    {
        [Description("1 Reports and Letters")]
        ReportsAndLetters = 1,
        [Description("2 Presentations")]
        Presentations = 2,
        [Description("3 Emails")]
        Emails = 3,
        [Description("4 Returns")]
        Returns = 4,
        [Description("5 Other")]
        Other = 5
    }

}
