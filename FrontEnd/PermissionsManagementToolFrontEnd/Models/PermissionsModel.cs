﻿namespace PermissionsManagementToolFrontEnd.Models
{
    class PermissionsModel
    {
        public int Id { get; set; }

        public int ClientProjectId { get; set; }

        public int AccessTypeId { get; set; }

        public int OrganisationalStructureId { get; set; }

        public int RuleTypeId { get; set; }

        public int ScopeId { get; set; }

        public string Project { get; set; }

        public string AccessType { get; set; }

        public string OrganisationalStructure { get; set; }

        public string RuleType { get; set; }

        public string ScopeType { get; set; }
    }

    class UserPermissionModel
    {
        public int PermissionId { get; set; }

        public string AccessType { get; set; }

        public string OrganisationalStructure { get; set; }

        public string RuleType { get; set; }

        public string ScopeType { get; set; }

    }

}
