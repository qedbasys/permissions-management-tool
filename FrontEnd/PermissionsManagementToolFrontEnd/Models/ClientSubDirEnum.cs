﻿using System.ComponentModel;

namespace PermissionsManagementToolFrontEnd.Models
{
    enum ClientSubDirEnum
    {
        [Description("1 Audit Trail")]
        AuditTrail = 1,
        [Description("2 TendersProposals")]
        Received = 2,
        [Description("3 Agreements")]
        Workings = 3,
        [Description("4 Billing")]
        Results = 4,
        [Description("5 Client Feedback")]
        Other = 5
    }

    enum ClientMetaDataSubDir
    {
        [Description("Client Documentation")]
        ClientDocumentation = 1,
        [Description("QED Documentation")]
        QEDDocumentation = 2,

    }

    enum ClientMetaDataAuditTrailClientDocEnum
    {
        [Description("Committee Packs")]
        CommitteePacks = 1,
        [Description("Company Policies")]
        CompanyPolicies = 2,
        [Description("External Reports")]
        ExternalReports = 3,
        [Description("Policy Documentation")]
        PolicyDocumentation = 4,
        [Description("Regulatory")]
        Regulatory = 5,
        [Description("Reinsurance Agreements")]
        ReinsuranceAgreements = 6,
        [Description("Scheme Rules")]
        SchemeRules = 7,
        [Description("Tables")]
        Tables = 8,
    }

    enum TablesEnum
    {
        [Description("Other")]
        Other = 1,
        [Description("Premium Rates")]
        PremiumRates = 2,
        [Description("PUP Values")]
        PUPValues = 3,
        [Description("Reinsurance Rates")]
        ReinsuranceRates = 4,
        [Description("Surrender Values")]
        SurrenderValues = 5
    }
    enum QEDDocumentationEnum
    {
        [Description("Client Notes")]
        ClientNotes = 1,
        [Description("ProcessDocumentation")]
        ProcessDocumentation = 2,
    }

    enum CommitteePacksEnum
    {
        [Description("ARC")]
        ARC = 1,
        [Description("Audit")]
        Audit = 2,
        [Description("Board")]
        Board = 3,

    }
}
