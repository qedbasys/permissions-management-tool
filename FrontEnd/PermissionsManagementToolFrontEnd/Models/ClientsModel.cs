﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PermissionsManagementToolFrontEnd.Models
{
    class ClientsModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string UserName { get; set; }

        public int UserId { get; set; }

        public string CountryName { get; set; }

        public int CountryId { get; set; }
    }
}
