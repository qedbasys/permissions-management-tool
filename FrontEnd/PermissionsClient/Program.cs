﻿using DataAccessLayer;
using System;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Net.Sockets;
class CountriesModel
{
    public int Id { get; set; }

    public string Name { get; set; }
}

class UsersModel
{
    public int Id { get; set; }

    public string UserId { get; set; }

    public string FirstName { get; set; }

    public string LastName { get; set; }

    public string UserName { get; set; }

    public string Email { get; set; }

    public string ClientManagerName { get; set; }
}
class ClientsModel
{
    public int Id { get; set; }

    public string Name { get; set; }

    public string UserName { get; set; }

    public int UserId { get; set; }

    public string CountryName { get; set; }

    public int CountryId { get; set; }
}
class Program
{
    static void Main(string[] args)
    {
        //new Thread(() =>
        //{
        //    Thread.CurrentThread.IsBackground = true;
        //    Connect("127.0.0.1", "Hello I'm Device 1...");
        //}).Start();
        //new Thread(() =>
        //{
        //    Thread.CurrentThread.IsBackground = true;
        //    Connect("127.0.0.1", "Hello I'm Device 2...");
        //}).Start();
        //Connect("127.0.0.1", @"C:\Users\EtienneL\Documents\947 Certificates");
        //Connect("10.45.120.67", @"\\10.45.120.66\QED Clients\Test Country\Test Client;B10;Allow;Write;Add;Client");
        //Connect("10.45.120.67", @"U:\Test Country\Test Client;B10;Allow;Write;Add;Client");
        //Connect("10.45.120.66", @"Test Country\Test Client;B05;Allow;Write;Add;Client");

        //Connect("127.0.0.1", @"C:\Users\EtienneL\Documents\Test Country\Test Client;B10;Allow;Write;Add;Client");
        //Connect("127.0.0.1", @"\\127.0.0.1\Users\EtienneL\Documents\Test Country\Test Client;B10;Allow;Write;Add;Client");

        //read excel file

        string connectString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=c:\\Users\\LawrenceP\\Downloads\\Client Managers.xlsx;Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1;\"";

        OleDbConnection conn = new OleDbConnection(connectString);
        OleDbDataAdapter da = new OleDbDataAdapter("Select * From [Sheet1$]", conn);
        DataTable dt = new DataTable();
        da.Fill(dt);
        conn.Close();
        //get country id by name
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CountriesModel country = GetCountryByName(dt.Rows[i]["Country"].ToString());
            string[] nameSplit = dt.Rows[i]["Responsible Person"].ToString().Split(' ');
            string name = nameSplit[0];
            string surname = nameSplit.Count() > 2 ? nameSplit[1] + " " + nameSplit[2] : nameSplit[1];
            UsersModel user = GetUserFromDb(name, surname);

            ClientsModel clientModel = InsertIntoClients(dt.Rows[i]["New Folder Name"].ToString().Trim(), user.Id, country.Id);
            //todo code that sets permissions from active directory
        }
        Console.ReadLine();
    }

    internal static ClientsModel InsertIntoClients(string clientName, int userId, int countryId)
    {
        try
        {
            string columnString = "name,user_id,country_id";
            string valuesString = "'" + clientName + "'," + userId + "," + countryId;
            int clientId = PostgreSqlClass.InsertData("clients", columnString, valuesString);
            ClientsModel clientModel = new ClientsModel
            {
                Id = clientId,
                Name = clientName,
                CountryId = countryId,
                UserId = userId,
            };
            return clientModel;
        }
        catch (Exception exception)
        {
            throw exception;
        }
    }

    internal static UsersModel GetUserFromDb(string firstName, string lastName)
    {
        try
        {
            string sql = "SELECT * FROM users WHERE first_name = '" + firstName + "' AND last_name='" + lastName + "'";
            DataTable userSet = PostgreSqlClass.GetAllDataTable(sql);
            UsersModel model = new UsersModel();
            for (int i = 0; i < userSet.Rows.Count; i++)
            {
                model = new UsersModel
                {
                    Id = Convert.ToInt32(userSet.Rows[i]["id"]),
                    FirstName = userSet.Rows[i]["first_name"].ToString(),
                    LastName = userSet.Rows[i]["last_name"].ToString(),
                    UserId = userSet.Rows[i]["user_id"].ToString(),
                    Email = userSet.Rows[i]["email"].ToString(),
                    UserName = userSet.Rows[i]["username"].ToString(),
                };
            }
            return model;
        }
        catch (Exception exception)
        {
            throw exception;
        }
    }

    internal static CountriesModel GetCountryByName(string countryName)
    {
        try
        {
            string sql = "SELECT * FROM countries WHERE name = '" + countryName + "'";
            DataTable accessTypes = PostgreSqlClass.GetAllDataTable(sql);
            CountriesModel model = new CountriesModel();
            for (int i = 0; i < accessTypes.Rows.Count; i++)
            {
                model = new CountriesModel
                {
                    Id = Convert.ToInt32(accessTypes.Rows[i]["id"]),
                    Name = accessTypes.Rows[i]["name"].ToString(),
                };
            }
            return model;
        }
        catch (Exception exception)
        {
            throw exception;
        }
    }

    static void Connect(String server, String message)
    {
        try
        {
            Int32 port = 13000;
            TcpClient client = new TcpClient(server, port);
            NetworkStream stream = client.GetStream();

            // Translate the Message into ASCII.
            Byte[] data = System.Text.Encoding.ASCII.GetBytes(message);
            // Send the message to the connected TcpServer. 
            stream.Write(data, 0, data.Length);
            Console.WriteLine("Sent: {0}", message);
            // Bytes Array to receive Server Response.
            data = new Byte[256];
            String response = String.Empty;
            // Read the Tcp Server Response Bytes.
            Int32 bytes = stream.Read(data, 0, data.Length);
            response = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
            Console.WriteLine("Received: {0}", response);

            stream.Close();
            client.Close();
        }
        catch (Exception e)
        {
            Console.WriteLine("Exception: {0}", e);
        }
        Console.Read();
    }
}