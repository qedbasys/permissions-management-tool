﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Security.AccessControl;
class Server
{
    TcpListener server = null;
    public Server(string ip, int port)
    {
        IPAddress localAddr = IPAddress.Parse(ip);
        server = new TcpListener(localAddr, port);
        server.Start();
        StartListener();
    }
    public void StartListener()
    {
        try
        {
            while (true)
            {
                Console.WriteLine("Waiting for a connection...");
                TcpClient client = server.AcceptTcpClient();
                Console.WriteLine("Connected!");
                Thread t = new Thread(new ParameterizedThreadStart(HandleDeivce));
                t.Start(client);
            }
        }
        catch (SocketException e)
        {
            Console.WriteLine("SocketException: {0}", e);
            server.Stop();
        }
    }
    public void HandleDeivce(Object obj)
    {
        TcpClient client = (TcpClient)obj;
        var stream = client.GetStream();
        string imei = String.Empty;
        string data = null;
        Byte[] bytes = new Byte[256];
        int i;
        try
        {
            while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
            {
                string hex = BitConverter.ToString(bytes);
                data = Encoding.ASCII.GetString(bytes, 0, i);
                Console.WriteLine("{1}: Received: {0}", data, Thread.CurrentThread.ManagedThreadId);
                string str = "Hey Device!";
                Byte[] reply = System.Text.Encoding.ASCII.GetBytes(str);
                stream.Write(reply, 0, reply.Length);
                Console.WriteLine("{1}: Sent: {0}", str, Thread.CurrentThread.ManagedThreadId);
                SetClientPermissions(@"C:\Users\EtienneL\Documents\947 Certificates", "B10", "Allow", "Write");
            }
        }
        catch (Exception e)
        {
            Console.WriteLine("Exception: {0}", e.ToString());
            client.Close();
        }
    }

    public static void SetClientPermissions(string path, string department, string accessControl, string fileSystemRight)
    {
        //string serverPrefixPath = @"E:\QED\G_ArrayTemp\QED Clients\";

        //Remove U:\ from the path that comes in and then concat the path with the server prefix

        //string root = Path.GetPathRoot(path);

        //if (root != null)
        //{
        //    path = path.Substring(root.Length);
        //}

        try
        {
            //string DirectoryName = @"C:\Inetpub\ftproot";
            //string DirectoryName = serverPrefixPath + path;
            string DirectoryName = path;

            Console.WriteLine("Adding access control entry for " + DirectoryName);

            AccessControlType accessControlType;
            accessControlType = AccessControlType.Allow;

            FileSystemRights fileSystemRightType;
            fileSystemRightType = FileSystemRights.Read;


            accessControl = accessControl.ToLower();
            fileSystemRight = fileSystemRight.ToLower();

            if (accessControl == "allow")
            {
                accessControlType = AccessControlType.Allow;
            }
            else if (accessControl == "deny")
            {
                accessControlType = AccessControlType.Deny;
            }

            if (fileSystemRight == "read")
            {
                fileSystemRightType = FileSystemRights.Read;
            }
            else if (fileSystemRight == "write")
            {
                fileSystemRightType = FileSystemRights.Write;
            }


            AddDirectorySecurity(DirectoryName, @"qed.local\" + department, FileSystemRights.Modify, AccessControlType.Allow);

            // Add the access control entry to the directory.
            //AddDirectorySecurity(DirectoryName, @"QED\" + department, fileSystemRightType, accessControlType);
            //Console.WriteLine("Removing access control entry from " + DirectoryName);
            // Remove the access control entry from the directory.
            //RemoveDirectorySecurity(DirectoryName, @"QED\" + department, FileSystemRights.Write, AccessControlType.Allow);
            //Console.WriteLine("Done.");
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }

        Console.ReadLine();
    }

    // Adds an ACL entry on the specified directory for the specified account.
    private static void AddDirectorySecurity(string FileName, string Account, FileSystemRights Rights, AccessControlType ControlType)
    {
        // Create a new DirectoryInfo object.
        DirectoryInfo dInfo = new DirectoryInfo(FileName);

        // Get a DirectorySecurity object that represents the 
        // current security settings.
        DirectorySecurity dSecurity = dInfo.GetAccessControl();

        // Add the FileSystemAccessRule to the security settings. 
        dSecurity.AddAccessRule(new FileSystemAccessRule(Account, Rights, ControlType));
        dSecurity.AddAccessRule(new FileSystemAccessRule(Account, Rights, InheritanceFlags.ContainerInherit, PropagationFlags.InheritOnly, ControlType));
        dSecurity.AddAccessRule(new FileSystemAccessRule(Account, Rights, InheritanceFlags.ObjectInherit, PropagationFlags.InheritOnly, ControlType));



        // Set the new access settings.
        dInfo.SetAccessControl(dSecurity);

    }

    // Removes an ACL entry on the specified directory for the specified account.
    private static void RemoveDirectorySecurity(string FileName, string Account, FileSystemRights Rights, AccessControlType ControlType)
    {
        // Create a new DirectoryInfo object.
        DirectoryInfo dInfo = new DirectoryInfo(FileName);

        // Get a DirectorySecurity object that represents the 
        // current security settings.
        DirectorySecurity dSecurity = dInfo.GetAccessControl();

        // Add the FileSystemAccessRule to the security settings. 
        dSecurity.RemoveAccessRule(new FileSystemAccessRule(Account, Rights, ControlType));

        // Set the new access settings.
        dInfo.SetAccessControl(dSecurity);

    }
}
